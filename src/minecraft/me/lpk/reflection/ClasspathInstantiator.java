package me.lpk.reflection;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.reflect.ClassPath;
import com.google.common.reflect.ClassPath.ClassInfo;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.function.Consumer;

/**
 * A utility for loading classes from a package.
 * 
 * @param <T>
 *            Generic type of classes to be instantiated..
 */
public final class ClasspathInstantiator<T> {
	/**
	 * Boolean for recursive package searching.
	 */
	private boolean recursiveSearch;

	/**
	 * The top-level package to be used for searching. Sub-packages will be
	 * searched if {@linkplain #recursiveSearch} is true.
	 */
	private String targetPackage;

	/**
	 * The super type required by classes in {@linkplain #targetPackage the
	 * target package} to be loaded. If a class in the package is not of this
	 * type, it will not be passed to function in
	 * {@linkplain #forEachClass(ClassLoader, Consumer)}.
	 */
	private Class<T> superClass;

	/**
	 * Used for loading the Class object from Guava's ClassInfo object.
	 */
	private LoadStrategy<T> loadStrategy;

	/**
	 * Used for creating an instance of the class created from the
	 * {@linkplain #loadStrategy}.
	 */
	private InstanceStrategy<T> instanceStrategy;

	/**
	 * Constructs a ClasspathInstantiator with a given target package, super
	 * class type, load and instance strategies, and a boolean for recursive
	 * package searching.
	 * 
	 * @param targetPackage
	 *            Top level package to be loaded from.
	 * @param superClass
	 *            Super type of classes to be loaded.
	 * @param loadStrategy
	 *            Strategy for loading classes.
	 * @param instanceStrategy
	 *            Strategy for creating instances.
	 * @param recursiveSearch
	 *            If the target package sub-packages should be searched.
	 */
	public ClasspathInstantiator(String targetPackage, Class<T> superClass, LoadStrategy<T> loadStrategy, InstanceStrategy<T> instanceStrategy, boolean recursiveSearch) {
		this.targetPackage = targetPackage;
		this.superClass = superClass;
		this.loadStrategy = loadStrategy;
		this.instanceStrategy = instanceStrategy;
		this.recursiveSearch = recursiveSearch;
	}

	/**
	 * Constructs a simple ClasspathInstantiator with a given target package,
	 * super class type, and a boolean for recursive package searching.
	 * 
	 * @param targetPackage
	 *            Top level package to be loaded from.
	 * @param superClass
	 *            Super type of classes to be loaded.
	 * @param recursiveSearch
	 *            If the target package sub-packages should be searched.
	 */
	public static <T> ClasspathInstantiator create(String targetPackage, Class<T> superClass, boolean recursiveSearch) {
		LoadStrategy<T> loadStrategy = new LoadStrategy() {
			@Override
			public Class loadClass(ClassInfo classInfo, Class superClass) {
				try {
					// Load the class given the name
					Class<?> c = Class.forName(classInfo.getName());
					// Check if the class extends superClass
					// If it does, return it and it will be added to the list in
					// ClasspathInstantiator
					// If it does not, return null and it will be ignored.
					if (superClass.isAssignableFrom(c)) {
						return c;
					}
				} catch (ClassNotFoundException e) {
				}
				return null;
			}
		};
		InstanceStrategy<T> instanceStrategy = new InstanceStrategy() {
			@Override
			public Object createInstance(Class clazz) {
				try {
					return clazz.newInstance();
				} catch (InstantiationException | IllegalAccessException e) {
					return null;
				}
			}
		};
		return new ClasspathInstantiator(targetPackage, superClass, loadStrategy, instanceStrategy, recursiveSearch);
	}

	/**
	 * Iterates classes from {@link #getTargetPackage() the target package} and
	 * sends them through the given consumer.
	 * 
	 * @param classLoader
	 *            ClassLoader used to load classes from.
	 * @param consumer
	 *            The consumer function.
	 * @throws ClasspathLoadException
	 *             Thrown if the ClassPath object couldn't be generated from the
	 *             given ClassLoader.
	 * @throws IllegalArgumentException
	 *             Thrown if either of the inputs are null.
	 */
	public final void forEachClass(ClassLoader classLoader, Consumer<T> consumer) throws IOException, IllegalArgumentException {
		// Check input validy.
		// NOTE: The classLoader is checked in getClasses(classLoader)
		// an extra check here would be redundant.
		if (consumer == null)
			throw new IllegalArgumentException("Consumer cannot be null!");

		// Retrieve classes from the package, run them through the consumer.
		List<T> classes = getInstances(classLoader);
		for (T t : classes)
			consumer.accept(t);
	}

	/**
	 * Iterates classes from {@link #getTargetPackage() the target package} and
	 * adds an instance of each to a list.
	 * 
	 * @param classLoader
	 *            ClassLoader used to load classes from.
	 * @throws ClasspathLoadException
	 *             Thrown if the ClassPath object couldn't be generated from the
	 *             given ClassLoader.
	 * @throws IllegalArgumentException
	 *             Thrown if the ClassLoader is null.
	 */
	public final List<T> getInstances(ClassLoader classLoader) throws IOException, IllegalArgumentException {
		// The list of class instances.
		List<T> instances = Lists.newArrayList();
		List<Class<T>> classes = getClasses(classLoader);

		// Iterate the Classes, create instances if possible.
		for (Class<T> clazz : classes) {
			if (canBeInstantiated(clazz)) {
				// Create an instance, add it to the list.
				T instance = this.instanceStrategy.createInstance(clazz);
				if (instance != null)
					instances.add(instance);
			}
		}
		return instances;
	}

	/**
	 * Iterates classes from {@link #getTargetPackage() the target package} and
	 * adds them to a list.
	 * 
	 * @param classLoader
	 *            ClassLoader used to load classes from.
	 * @throws ClasspathLoadException
	 *             Thrown if the ClassPath object couldn't be generated from the
	 *             given ClassLoader.
	 * @throws IllegalArgumentException
	 *             Thrown if the ClassLoader is null.
	 */
	public final List<Class<T>> getClasses(ClassLoader classLoader) throws IOException, IllegalArgumentException {
		// Check input validy
		if (classLoader == null)
			throw new IllegalArgumentException("ClassLoader cannot be null!");

		// Load Guava classpath from given ClassLoader
		ClassPath classpath = ClassPath.from(classLoader);

		// Load classes from the target package.
		// Determine if classes are to be added recursively.
		ImmutableSet<ClassInfo> loadedClasses;
		if (this.isRecursiveSearch())
			loadedClasses = classpath.getTopLevelClassesRecursive(this.getTargetPackage());
		else
			loadedClasses = classpath.getTopLevelClasses(this.getTargetPackage());

		// The list of class instances.
		List<Class<T>> classes = Lists.newArrayList();

		// Iterate the ClassInfo objects discovered in the package(s).
		for (ClassInfo classInfo : loadedClasses) {
			// Load the class, add it to the list
			Class<T> clazz = this.loadStrategy.loadClass(classInfo, superClass);
			if (clazz != null) {
				classes.add(clazz);
			}
		}
		return classes;
	}

	/**
	 * Checks if a given class can be instantiated.
	 * 
	 * @param clazz
	 * @return
	 */
	private final boolean canBeInstantiated(Class<T> clazz) {
		// That's not even a class!
		if (clazz == null)
			return false;
		// Abstract, cannot be instantiated
		if (Modifier.isAbstract(clazz.getModifiers()))
			return false;
		// Interface, cannot be instantiated
		if (clazz.isInterface())
			return false;
		// Should be an instantiatable class.
		return true;
	}

	/**
	 * Gets if the {@link #getTargetPackage() top level package} should be
	 * searched recursively <i>(In its sub-packages)</i>.
	 * 
	 * @return Using recursive searching.
	 */
	public final boolean isRecursiveSearch() {
		return recursiveSearch;
	}

	/**
	 * Sets {@link #isRecursiveSearch() recursive searching}.
	 * 
	 * @param recursiveSearch
	 *            If sub-packages of {@link #getTargetPackage() top level
	 *            package} should be searched.
	 */
	public final void setRecursiveSearch(boolean recursiveSearch) {
		this.recursiveSearch = recursiveSearch;
	}

	/**
	 * Gets the top level package used for searching for classes that are
	 * children of {@link #getSuperClass() the given super class}.
	 * 
	 * @return Top level package.
	 */
	public final String getTargetPackage() {
		return targetPackage;
	}

	/**
	 * Sets the {@link #getTargetPackage() top level package} used for
	 * searching.
	 * 
	 * @param targetPackage
	 *            Top level package.
	 * @throws IllegalArgumentException
	 *             Thrown if the parameter is null.
	 */
	public final void setTargetPackage(String targetPackage) {
		if (targetPackage == null)
			throw new IllegalArgumentException("Cannot set targetPackage to null!");
		this.targetPackage = targetPackage;
	}

	/**
	 * Gets the super type required by classes in {@linkplain #targetPackage the
	 * target package} to be loaded. If a class in the package is not of this
	 * type, it will not be passed to function in
	 * {@linkplain #forEachClass(ClassLoader, Consumer)}.
	 * 
	 * @return Super type.
	 */
	public final Class<T> getSuperClass() {
		return superClass;
	}

	/**
	 * Sets the super type required by classes in {@linkplain #targetPackage the
	 * target package} to be loaded.
	 * 
	 * @param superClass
	 *            Super type.
	 * @throws IllegalArgumentException
	 *             Thrown if the parameter is null.
	 */
	public final void setSuperClass(Class<T> superClass) {
		if (superClass == null)
			throw new IllegalArgumentException("Cannot set superClass type to null!");
		this.superClass = superClass;
	}

	/**
	 * Gets the strategy for loading classes in
	 * {@link #forEachClass(ClassLoader, Consumer)}.
	 * 
	 * @return Strategy for loading classes.
	 */
	public final LoadStrategy<T> getLoadStrategy() {
		return loadStrategy;
	}

	/**
	 * Sets the strategy for loading classes in
	 * {@link #forEachClass(ClassLoader, Consumer)}.
	 * 
	 * @param loadStrategy
	 *            Strategy for loading classes.
	 * @throws IllegalArgumentException
	 *             Thrown if the parameter is null.
	 */
	public final void setLoadStrategy(LoadStrategy<T> loadStrategy) {
		if (loadStrategy == null)
			throw new IllegalArgumentException("Cannot set loadStrategy to null!");
		this.loadStrategy = loadStrategy;
	}

	/**
	 * Gets the strategy for instantiating classes in
	 * {@link #forEachClass(ClassLoader, Consumer)}.
	 * 
	 * @return Strategy for instantiating classes.
	 */
	public final InstanceStrategy<T> getInstanceStrategy() {
		return instanceStrategy;
	}

	/**
	 * Sets the strategy for instantiating classes in
	 * {@link #forEachClass(ClassLoader, Consumer)}.
	 * 
	 * @param instanceStrategy
	 *            Strategy for instantiating classes.
	 * @throws IllegalArgumentException
	 *             Thrown if the parameter is null.
	 */
	public final void setInstanceStrategy(InstanceStrategy<T> instanceStrategy) {
		if (instanceStrategy == null)
			throw new IllegalArgumentException("Cannot set instanceStrategy to null!");
		this.instanceStrategy = instanceStrategy;
	}
}