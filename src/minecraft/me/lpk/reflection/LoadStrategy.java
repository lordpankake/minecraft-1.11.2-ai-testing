package me.lpk.reflection;

import com.google.common.reflect.ClassPath.ClassInfo;

public interface LoadStrategy<T> {

	Class<T> loadClass(ClassInfo classInfo, Class<T> superClass);

}