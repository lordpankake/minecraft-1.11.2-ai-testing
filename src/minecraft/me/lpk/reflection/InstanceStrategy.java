package me.lpk.reflection;

public interface InstanceStrategy<T> {

	T createInstance(Class<T> clazz);

}