package me.lpk.util;

public class Timer {
	private long previousTime;

	public Timer() {
		previousTime = getCurrentTime();
	}

	public boolean check(float milliseconds) {
		return elapsed() >= milliseconds;
	}

	public long elapsed() {
		return getCurrentTime() - previousTime;
	}

	public void reset() {
		previousTime = getCurrentTime();
	}

	public long getCurrentTime() {
		return System.currentTimeMillis();
	}
}