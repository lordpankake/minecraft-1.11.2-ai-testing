package me.lpk.ai;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.pathfinding.PathNavigate;
import net.minecraft.pathfinding.PathNodeType;

public interface AIUser {

	EntityLivingBase getEntity();

	IMoveHelper getMoveHelper();

	float getPathPriority(PathNodeType pathnodetype3);

	void setPathPriority(PathNodeType water, float avoidsWater);

	PathNavigate getNavigator();

	void setAIMoveSpeed(float f1);

	void setMoveForward(float moveForward);

	void setMoveStrafing(float moveStrafe);

	IJumpHelper getJumpHelper();

}
