package me.lpk.ai;

public interface IMoveHelper {
	void setMoveTo(double x, double y, double z, double speedIn);

	void read(IMoveHelper other);

	boolean isUpdating();

	double getSpeed();

	void strafe(float f, float g);

}
