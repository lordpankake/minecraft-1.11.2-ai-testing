package me.lpk.event;

/**
 * The event. Can be extended to carry data.
 * 
 * @author LPK
 */
public abstract class Event {
	/**
	 * Boolean for determining if the event has been cancelled.
	 * 
	 * Does not prevent subscribers from receiving the event if it has already
	 * been cancelled.
	 */
	private boolean cancelled;

	public boolean isCancelled() {
		return cancelled;
	}

	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
}