package me.lpk.event;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Handles the firing of events of a single type to all subscribed classes.
 *
 * @author LPK
 */
public class SubscriptionHandler {
	/**
	 * A map of subscribed objects to the appropriate subscribed method.
	 */
	private final Map<Object, Method> subscribed = new HashMap<>();

	/**
	 * Fires an event to all subscribed classes.
	 *
	 * @param event
	 */
	public void fire(Event event) {
		for (Object subscriber : subscribed.keySet()) {
			try {
				subscribed.get(subscriber).invoke(subscriber, event);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Adds a subscriber's method to the event receiving map.
	 *
	 * @param subscriber
	 * @param mh
	 */
	public void add(Object subscriber, Method mh) {
		subscribed.put(subscriber, mh);
	}

	/**
	 * Removes a subscriber from event receiving.
	 *
	 * @param subscriber
	 */
	public void remove(Object subscriber) {
		if (subscribed.keySet().contains(subscriber)) {
			subscribed.remove(subscriber);
		}
	}

	/**
	 * Gets the set of subscribers.
	 *
	 * @return
	 */
	public Set<Object> getSubscribed() {
		return subscribed.keySet();
	}
}