package me.lpk.event;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Handles event firing and subscribed objects.
 *
 * @author LPK
 */
public class EventSystem {
	/**
	 * A map of event classes to their subscription handlers.
	 */
	private static final Map<Class<? extends Event>, SubscriptionHandler> SUBSCRIPTIONS = new HashMap<Class<? extends Event>, SubscriptionHandler>();

	/**
	 * A map of event classes to their instances.
	 */
	private static final Map<Class<? extends Event>, Event> INSTANCE_MAP = new HashMap<Class<? extends Event>, Event>();

	/**
	 * Registers the given event for later usage.
	 *
	 * @param event
	 */
	public static void registerEvent(Event event) {
		EventSystem.INSTANCE_MAP.put(event.getClass(), event);
		EventSystem.SUBSCRIPTIONS.put(event.getClass(), new SubscriptionHandler());
	}

	/**
	 * Gets an events stored instance given the event's class.
	 *
	 * @param eventClass
	 * @return
	 */
	public static <T extends Event> T getEvent(Class<? extends Event> eventClass) {
		return (T) EventSystem.INSTANCE_MAP.get(eventClass);
	}

	/**
	 * Fires a given event to it's subscribers.
	 *
	 * @param event
	 * @param preentity
	 */
	public static void fire(Event event) {
		// Reset event's cancelled state every time it's fired
		event.setCancelled(false);
		EventSystem.SUBSCRIPTIONS.get(event.getClass()).fire(event);
	}

	/**
	 * Fires a given class's event instance to it's subscribers.
	 *
	 * @param eventClass
	 */
	public static void fire(Class<? extends Event> eventClass) {
		EventSystem.fire(EventSystem.INSTANCE_MAP.get(eventClass));
	}

	/**
	 * Registers an object for accepting events.
	 *
	 * @param subscriber
	 * @throws IllegalAccessException
	 */
	public static void subscribe(Object subscriber) {
		Method[] methods = subscriber.getClass().getDeclaredMethods();
		for (Method method : methods) {
			if (method.getParameterCount() != 1) {
				// Invalid parameter count
				continue;
			}
			Class<?> paramClass = method.getParameterTypes()[0];
			if (!Event.class.isAssignableFrom(paramClass)) {
				// Parameter is not an event.
				continue;
			}
			EventSystem.SUBSCRIPTIONS.get(paramClass).add(subscriber, method);
		}
	}

	/**
	 * Unregisters an object for accepting events.
	 *
	 * @param subscriber
	 * @throws IllegalAccessException
	 */
	public static void unsubscribe(Object subscriber) {
		Method[] methods = subscriber.getClass().getDeclaredMethods();
		for (Method method : methods) {
			if (method.getParameterCount() != 1) {
				// Invalid parameter count
				continue;
			}
			Class<?> paramClass = method.getParameterTypes()[0];
			if (!Event.class.isAssignableFrom(paramClass)) {
				// Parameter is not an event.
				continue;
			}
			EventSystem.SUBSCRIPTIONS.get(paramClass).remove(subscriber);
		}
	}
}