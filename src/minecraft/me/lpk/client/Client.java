package me.lpk.client;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import me.lpk.client.analytics.Analytics;
import me.lpk.client.lang.Lang;
import me.lpk.client.plugin.Plugins;
import me.lpk.event.Event;
import me.lpk.event.EventSystem;
import me.lpk.reflection.ClasspathInstantiator;

public class Client {
	public static boolean replaceSysOut = false;
	public static boolean publicBuild = false;
	//
	// TODO: Update all ClasspathInstantiator usage to use default package if
	// the client is obfuscated
	// TODO: Check if a top-level package of "" is default package in
	// ClasspathInstantiator
	private final static Logger logger = LogManager.getLogger("ClientLog");
	private final static Gson gson = new GsonBuilder().setPrettyPrinting().create();
	private final static File directory = new File("CData");
	private final static Analytics analytics = new Analytics();
	private final static Plugins plugins = new Plugins();
	private final static Lang lang = new Lang();

	public static void init(String language) {
		registerEvents();
		lang.onLoad();
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				// Save stats
				analytics.onUnload();
				// Save plugin data
				plugins.onUnload();
				// Save translations
				if (!Client.publicBuild) {
					lang.onUnload();
				}
			}
		});
		if (!directory.exists()) {
			directory.mkdir();
		}
		analytics.onLoad();
		plugins.onLoad();
	}

	/**
	 * Load events by recursively searching packages relative to this class.
	 */
	private static void registerEvents() {
		try {
			ClasspathInstantiator<Event> cpInst = ClasspathInstantiator.create(Client.class.getPackage().getName(), Event.class, true);
			cpInst.forEachClass(ClassLoader.getSystemClassLoader(), (Event e) -> EventSystem.registerEvent(e));
		} catch (IllegalArgumentException | IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Return the language handler instance.
	 */
	public static Lang getLang() {
		return lang;
	}

	/**
	 * Return the plugin manager instance.
	 */
	public static Plugins getPlugins() {
		return plugins;
	}

	/**
	 * Return the directory.
	 */
	public static File getDirectory() {
		return directory;
	}

	/**
	 * Return the GSON instance.
	 */
	public static Gson gson() {
		return gson;
	}

	/**
	 * Return the Analytics instance.
	 */
	public static Analytics getAnalytics() {
		return analytics;
	}

	/**
	 * Return the client logger.
	 */
	public static Logger getLog() {
		return logger;
	}

	/**
	 * Print an error.
	 * 
	 * @param err
	 *            The error message.
	 */
	public static void logErr(String err) {
		logger.error(err);
	}

	/**
	 * Print a message.
	 * 
	 * @param err
	 *            The message.
	 */
	public static void logInfo(String info) {
		logger.info(info);
	}
}
