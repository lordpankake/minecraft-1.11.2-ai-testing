package me.lpk.client;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.google.gson.JsonSyntaxException;

public interface Loadable {
	/**
	 * Called when the plugin is loaded. Loading information should be done
	 * here.
	 */
	void onLoad();

	/**
	 * Called when the plugin is unloaded. Saving information should be done
	 * here.
	 */
	void onUnload();

	/**
	 * Reads a Map from the given file. All found entries are added to the given
	 * map. The given Type is generted from a Gson TypeToken so that the map is
	 * parsed with the correct generic types <i>(IE: specifying the K and V in
	 * Map<K,V>)</i>.
	 * 
	 * @param file
	 *            JSON file
	 * @param map
	 *            Map to put values into
	 * @param type
	 *            Type of the map
	 * @throws JsonSyntaxException
	 *             Thrown if the JSON syntax is broken
	 * @throws IOException
	 *             Thrown if some IO issue occured
	 */
	default void loadMap(File file, Map map, Type type) throws JsonSyntaxException, IOException {
		if (!file.exists()) {
			file.createNewFile();
			// Don't bother reading from it if it's going to be empty.
		} else {
			Map<String, Long> serverUsage = Client.gson().fromJson(FileUtils.readFileToString(file), type);
			if (serverUsage != null) {
				map.putAll(serverUsage);
			}
		}
	}
}
