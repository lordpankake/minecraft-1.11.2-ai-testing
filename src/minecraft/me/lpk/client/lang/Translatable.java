package me.lpk.client.lang;

public interface Translatable {
	/**
	 * Called when the translatable components of the class need to be updated.
	 */
	void onLanguageUpdate();
}
