package me.lpk.client.lang;

import me.lpk.client.Client;

public interface Translator {
	default String translate(String key) {
		return Client.getLang().get(key);
	}
}
