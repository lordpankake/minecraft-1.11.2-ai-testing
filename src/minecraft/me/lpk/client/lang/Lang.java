package me.lpk.client.lang;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.google.gson.reflect.TypeToken;

import me.lpk.client.Client;
import me.lpk.client.Loadable;
import me.lpk.client.plugin.Plugin;

public class Lang implements Loadable {

	/**
	 * File that stores plugin preferred language translations.
	 */
	private File langFile;

	/**
	 * Map of translations.
	 */
	private Map<String, String> translations = new HashMap<>();
	private Map<Class<? extends Plugin>, PluginTranslation> translationsForPlugins = new HashMap<>();

	/**
	 * The language suffix. Example:
	 * <ul>
	 * <li>en_us - English</li>
	 * </ul>
	 */
	private String language = "en_us";

	/**
	 * Gson serialization.
	 */
	private static final Type TYPE_STRING = new TypeToken<Map<String, String>>() {
	}.getType();

	// ---------------------------- //
	// ----- INSTANCE METHODS ----- //
	// ---------------------------- //

	/**
	 * Gets the translated text.
	 * 
	 * @param translationKey
	 *            Key value.
	 * @return
	 */
	public String get(String translationKey) {
		return translations.getOrDefault(translationKey, translationKey);
	}

	/**
	 * Updates the translation data for the given plugin. This process only
	 * reads and associates data from the translations map to the given plugin.
	 * The translations map must be populated beforehand.
	 * 
	 * @param plugin
	 *            The plugin class.
	 * @param translationKey
	 *            The translation key for the plugin. Syntax is
	 *            <i>"mod.mod_name"</i>.
	 */
	public void updatePluginData(Class<? extends Plugin> plugin, String translationKey) {
		String nameKey = translationKey + ".name";
		String descKey = translationKey + ".desc";
		String name = translations.get(nameKey);
		String desc = translations.get(descKey);
		PluginTranslation translation = new PluginTranslation(translationKey, name, desc);
		translationsForPlugins.put(plugin, translation);
	}

	/**
	 * Retrieves an object <i>(PluginTranslation)</i> that contains translation
	 * data for the given plugin.
	 * 
	 * @param plugin
	 * @return
	 */
	public PluginTranslation getPluginData(Class<? extends Plugin> plugin) {
		return translationsForPlugins.get(plugin);
	}

	/**
	 * Called when a translation file couldn't be loaded. Populates the
	 * translations map then saves it.
	 */
	private void createInitialLangFile() {
		//
		// Misc strings
		translations.put("require.creative", "Requires creative!");
		// Mod categories
		translations.put("modtype.movement", "Movement");
		translations.put("modtype.combat", "Combat");
		translations.put("modtype.world", "World");
		translations.put("modtype.render", "Render");
		translations.put("modtype.inventory", "Inventory");
		translations.put("modtype.other", "Other");
		// Mods
		translations.put("mod.fly.name", "Fly");
		translations.put("mod.fly.desc", "Lets you fly around");
		translations.put("mod.tracer.name", "Tracers");
		translations.put("mod.tracer.desc", "Draw lines to entities around you");
		translations.put("mod.fastmine.name", "FastMine");
		translations.put("mod.fastmine.desc", "Makes you dig faster");
		translations.put("mod.builder.name", "Builder");
		translations.put("mod.builder.desc", "Makes macro-building in creative easy");
		translations.put("mod.nofall.name", "Nofall");
		translations.put("mod.nofall.desc", "Prevents fall damage");
		translations.put("mod.aitest.name", "AI_TEST");
		translations.put("mod.aitest.desc", "Mob AI Implementation, testing implementation");
		onUnload();
	}

	@Override
	public void onLoad() {
		File langFile = getLangFile();
		if (!Client.publicBuild || !langFile.exists()) {
			createInitialLangFile();
		} else {
			try {
				// TODO: First update language type before pulling from maps
				loadMap(langFile, translations, TYPE_STRING);
			} catch (IOException e) {
				Client.logErr("There was an issue loading the language file: " + getLangFile().getAbsolutePath());
			}
		}
	}

	@Override
	public void onUnload() {
		try {
			FileUtils.write(getLangFile(), Client.gson().toJson(translations, TYPE_STRING));
		} catch (IOException e) {
			Client.logErr("There was an issue saving the language file: " + getLangFile().getAbsolutePath());
		}
	}

	/**
	 * Returns the language file for string conversion data.
	 * 
	 * @return
	 */
	private File getLangFile() {
		if (langFile == null) {
			langFile = new File(Client.getDirectory(), "lang_" + language + ".json");
		}
		return langFile;
	}
}
