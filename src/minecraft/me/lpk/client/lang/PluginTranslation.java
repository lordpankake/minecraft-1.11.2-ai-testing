package me.lpk.client.lang;

public class PluginTranslation {
	private final String base;
	private final String name;
	private final String desc;

	public PluginTranslation(String base, String name, String desc) {
		this.base = base;
		this.name = name;
		this.desc = desc;
	}

	public String getTranslationKey() {
		return base;
	}

	public String getName() {
		return name;
	}

	public String getDesc() {
		return desc;
	}

}
