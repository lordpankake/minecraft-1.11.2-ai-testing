package me.lpk.client;

import me.lpk.client.events.client.*;
import me.lpk.client.events.player.*;
import me.lpk.client.events.world.*;
import me.lpk.client.keybind.Keybind;
import me.lpk.client.plugin.Plugin;
import me.lpk.event.Event;
import me.lpk.event.EventSystem;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.network.Packet;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;

public class Events {
	public static void onPlayerTick() {
		fire(EventTick.class);
	}

	public static void onRender3DNoBobbing(float partialTicks) {
		EventRender3DImposed e = get(EventRender3DImposed.class);
		e.partialTicks = partialTicks;
		fire(e);
	}
	public static void onRender3DWithBobbing(float partialTicks) {
		EventRender3DAligned e = get(EventRender3DAligned.class);
		e.partialTicks = partialTicks;
		fire(e);
	}

	public static void onPlayerMotion(boolean preMotion) {
		EventMotion e = get(EventMotion.class);
		e.preUpdate = preMotion;
		fire(e);
	}

	public static String onChatSend(String msg) {
		EventChat e = get(EventChat.class);
		e.msg = msg;
		fire(e);
		return e.msg;
	}

	public static Packet<?> onPacketSend(Packet<?> packetIn) {
		EventPacket e = get(EventPacket.class);
		e.packet = packetIn;
		fire(e);
		return e.packet;
	}

	public static void onKeyPress(int i, boolean isDown) {
		if (isDown) {
			Client.logInfo("TODO: Update onKeyPress so that masked keys can cancel non-masked key presses");
			for (Plugin plugin : Client.getPlugins().getPlugins()) {
				// Not all mods have keybinds.
				// Skip those that don't
				Keybind key = plugin.getKeybind();
				if (key == null)
					continue;
				// Are all the required keys down?
				// If so toggle
				if (key.isActive())
					plugin.toggle();
			}
		}
	}

	public static void onStartBreakBlock(IBlockState iblockstate, BlockPos pos) {
		EventStartBreakBlock event = get(EventStartBreakBlock.class);
		event.blockstate = iblockstate;
		event.pos = pos;
		fire(event);
	}

	public static float onBlockDamage(IBlockState iblockstate, BlockPos posBlock, float defaultDamage) {
		EventDamageBlock event = get(EventDamageBlock.class);
		event.blockstate = iblockstate;
		event.pos = posBlock;
		event.damage = defaultDamage;
		fire(event);
		return event.damage;
	}

	public static boolean onBlockBreak(BlockPos pos, IBlockState blockstate, EnumFacing face) {
		EventDestroyBlock event = get(EventDestroyBlock.class);
		event.pos = pos;
		event.blockstate = blockstate;
		event.face = face;
		event.doBreak = true;
		fire(event);
		return event.doBreak;
	}

	public static boolean onBlockPlace(ItemStack itemstack, BlockPos pos, EnumFacing face) {
		if (itemstack == null || !(itemstack.getItem() instanceof ItemBlock)) {
			// Continue on with vanilla logic
			return true;
		}
		EventPlaceBlock event = get(EventPlaceBlock.class);
		event.block = ((ItemBlock) itemstack.getItem()).getBlock();
		event.pos = pos;
		event.face = face;
		event.doPlace = true;
		fire(event);
		return event.doPlace;
	}

	/**
	 * Returns false if an event subscriber has cancelled picking up the item.
	 * 
	 * @param itemstack
	 * @return
	 */
	public static boolean onItemPickup(ItemStack itemstack) {
		EventItemPickup e = get(EventItemPickup.class);
		e.itemStack = itemstack;
		fire(e);
		return !e.isCancelled();
	}

	/**
	 * Inline generics for cleaner code regarding event retrieval.
	 * 
	 * @param c
	 * @return
	 */
	private static <T extends Event> T get(Class<T> c) {
		return (T) EventSystem.getEvent(c);
	}

	/**
	 * Fire event.
	 * 
	 * @param c
	 */
	private static void fire(Class<? extends Event> c) {
		EventSystem.fire(c);
	}

	/**
	 * Fire event.
	 * 
	 * @param e
	 */
	private static void fire(Event e) {
		EventSystem.fire(e);
	}
}
