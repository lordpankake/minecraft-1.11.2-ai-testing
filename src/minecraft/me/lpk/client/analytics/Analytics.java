package me.lpk.client.analytics;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.joda.time.Instant;
import org.joda.time.Interval;

import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.entity.Entity;
import net.minecraft.util.DamageSource;

import com.google.gson.reflect.TypeToken;

import me.lpk.client.Client;
import me.lpk.client.Loadable;
import me.lpk.client.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class Analytics implements Loadable {
	/**
	 * Records the number of times a plugin has been toggled.
	 */
	private final Map<String, Integer> pluginUses = new HashMap<String, Integer>();

	/**
	 * Records the time (in seconds) a plugin has been used.
	 */
	private final Map<String, Long> pluginUsage = new HashMap<String, Long>();

	/**
	 * Records the time (in seconds) a player was on a server.
	 */
	private final Map<String, Long> serverUsage = new HashMap<String, Long>();

	/**
	 * File that stores plugin usage time.
	 */
	private File filePluginTime;

	/**
	 * File that stores plugin toggles.
	 */
	private File filePluginUses;

	/**
	 * File that stores plugin toggles.
	 */
	private File fileServerTime;

	/**
	 * Gson serialization.
	 */
	private static final Type TYPE_INT = new TypeToken<Map<String, Integer>>() {
	}.getType();

	/**
	 * Gson serialization.
	 */
	private static final Type TYPE_LONG = new TypeToken<Map<String, Long>>() {
	}.getType();

	/**
	 * Time that the client was loaded at.
	 */
	public static DateTime START = DateTime.now();

	/**
	 * Loads stored statistic data.
	 */
	@Override
	public void onLoad() {
		try {
			loadMap(getServerUsageFile(), serverUsage, TYPE_LONG);
			loadMap(getPluginUsageFile(), pluginUsage, TYPE_LONG);
			loadMap(getPluginUsesFile(), pluginUses, TYPE_INT);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Saves current statistic data.
	 */
	@Override
	public void onUnload() {
		try {
			FileUtils.write(getPluginUsesFile(), Client.gson().toJson(serverUsage, TYPE_LONG));
			FileUtils.write(getPluginUsageFile(), Client.gson().toJson(pluginUsage, TYPE_LONG));
			FileUtils.write(getPluginUsesFile(), Client.gson().toJson(pluginUses, TYPE_INT));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Increments the recorded number of times a plugin has been used.
	 * 
	 * @param plugin
	 */
	public void recordPluginToggle(Plugin plugin) {
		String key = getPluginKey(plugin);
		if (pluginUses.containsKey(key)) {
			// Increment uses
			pluginUses.put(key, pluginUses.get(key).intValue() + 1);
		} else {
			// Initial use
			pluginUses.put(key, 1);
		}
	}

	/**
	 * Increases the duration of plugin use-time.
	 * 
	 * @param plugin
	 * @param time
	 *            Time in seconds active
	 */
	public void recordPluginTime(Plugin plugin) {
		String key = getPluginKey(plugin);
		long time = plugin.getTimeUsed();
		if (pluginUsage.containsKey(key)) {
			// Increment time
			pluginUsage.put(key, pluginUsage.get(key).longValue() + time);
		} else {
			// Initial time
			pluginUsage.put(key, time);
		}
	}

	/**
	 * Gets the elapsed time between the client startup and the current time.
	 * 
	 * @return
	 */
	public static long getTimeRun() {
		Interval interval = new Interval(START.toInstant(), new Instant());
		return interval.toDurationMillis();
	}

	/**
	 * Called when a player joins a server and the ServerData is set.
	 * 
	 * @param data
	 */
	public void onJoin(ServerData data) {
		// TODO: Keep track of who's online where so users can team up
		// Essentially like Steam's "Join Friend's game"
	}

	/**
	 * Called when a player leaves a server.
	 * 
	 * @param data
	 */
	public void onLeave(ServerData data) {
		long time = data.getTimeUsed();
		String key = data.serverIP;
		if (serverUsage.containsKey(key)) {
			// Increment time
			serverUsage.put(key, serverUsage.get(key).longValue() + time);
		} else {
			// Initial time
			serverUsage.put(key, time);
		}
	}

	/**
	 * Called when the player kills an entity.
	 * 
	 * @param entity
	 */
	public void onKill(Entity entity, DamageSource attackUsed) {
		// TODO: Rival system
	}

	/**
	 * Called when the player is killed by an entity.
	 * 
	 * @param target
	 */
	public void onKilledBy(DamageSource source) {
		// TODO: Rival system
	}

	/**
	 * Called when the player is hurt by an entity.
	 * 
	 * @param source
	 */
	public void onDamaged(DamageSource source) {
		// TODO: Rival system
	}

	/**
	 * Returns the statistics file for plugin usage duration.
	 * 
	 * @return
	 */
	private File getPluginUsageFile() {
		if (filePluginTime == null) {
			filePluginTime = new File(Client.getDirectory(), "Stats_time.json");
		}
		return filePluginTime;
	}

	/**
	 * Returns the statistics file for plugin toggles.
	 * 
	 * @return
	 */
	private File getPluginUsesFile() {
		if (filePluginUses == null) {
			filePluginUses = new File(Client.getDirectory(), "plugin_uses.json");
		}
		return filePluginUses;
	}

	/**
	 * Returns the statistics file for time spent on servers.
	 * 
	 * @return
	 */
	private File getServerUsageFile() {
		if (fileServerTime == null) {
			fileServerTime = new File(Client.getDirectory(), "server_usage.json");
		}
		return fileServerTime;
	}

	/**
	 * Gets the non-language specific identifier for the given plugin.
	 * 
	 * @param plugin
	 * @return
	 */
	private final static String getPluginKey(Plugin plugin) {
		return Client.getLang().getPluginData(plugin.getClass()).getTranslationKey();
	}
}
