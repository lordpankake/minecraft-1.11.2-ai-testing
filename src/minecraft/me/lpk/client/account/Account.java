package me.lpk.client.account;

import java.net.Proxy;
import com.mojang.authlib.Agent;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.exceptions.InvalidCredentialsException;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication;

import me.lpk.client.Client;
import net.minecraft.client.Minecraft;
import net.minecraft.util.Session;

public class Account {
	public static void login(String userInput, String password) {
		if (userInput == null || userInput.length() <= 0 || password == null || password.length() <= 0) {
			return;
		}
		YggdrasilAuthenticationService sessService = new YggdrasilAuthenticationService(Proxy.NO_PROXY, "");
		YggdrasilUserAuthentication userAuth = (YggdrasilUserAuthentication) sessService.createUserAuthentication(Agent.MINECRAFT);
		userAuth.setUsername(userInput);
		userAuth.setPassword(password);
		if (userAuth.canLogIn()) {
			try {
				userAuth.logIn();
				String username = userAuth.getSelectedProfile().getName();
				String playerID = userAuth.getSelectedProfile().getId().toString();
				String token = userAuth.getAuthenticatedToken();
				String sessionType = userInput.contains("@") ? "MOJANG" : "LEGACY";
				Session session = new Session(username, playerID, token, sessionType);
				Minecraft.getMinecraft().session = session;
			} catch (InvalidCredentialsException e) {
				Client.logErr("Invalid credentials for: " + userInput);
			} catch (AuthenticationException e) {
				Client.logErr("Authentication error for: " + userInput);
			}
		} else {
			Client.logErr("Unable to log in, auth service denied login access. Try again later.");
		}
	}
}
