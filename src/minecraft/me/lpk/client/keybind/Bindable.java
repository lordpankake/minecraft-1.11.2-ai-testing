package me.lpk.client.keybind;

public interface Bindable {
	Keybind getKeybind();

	void setKeybind(Keybind bind);

	default void setKeybind(int key) {
		getKeybind().setKey(key);
	}

	default void setKeyMask(Keybind mask) {
		getKeybind().setMask(mask);
	}
}
