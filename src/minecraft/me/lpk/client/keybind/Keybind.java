package me.lpk.client.keybind;

import org.lwjgl.input.Keyboard;

public class Keybind {
	/**
	 * LWJGL key code.
	 */
	private int key;
	/**
	 * Mask for keybind.
	 */
	private Keybind mask;

	/**
	 * Creates a keybind from the given key.
	 * 
	 * @param key
	 * @return
	 */
	public static Keybind create(int key) {
		Keybind bind = new Keybind();
		bind.key = key;
		return bind;
	}

	/**
	 * Returns if the designated key code is down. This does not account for the
	 * mask.
	 * 
	 * @return
	 */
	public boolean isKeyDown() {
		return Keyboard.isKeyDown(key);
	}

	/**
	 * Returns if the mask's key is down.
	 * 
	 * @return
	 */
	public boolean isMaskDown() {
		return mask == null ? true : mask.isActive();
	}

	/**
	 * Returns if the designated key and mask are both down.
	 * 
	 * @return
	 */
	public boolean isActive() {
		return isKeyDown() && isMaskDown();
	}

	/**
	 * Returns the name of the designated key code. This does not include the
	 * mask.
	 * 
	 * @return
	 */
	public String getKeyName() {
		return Keyboard.getKeyName(key);
	}

	/**
	 * Returns the combined names of the mask and the designated key code.
	 * 
	 * @return
	 */
	public String getDisplayName() {
		String key = getKeyName();
		String mask = this.mask == null ? "" : this.mask.getDisplayName() + " + ";
		return mask + key;
	}

	/**
	 * Returns the designated key ocde.
	 * 
	 * @return
	 */
	public int getKey() {
		return key;
	}

	/**
	 * Sets the designated key ocde.
	 * 
	 * @param key
	 */
	public void setKey(int key) {
		this.key = key;
	}

	/**
	 * Returns the keybind mask. May be null.
	 * 
	 * @return
	 */
	public Keybind getMask() {
		return mask;
	}

	/**
	 * Sets the keybind mask.
	 * 
	 * @param mask
	 */
	public void setMask(Keybind mask) {
		this.mask = mask;
	}
}
