package me.lpk.client.plugin.interfaces;

import net.minecraft.network.Packet;

public interface PacketUser extends MinecraftUser {
	/**
	 * Send a packet.
	 * 
	 * @param packet
	 *            Packet to send.
	 */
	@SuppressWarnings("all")
	default void send(Packet packet) {
		mc.getConnection().getNetworkManager().sendPacket(packet, null, null);
	}
}
