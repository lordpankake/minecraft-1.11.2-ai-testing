package me.lpk.client.plugin.interfaces;

import net.minecraft.util.text.TextFormatting;

public interface ChatUser extends MinecraftUser {
	/**
	 * Print a normal chat message.
	 * 
	 * @param msg
	 *            The message.
	 */
	default void info(String msg) {
		mc.player.printChatMessage(msg);
	}

	/**
	 * Print an error message to chat.
	 * 
	 * @param msg
	 *            The err message.
	 */
	default void error(String msg) {
		mc.player.printChatMessage(TextFormatting.RED + msg);
	}
}
