package me.lpk.client.plugin.interfaces;

import net.minecraft.client.Minecraft;

public interface MinecraftUser {
	/**
	 * Minecraft reference.
	 */
	Minecraft mc = Minecraft.getMinecraft();
}
