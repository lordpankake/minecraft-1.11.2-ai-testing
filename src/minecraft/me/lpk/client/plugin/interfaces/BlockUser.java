package me.lpk.client.plugin.interfaces;

import net.minecraft.block.state.IBlockState;
import net.minecraft.network.play.client.CPacketPlayerDigging;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;

public interface BlockUser extends PacketUser {
	/**
	 * Destroy a block given by it's position and the face that receives the
	 * destroy request.
	 * 
	 * @param pos
	 *            Block position.
	 * @param face
	 *            Face that receives destroy.
	 */
	default void breakBlock(BlockPos pos, EnumFacing face) {
		send(new CPacketPlayerDigging(CPacketPlayerDigging.Action.START_DESTROY_BLOCK, pos, face));
		mc.playerController.onPlayerDestroyBlock(pos, face, true);
	}

	/**
	 * Place a block at the given position.
	 * 
	 * @param pos
	 *            Position to place block at.
	 * @param face
	 *            Face to place block on, should not point to an air block.
	 * @param hand
	 *            Hand with the item to place.
	 */
	default void placeBlock(BlockPos pos, EnumFacing face, EnumHand hand) {
		mc.playerController.processRightClickBlock(mc.player, mc.world, pos, face, mc.objectMouseOver.hitVec, hand, true);
	}

	/**
	 * Retrieve block information from the given position.
	 * 
	 * @param newPos
	 * @return
	 */
	default IBlockState getBlockState(BlockPos newPos) {
		return mc.world.getBlockState(newPos);
	}
}
