package me.lpk.client.plugin.interfaces;

import java.util.Set;

import net.minecraft.entity.ai.EntityAIBase;

public interface AIUser extends MinecraftUser {
	/**
	 * Override player control with the AI system.
	 */
	default void setupAI() {
		mc.player.isUsingAI = true;
	}

	/**
	 * Add a task to the player's AI task set.
	 * 
	 * @param owner
	 *            Plugin that spawned the task.
	 * @param priority
	 *            Priority of the task.
	 * @param task
	 *            The task.
	 */
	default void addTask(int priority, EntityAIBase task) {
		mc.player.tasks.addTask(this, priority, task);
	}

	/**
	 * Remove the given task from the player's AI task set.
	 * 
	 * @param task
	 *            The task.
	 */
	default void removeTask(EntityAIBase task) {
		mc.player.tasks.removeTask(this, task);
	}

	/**
	 * End all tasks spawned by the given plugin. If this empties the player's
	 * AI task set, normal player control is resumed and the AI system is
	 * disabled.
	 * 
	 * @param owner
	 *            The plugin.
	 */
	default void endTasks() {
		// Retrieve copied set, prevent iteration exception
		Set<EntityAIBase> tasks = mc.player.tasks.getPluginTasks(this, true);
		for (EntityAIBase task : tasks) {
			mc.player.tasks.removeTask(this, task);
		}
		// If no more tasks, end AI control.
		if (mc.player.tasks.taskEntries.isEmpty()) {
			mc.player.isUsingAI = false;
		}
	}
}
