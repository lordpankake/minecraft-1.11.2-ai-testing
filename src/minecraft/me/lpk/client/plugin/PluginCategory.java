package me.lpk.client.plugin;

import me.lpk.client.Client;

public enum PluginCategory {
	MOVEMENT("modtype.movement"), COMBAT("modtype.combat"), WORLD("modtype.world"), RENDER("modtype.render"), INVENTORY("modtype.inventory"), OTHER("modtype.other");

	private String translationKey;

	PluginCategory(String translationKey) {
		this.translationKey = translationKey;
	}

	public String getDisplayName() {
		return Client.getLang().get(this.translationKey);
	}
}
