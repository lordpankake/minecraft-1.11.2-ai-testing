package me.lpk.client.plugin;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import me.lpk.client.Loadable;
import me.lpk.reflection.ClasspathInstantiator;

public class Plugins implements Loadable {
	private final Map<Class, Plugin> plugins = new HashMap<>();

	@Override
	public void onLoad() {
		try {
			ClasspathInstantiator<Plugin> cpInst = ClasspathInstantiator.create(Plugin.class.getPackage().getName(), Plugin.class, true);
			cpInst.forEachClass(ClassLoader.getSystemClassLoader(), (Plugin p) -> register(p));
		} catch (IllegalArgumentException | IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onUnload() {
		for (Plugin plugin : plugins.values()) {
			plugin.onUnload();
		}
	}

	private void register(Plugin plugin) {
		// Add plugin instance
		plugins.put(plugin.getClass(), plugin);
		plugin.onLoad();
	}

	public Plugin getPlugin(Class<? extends Plugin> pluginClass) {
		return plugins.get(pluginClass);
	}

	public Collection<Plugin> getPlugins() {
		return plugins.values();
	}
}
