package me.lpk.client.plugin;

import me.lpk.client.Client;
import me.lpk.client.Loadable;
import me.lpk.client.Timeable;
import me.lpk.client.Togglable;
import me.lpk.client.keybind.Bindable;
import me.lpk.client.keybind.Keybind;
import me.lpk.client.lang.PluginTranslation;
import me.lpk.client.lang.Translatable;
import me.lpk.client.lang.Translator;
import me.lpk.client.plugin.interfaces.ChatUser;
import me.lpk.client.plugin.interfaces.MinecraftUser;
import me.lpk.event.EventSystem;
import me.lpk.util.Timer;

public class Plugin implements Loadable, Togglable, MinecraftUser, Timeable, Bindable, Translatable, ChatUser, Translator {
	/**
	 * Timer used for recording use-time.
	 */
	private final Timer timer = new Timer();

	/**
	 * Status of the toggleable plugin.
	 */
	private boolean status;

	/**
	 * Keybind.
	 */
	private Keybind bind;

	/**
	 * Name of the plugin.
	 */
	private String name;

	/**
	 * Description of the plugin.
	 */
	private String desc;

	/**
	 * Category of the plugin.
	 */
	private PluginCategory category;

	/**
	 * Sets up the plugin with a PluginConstructor. This object can be created
	 * from {@link #create(String, int)}.
	 * 
	 * @param data
	 */
	protected final void setup(PluginConstructor data) {
		setup(data.translation, data.keybind, data.type);
	}

	/**
	 * Sets up the plugin with the given data.
	 * 
	 * @param translation
	 * @param bind
	 */
	private final void setup(PluginTranslation translation, Keybind bind, PluginCategory type) {
		this.name = translation.getName();
		this.desc = translation.getDesc();
		this.category = type;
		this.bind = bind;
	}

	@Override
	public void toggle() {
		this.status = !this.status;
		info(getName() + ": " + status);
		if (getStatus()) {
			handleEnable();
			onEnable();
		} else {
			handleDisable();
			shutdownAI();
		}
	}

	/**
	 * Subscribes the plugin to events and increments the number of times this
	 * plugin was used.
	 */
	private final void handleEnable() {
		// Register event listening
		EventSystem.subscribe(this);
		// Record number of times this plugin has been used
		Client.getAnalytics().recordPluginToggle(this);
		// Reset timer recording time this plugin has been used
		timer.reset();
	}

	/**
	 * Unsubscribes the plugin to events and increments the time this plugin was
	 * in use.
	 */
	private final void handleDisable() {
		// Unregister event listening
		EventSystem.unsubscribe(this);
		// Record time this plugin has been used
		Client.getAnalytics().recordPluginTime(this);
	}

	@Override
	public void onEnable() {

	}

	@Override
	public void shutdownAI() {

	}

	@Override
	public void onLoad() {

	}

	@Override
	public void onUnload() {

	}

	@Override
	public void onLanguageUpdate() {
		PluginTranslation translation = Client.getLang().getPluginData(this.getClass());
		this.name = translation.getName();
		this.desc = translation.getDesc();
	}

	// --------------------------- //
	// --- Getters and Setters --- //
	// --------------------------- //

	/**
	 * Returns the name of the plugin.
	 */
	public final String getName() {
		return name;
	}

	/**
	 * Returns the description of the plugin.
	 */
	public final String getDesc() {
		return desc;
	}

	/**
	 * Returns the category of the plugin.
	 */
	public final PluginCategory getCategory() {
		return category;
	}

	@Override
	public void setStatus(boolean status) {
		this.status = status;
	}

	@Override
	public boolean getStatus() {
		return status;
	}

	@Override
	public Timer getTimer() {
		return timer;
	}

	@Override
	public Keybind getKeybind() {
		return bind;
	}

	@Override
	public void setKeybind(Keybind bind) {
		this.bind = bind;
	}

	/**
	 * Creates an object to be passed to {@link #setup(PluginConstructor)}.
	 * 
	 * @param translationKey
	 * @param key
	 * @return
	 */
	protected final PluginConstructor create(String translationKey, int key, PluginCategory type) {
		// Register the plugin class so it can get translations.
		Client.getLang().updatePluginData(getClass(), translationKey);
		// Construct and return the object
		return new PluginConstructor(Client.getLang().getPluginData(this.getClass()), Keybind.create(key), type);
	}

	static final class PluginConstructor {
		final Keybind keybind;
		final PluginTranslation translation;
		final PluginCategory type;

		public PluginConstructor(PluginTranslation translation, Keybind keybind, PluginCategory type) {
			this.translation = translation;
			this.keybind = keybind;
			this.type = type;
		}

	}
}
