package me.lpk.client.plugin.impl;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import me.lpk.client.events.client.EventRender3DAligned;
import me.lpk.client.events.world.EventDestroyBlock;
import me.lpk.client.events.world.EventPlaceBlock;
import me.lpk.client.plugin.Plugin;
import me.lpk.client.plugin.PluginCategory;
import me.lpk.client.plugin.interfaces.BlockUser;
import me.lpk.client.plugin.interfaces.PositionUser;
import me.lpk.client.plugin.interfaces.RenderUser;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;

public class Builder extends Plugin implements BlockUser, PositionUser, RenderUser {
	public int radius = 3;
	public boolean destroy = true;
	public boolean build = true;

	public Builder() {
		setup(create("mod.builder", Keyboard.KEY_L, PluginCategory.WORLD));
	}

	@Override
	public void onEnable() {
		// This mod is designed to ONLY work with creative
		// TODO: Client can hide mod if certain conditions are not met
		if (mc.playerController.isNotCreative()) {
			error(getName() + ":" + translate("require.creative"));
			toggle();
		}
	}

	public void onRender3D(EventRender3DAligned e) {
		if (mc.objectMouseOver == null || mc.objectMouseOver.typeOfHit != RayTraceResult.Type.BLOCK || mc.objectMouseOver.hitVec == null) {
			return;
		}
		this.standardRender(true);
		this.setSmoothLines(true);
		GL11.glLineWidth(5f);
		boolean isDestroying = mc.player.inventory.getCurrentItem().getItem() instanceof ItemBlock;
		EnumFacing face = mc.objectMouseOver.sideHit;
		BlockPos pos = mc.objectMouseOver.getBlockPos();

		int xSpread = 0;
		int ySpread = 0;
		int zSpread = 0;
		int xDirOffset = 0;
		int yDirOffset = 0;
		int zDirOffset = 0;
		EnumFacing placeFace = face;
		switch (face) {
		case NORTH:
			zDirOffset = -1;
			xSpread = ySpread = radius;
			break;
		case SOUTH:
			zDirOffset = 1;
			xSpread = ySpread = radius;
			break;
		case EAST:
			xDirOffset = 1;
			zSpread = ySpread = radius;
			break;
		case WEST:
			xDirOffset = -1;
			zSpread = ySpread = radius;
			break;
		case DOWN:
			yDirOffset = -1;
			xSpread = zSpread = radius;
			break;
		case UP:
			yDirOffset = 1;
			xSpread = zSpread = radius;
			break;
		}
		// Determine hand to use
		EnumHand hand = null;
		ItemStack stackMain = mc.player.getHeldItem(EnumHand.MAIN_HAND);
		if (stackMain == null) {
			hand = EnumHand.OFF_HAND;
		} else if (stackMain.getItem() instanceof ItemBlock) {
			hand = EnumHand.MAIN_HAND;
		}
		// Iterate spread, place or destroy blocks where needed
		for (int y = -ySpread; y <= ySpread; y++) {
			for (int x = -xSpread; x <= xSpread; x++) {
				for (int z = -zSpread; z <= zSpread; z++) {
					BlockPos newPos = new BlockPos(pos.getX() + x, pos.getY() + y, pos.getZ() + z);
					BlockPos shiftedNewPos = new BlockPos(pos.getX() + x + xDirOffset, pos.getY() + y + yDirOffset, pos.getZ() + z + zDirOffset);
					boolean isAir = getBlockState(newPos).getMaterial() == Material.AIR;
					boolean isShiftedAir = getBlockState(shiftedNewPos).getMaterial() == Material.AIR;
					// Visibility required
					if ((isDestroying && !isShiftedAir) || (!isDestroying && !isShiftedAir) || outOfRange(newPos)) {
						continue;
					}
					// Break if not air, build if air.
					if (isDestroying && !isAir) {

					} else if (!isDestroying && !isAir) {
						switch (face) {
						case DOWN:
							shiftedNewPos = shiftedNewPos.up();
							break;
						case EAST:
							shiftedNewPos = shiftedNewPos.west();
							break;
						case NORTH:
							shiftedNewPos = shiftedNewPos.south();
							break;
						case SOUTH:
							shiftedNewPos = shiftedNewPos.north();
							break;
						case UP:
							shiftedNewPos = shiftedNewPos.down();
							break;
						case WEST:
							shiftedNewPos = shiftedNewPos.east();
							break;
						}
					} else {
						continue;
					}
					this.renderBlock(shiftedNewPos);
				}
			}
		}
		this.setSmoothLines(false);
		this.standardRender(false);
	}

	public void onBlockBreakStart(EventDestroyBlock e) {
		// If destroy is disabled, skip
		if (destroy)
			handle(true, e.face, e.pos, null);
	}

	public void onItemPlace(EventPlaceBlock e) {
		// If build is disabled, skip
		if (build)
			handle(false, e.face, e.pos, e.block);
	}

	public void handle(boolean isDestroying, EnumFacing face, BlockPos pos, Block block) {
		int xSpread = 0;
		int ySpread = 0;
		int zSpread = 0;
		int xDirOffset = 0;
		int yDirOffset = 0;
		int zDirOffset = 0;
		EnumFacing placeFace = face;
		switch (face) {
		case NORTH:
			zDirOffset = -1;
			xSpread = ySpread = radius;
			break;
		case SOUTH:
			zDirOffset = 1;
			xSpread = ySpread = radius;
			break;
		case EAST:
			xDirOffset = 1;
			zSpread = ySpread = radius;
			break;
		case WEST:
			xDirOffset = -1;
			zSpread = ySpread = radius;
			break;
		case DOWN:
			yDirOffset = -1;
			xSpread = zSpread = radius;
			break;
		case UP:
			yDirOffset = 1;
			xSpread = zSpread = radius;
			break;
		}
		// Determine hand to use
		EnumHand hand = null;
		ItemStack stackMain = mc.player.getHeldItem(EnumHand.MAIN_HAND);
		if (stackMain == null) {
			hand = EnumHand.OFF_HAND;
		} else if (stackMain.getItem() instanceof ItemBlock) {
			hand = EnumHand.MAIN_HAND;
		}
		// Iterate spread, place or destroy blocks where needed
		for (int y = -ySpread; y <= ySpread; y++) {
			for (int x = -xSpread; x <= xSpread; x++) {
				for (int z = -zSpread; z <= zSpread; z++) {
					BlockPos newPos = new BlockPos(pos.getX() + x, pos.getY() + y, pos.getZ() + z);
					BlockPos shiftedNewPos = new BlockPos(pos.getX() + x + xDirOffset, pos.getY() + y + yDirOffset, pos.getZ() + z + zDirOffset);
					boolean isAir = getBlockState(newPos).getMaterial() == Material.AIR;
					boolean isShiftedAir = getBlockState(shiftedNewPos).getMaterial() == Material.AIR;
					// Visibility required
					if ((isDestroying && !isShiftedAir) || (!isDestroying && !isShiftedAir) || outOfRange(newPos)) {
						continue;
					}
					// Break if not air, build if air.
					if (isDestroying && !isAir) {
						breakBlock(newPos, face);
					} else if (!isDestroying && !isAir) {
						switch (face) {
						case DOWN:
							shiftedNewPos = shiftedNewPos.up();
							break;
						case EAST:
							shiftedNewPos = shiftedNewPos.west();
							break;
						case NORTH:
							shiftedNewPos = shiftedNewPos.south();
							break;
						case SOUTH:
							shiftedNewPos = shiftedNewPos.north();
							break;
						case UP:
							shiftedNewPos = shiftedNewPos.down();
							break;
						case WEST:
							shiftedNewPos = shiftedNewPos.east();
							break;
						}
						placeBlock(shiftedNewPos, placeFace, hand);
					}
				}
			}
		}
	}

}
