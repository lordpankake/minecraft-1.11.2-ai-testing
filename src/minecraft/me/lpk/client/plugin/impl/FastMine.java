package me.lpk.client.plugin.impl;

import org.lwjgl.input.Keyboard;

import me.lpk.client.events.player.EventTick;
import me.lpk.client.events.world.EventDamageBlock;
import me.lpk.client.events.world.EventStartBreakBlock;
import me.lpk.client.plugin.Plugin;
import me.lpk.client.plugin.PluginCategory;

public class FastMine extends Plugin {
	public int blockHitDelay = 0;
	public float initialDamageMultiplier = 2.5f;
	public float damageIncrementMultiplier = 1.5f;

	public FastMine() {
		setup(create("mod.fastmine", Keyboard.KEY_V, PluginCategory.WORLD));
	}

	public void onTick(EventTick e) {
		mc.playerController.blockHitDelay = blockHitDelay;
	}

	public void onBlockBreakStart(EventStartBreakBlock e) {
		float initialDamage = e.blockstate.getPlayerRelativeBlockHardness(mc.player, mc.world, e.pos);
		initialDamage *= initialDamageMultiplier;
		mc.playerController.curBlockDamageMP = initialDamage;
	}

	public void onBlockBreakStart(EventDamageBlock e) {
		e.damage *= damageIncrementMultiplier;
	}
}
