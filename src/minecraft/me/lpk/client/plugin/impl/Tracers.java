package me.lpk.client.plugin.impl;

import org.lwjgl.input.Keyboard;

import me.lpk.client.events.client.EventRender3DImposed;
import me.lpk.client.plugin.Plugin;
import me.lpk.client.plugin.PluginCategory;
import me.lpk.client.plugin.interfaces.BlockUser;
import me.lpk.client.plugin.interfaces.PositionUser;
import me.lpk.client.plugin.interfaces.RenderUser;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.math.Vec3d;

public class Tracers extends Plugin implements BlockUser, PositionUser, RenderUser {

	public Tracers() {
		setup(create("mod.tracer", Keyboard.KEY_O, PluginCategory.RENDER));
	}

	public void onRender3D(EventRender3DImposed e) {
		Vec3d cursorVec = getPlayerCursorPoint(e.partialTicks);
		this.standardRender(true);
		this.drawLines(true);
		for (Entity entity : mc.world.loadedEntityList) {
			if (entity == mc.player)
				continue;
			if (!(entity instanceof EntityLivingBase))
				continue;
			Vec3d entityVec = mix(e.partialTicks, entity);
			Tracers.color(entity);
			this.vertex3(cursorVec);
			this.vertex3(entityVec);
		}
		this.drawLines(false);
		this.standardRender(false);
	}

	private static void color(Entity entity) {
		float dist = mc.player.getDistanceToEntity(entity);
		float factor = dist / 40F;
		if (factor > 1)
			factor = 1;
		float alpha = (float) (4f / dist);
		GlStateManager.color(2 - (factor * 2F), factor * 2F, 0, alpha);
	}
}
