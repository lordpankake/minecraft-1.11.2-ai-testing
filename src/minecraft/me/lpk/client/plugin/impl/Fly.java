package me.lpk.client.plugin.impl;

import org.lwjgl.input.Keyboard;

import me.lpk.client.events.player.EventTick;
import me.lpk.client.plugin.Plugin;
import me.lpk.client.plugin.PluginCategory;

public class Fly extends Plugin {
	public Fly() {
		setup(create("mod.fly", Keyboard.KEY_F, PluginCategory.MOVEMENT));
	}

	@Override
	public void shutdownAI() {
		mc.player.capabilities.isFlying = false;
	}

	public void onTick(EventTick e) {
		mc.player.capabilities.isFlying = true;
	}
}
