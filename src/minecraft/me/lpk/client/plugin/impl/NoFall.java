package me.lpk.client.plugin.impl;

import org.lwjgl.input.Keyboard;

import me.lpk.client.events.client.EventPacket;
import me.lpk.client.plugin.Plugin;
import me.lpk.client.plugin.PluginCategory;
import net.minecraft.network.play.client.CPacketPlayer;

public class NoFall extends Plugin {
	public NoFall() {
		setup(create("mod.nofall", Keyboard.KEY_N, PluginCategory.MOVEMENT));
	}

	public void onTick(EventPacket e) {
		if (e.packet instanceof CPacketPlayer) {
			((CPacketPlayer) e.packet).setOnGround(true);
		}
	}
}
