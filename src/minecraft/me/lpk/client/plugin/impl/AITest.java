package me.lpk.client.plugin.impl;

import org.lwjgl.input.Keyboard;

import me.lpk.client.events.player.EventTick;
import me.lpk.client.plugin.Plugin;
import me.lpk.client.plugin.PluginCategory;
import me.lpk.client.plugin.interfaces.AIUser;
import me.lpk.client.plugin.interfaces.ChatUser;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.passive.EntityVillager;

public class AITest extends Plugin implements AIUser, ChatUser {
	private Entity target;

	public AITest() {
		setup(create("mod.aitest", Keyboard.KEY_J, PluginCategory.OTHER));
	}

	public void onTick(EventTick ev) {
		if (target == null) {
			return;
		}
		EntityLivingBase e = (EntityLivingBase) target;
		if (e.getHealth() <= 0F || e.isDead) {
			target = null;
			return;
		}
		if (e.motionX > 0.05 || e.motionY > 0.05 || e.motionZ > 0.05) {
			mc.player.navigator.tryMoveToXYZ(e.posX, e.posY, e.posZ, mc.player.getAIMoveSpeed());
		}
	}

	@Override
	public void onEnable() {
		setupAI();
		addTask(1, new EntityAIBase() {

			@Override
			public boolean shouldExecute() {
				if (target != null) {
					if (mc.player.getDistanceToEntity(target) < 3.5F) {
						target = null;
					}
				} else {
					Entity tmp = null;
					for (Entity e : mc.world.loadedEntityList) {
						if (e != mc.player && mc.player.getDistanceToEntity(e) < 35) {
							if (e instanceof EntityVillager) {
								if (tmp == null) {
									tmp = e;
								} else if (mc.player.getDistanceToEntity(tmp) > mc.player.getDistanceToEntity(e)) {
									tmp = e;
								}
							}
						}
					}
					if (tmp != null) {
						target = tmp;
						info("Follow: " + target.getName());
						return true;
					}
				}

				return false;
			}

			@Override
			public boolean continueExecuting() {
				return !mc.player.navigator.noPath();
			}

			@Override
			public void startExecuting() {
				if (target != null) {
					mc.player.navigator.tryMoveToXYZ(target.posX, target.posY, target.posZ, mc.player.getAIMoveSpeed());
				}
			}
		});
	}

	public void onDisable() {
		endTasks();
		target = null;
	}
}
