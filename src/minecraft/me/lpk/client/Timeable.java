package me.lpk.client;

import me.lpk.util.Timer;

public interface Timeable {
	/**
	 * Returns the timer object.
	 */
	Timer getTimer();

	/**
	 * Returns the elapsed time <i>(in seconds)</i> the
	 */
	default long getTimeUsed() {
		return getTimer().elapsed() / 1000L;
	}

	/**
	 * Resets the amount of elapsed time.
	 */
	default void reset() {
		getTimer().reset();
	}
}
