package me.lpk.client.security;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class AES {
	public static byte[] salt = new byte[] { 1, 3, 3, 7 };

	public static String getIP() throws IOException {
		URL whatismyip = new URL("http://checkip.amazonaws.com");
		BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
		String ip = in.readLine();
		in.close();
		return ip;
	}

	static String getShittyKey() throws IOException {
		// TODO: Use less volatile data
		// Screen resolution
		// http://alvinalexander.com/blog/post/jfc-swing/how-determine-get-screen-size-java-swing-app
		// Operating system
		// http://stackoverflow.com/questions/228477/how-do-i-programmatically-determine-operating-system-in-java
		StringBuilder keygen = new StringBuilder();
		String key = getIP();
		for (int i = 0; i < 2; i++) {
			keygen.append(key);
		}
		return keygen.toString().substring(0, 16);
	}

	public static String encrypt(String key, String initVector, String value) throws Exception {
		IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
		byte[] encrypted = cipher.doFinal(value.getBytes());
		return Base64.encodeBase64String(encrypted);
	}

	public static String decrypt(String key, String initVector, String encrypted) throws Exception {
		IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
		cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
		byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));
		return new String(original);
	}

	public static String getEncryptedLogin(String data) {
		try {
			String key = getShittyKey();
			String enc = encrypt(key, key, data);
			return enc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
