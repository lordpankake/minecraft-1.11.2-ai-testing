package me.lpk.client.events.world;

import me.lpk.event.Event;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;

public class EventDamageBlock extends Event {

	public IBlockState blockstate;
	/**
	 * Changing this location will not change the block being broken.
	 */
	public BlockPos pos;

	public float damage;

}
