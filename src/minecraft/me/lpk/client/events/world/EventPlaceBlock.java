package me.lpk.client.events.world;

import me.lpk.event.Event;
import net.minecraft.block.Block;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;

public class EventPlaceBlock extends Event {
	/**
	 * Changing this location will not change the location of the item being
	 * placed.
	 */
	public BlockPos pos;

	public EnumFacing face;

	public boolean doPlace;

	/**
	 * Changing this block will not change the block being placed.
	 */
	public Block block;

}
