import java.util.Arrays;

import me.lpk.client.Client;
import net.minecraft.client.main.Main;

public class Start {
	public static void main(String[] args) {
		try {
			Main.main(concat(new String[] { "--version", "1.11.2", "--accessToken", "0", "--assetsDir", "assets", "--assetIndex", "1.11", "--userProperties", "{}" }, args));
		} catch (Exception e) {
			if (!Client.publicBuild) {
				e.printStackTrace();
			}
		} catch (Throwable t) {
			if (!Client.publicBuild) {
				t.printStackTrace();
			}
		}
	}

	public static <T> T[] concat(T[] first, T[] second) {
		T[] result = Arrays.copyOf(first, first.length + second.length);
		System.arraycopy(second, 0, result, first.length, second.length);
		return result;
	}
}
