package net.minecraft.world.chunk.storage;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.optifine.Reflector;
import net.minecraft.world.chunk.BlockStateContainer;
import net.minecraft.world.chunk.NibbleArray;

public class ExtendedBlockStorage {
	private final int yBase;
	private int blockRefCount;
	private int tickRefCount;
	private final BlockStateContainer data;
	private NibbleArray blocklightArray;
	private NibbleArray skylightArray;

	public ExtendedBlockStorage(int i, boolean flag) {
		this.yBase = i;
		this.data = new BlockStateContainer();
		this.blocklightArray = new NibbleArray();
		if (flag) {
			this.skylightArray = new NibbleArray();
		}

	}

	public IBlockState get(int i, int j, int k) {
		return this.data.get(i, j, k);
	}

	public void set(int i, int j, int k, IBlockState iblockstate) {
		if (Reflector.IExtendedBlockState.isInstance(iblockstate)) {
			iblockstate = (IBlockState) Reflector.call(iblockstate, Reflector.IExtendedBlockState_getClean, new Object[0]);
		}

		IBlockState iblockstate1 = this.get(i, j, k);
		Block block = iblockstate1.getBlock();
		Block block1 = iblockstate.getBlock();
		if (block != Blocks.AIR) {
			--this.blockRefCount;
			if (block.getTickRandomly()) {
				--this.tickRefCount;
			}
		}

		if (block1 != Blocks.AIR) {
			++this.blockRefCount;
			if (block1.getTickRandomly()) {
				++this.tickRefCount;
			}
		}

		this.data.set(i, j, k, iblockstate);
	}

	public boolean isEmpty() {
		return this.blockRefCount == 0;
	}

	public boolean getNeedsRandomTick() {
		return this.tickRefCount > 0;
	}

	public int getYLocation() {
		return this.yBase;
	}

	public void setExtSkylightValue(int i, int j, int k, int l) {
		this.skylightArray.set(i, j, k, l);
	}

	public int getExtSkylightValue(int i, int j, int k) {
		return this.skylightArray.get(i, j, k);
	}

	public void setExtBlocklightValue(int i, int j, int k, int l) {
		this.blocklightArray.set(i, j, k, l);
	}

	public int getExtBlocklightValue(int i, int j, int k) {
		return this.blocklightArray.get(i, j, k);
	}

	public void removeInvalidBlocks() {
		IBlockState iblockstate = Blocks.AIR.getDefaultState();
		int i = 0;
		int j = 0;

		for (int k = 0; k < 16; ++k) {
			for (int l = 0; l < 16; ++l) {
				for (int i1 = 0; i1 < 16; ++i1) {
					IBlockState iblockstate1 = this.data.get(i1, k, l);
					if (iblockstate1 != iblockstate) {
						++i;
						Block block = iblockstate1.getBlock();
						if (block.getTickRandomly()) {
							++j;
						}
					}
				}
			}
		}

		this.blockRefCount = i;
		this.tickRefCount = j;
	}

	public BlockStateContainer getData() {
		return this.data;
	}

	public NibbleArray getBlocklightArray() {
		return this.blocklightArray;
	}

	public NibbleArray getSkylightArray() {
		return this.skylightArray;
	}

	public void setBlocklightArray(NibbleArray nibblearray) {
		this.blocklightArray = nibblearray;
	}

	public void setSkylightArray(NibbleArray nibblearray) {
		this.skylightArray = nibblearray;
	}
}
