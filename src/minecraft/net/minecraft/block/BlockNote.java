package net.minecraft.block;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.stats.StatList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityNote;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockNote extends BlockContainer {
	public BlockNote() {
		super(Material.WOOD);
		this.setCreativeTab(CreativeTabs.REDSTONE);
	}

	/**
	 * Called when a neighboring block was changed and marks that this state
	 * should perform any checks during a neighbor change. Cases may include
	 * when redstone power is updated, cactus blocks popping off due to a
	 * neighboring solid block, etc.
	 */
	@Override
	public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos) {
		boolean flag = worldIn.isBlockPowered(pos);
		TileEntity tileentity = worldIn.getTileEntity(pos);

		if (tileentity instanceof TileEntityNote) {
			TileEntityNote tileentitynote = (TileEntityNote) tileentity;

			if (tileentitynote.previousRedstoneState != flag) {
				if (flag) {
					tileentitynote.triggerNote(worldIn, pos);
				}

				tileentitynote.previousRedstoneState = flag;
			}
		}
	}

	/**
	 * Called when the block is right clicked by a player.
	 */
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
		if (worldIn.isRemote) {
			return true;
		} else {
			TileEntity tileentity = worldIn.getTileEntity(pos);

			if (tileentity instanceof TileEntityNote) {
				TileEntityNote tileentitynote = (TileEntityNote) tileentity;
				tileentitynote.changePitch();
				tileentitynote.triggerNote(worldIn, pos);
				playerIn.addStat(StatList.NOTEBLOCK_TUNED);
			}

			return true;
		}
	}

	@Override
	public void onBlockClicked(World worldIn, BlockPos pos, EntityPlayer playerIn) {
		if (!worldIn.isRemote) {
			TileEntity tileentity = worldIn.getTileEntity(pos);

			if (tileentity instanceof TileEntityNote) {
				((TileEntityNote) tileentity).triggerNote(worldIn, pos);
				playerIn.addStat(StatList.NOTEBLOCK_PLAYED);
			}
		}
	}

	/**
	 * Returns a new instance of a block's tile entity class. Called on placing
	 * the block.
	 */
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileEntityNote();
	}

	/**
	 * Called on both Client and Server when World#addBlockEvent is called. On
	 * the Server, this may perform additional changes to the world, like
	 * pistons replacing the block with an extended base. On the client, the
	 * update may involve replacing tile entities, playing sounds, or performing
	 * other visual actions to reflect the server side changes.
	 */
	@Override
	public boolean eventReceived(IBlockState state, World worldIn, BlockPos pos, int id, int param) {
		float f = (float) Math.pow(2.0D, (param - 12) / 12.0D);
		worldIn.playSound((EntityPlayer) null, pos, id, SoundCategory.RECORDS, 3.0F, f);
		worldIn.spawnParticle(EnumParticleTypes.NOTE, pos.getX() + 0.5D, pos.getY() + 1.2D, pos.getZ() + 0.5D, param / 24.0D, 0.0D, 0.0D, new int[0]);
		return true;
	}

	/**
	 * The type of render function called. MODEL for mixed tesr and static
	 * model, MODELBLOCK_ANIMATED for TESR-only, LIQUID for vanilla liquids,
	 * INVISIBLE to skip all rendering
	 */
	@Override
	public EnumBlockRenderType getRenderType(IBlockState state) {
		return EnumBlockRenderType.MODEL;
	}
}
