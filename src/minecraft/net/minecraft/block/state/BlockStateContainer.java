package net.minecraft.block.state;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedMap;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Table;
import com.google.common.collect.UnmodifiableIterator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import net.minecraft.block.Block;
import net.minecraft.block.BlockFlower;
import net.minecraft.block.material.EnumPushReaction;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateBase;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.optifine.BlockModelUtils;
import net.optifine.Reflector;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.MapPopulator;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Cartesian;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.property.IUnlistedProperty;

public class BlockStateContainer {
	private static final Pattern NAME_PATTERN = Pattern.compile("^[a-z0-9_]+$");
	private static final Function<IProperty<?>, String> GET_NAME_FUNC = new Function<IProperty<?>, String>() {
		@Override
		@Nullable
		public String apply(@Nullable IProperty<?> iproperty) {
			return iproperty == null ? "<NULL>" : iproperty.getName();
		}
	};
	private final Block block;
	private final ImmutableSortedMap<String, IProperty<?>> properties;
	private final ImmutableList<StateImplementation> validStates;

	public BlockStateContainer(Block blockx, IProperty<?>... aiproperty) {
		this(blockx, aiproperty, (ImmutableMap<IUnlistedProperty<?>, Optional<?>>) null);
	}

	protected BlockStateContainer.StateImplementation createState(Block blockx, ImmutableMap<IProperty<?>, Comparable<?>> immutablemap,
			ImmutableMap<IUnlistedProperty<?>, Optional<?>> var3) {
		return new BlockStateContainer.StateImplementation(blockx, immutablemap);
	}

	protected BlockStateContainer(Block blockx, IProperty<?>[] aiproperty, ImmutableMap<IUnlistedProperty<?>, Optional<?>> immutablemap) {
		this.block = blockx;
		HashMap hashmap = Maps.newHashMap();

		for (IProperty iproperty : aiproperty) {
			validateProperty(blockx, iproperty);
			hashmap.put(iproperty.getName(), iproperty);
		}

		this.properties = ImmutableSortedMap.copyOf(hashmap);
		LinkedHashMap linkedhashmap = Maps.newLinkedHashMap();
		ArrayList<BlockStateContainer.StateImplementation> arraylist = Lists.newArrayList();

		for (List list : Cartesian.cartesianProduct(this.getAllowedValues())) {
			Map map = MapPopulator.createMap(this.properties.values(), list);
			BlockStateContainer.StateImplementation blockstatecontainer$stateimplementation = this.createState(blockx, ImmutableMap.copyOf(map), immutablemap);
			linkedhashmap.put(map, blockstatecontainer$stateimplementation);
			arraylist.add(blockstatecontainer$stateimplementation);
		}

		for (BlockStateContainer.StateImplementation blockstatecontainer$stateimplementation1 : arraylist) {
			blockstatecontainer$stateimplementation1.buildPropertyValueTable(linkedhashmap);
		}

		this.validStates = ImmutableList.copyOf(arraylist);
	}

	public static <T extends Comparable<T>> String validateProperty(Block block, IProperty<T> property) {
		String s = property.getName();

		if (!NAME_PATTERN.matcher(s).matches()) {
			throw new IllegalArgumentException("Block: " + block.getClass() + " has invalidly named property: " + s);
		} else {
			for (T t : property.getAllowedValues()) {
				String s1 = property.getName(t);

				if (!NAME_PATTERN.matcher(s1).matches()) {
					throw new IllegalArgumentException("Block: " + block.getClass() + " has property: " + s + " with invalidly named value: " + s1);
				}
			}

			return s;
		}
	}

	public ImmutableList<StateImplementation> getValidStates() {
		return this.validStates;
	}

	private List<Iterable<Comparable<?>>> getAllowedValues() {
		ArrayList arraylist = Lists.newArrayList();
		ImmutableCollection immutablecollection = this.properties.values();
		UnmodifiableIterator unmodifiableiterator = immutablecollection.iterator();

		while (unmodifiableiterator.hasNext()) {
			IProperty iproperty = (IProperty) unmodifiableiterator.next();
			arraylist.add(iproperty.getAllowedValues());
		}

		return arraylist;
	}

	public IBlockState getBaseState() {
		return this.validStates.get(0);
	}

	public Block getBlock() {
		return this.block;
	}

	public Collection<IProperty<?>> getProperties() {
		return this.properties.values();
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("block", Block.REGISTRY.getNameForObject(this.block))
				.add("properties", Iterables.transform(this.properties.values(), GET_NAME_FUNC)).toString();
	}

	@Nullable
	public IProperty<?> getProperty(String s) {
		return this.properties.get(s);
	}

	public static class Builder {
		private final Block block;
		private final List<IProperty<?>> listed = Lists.newArrayList();
		private final List<IUnlistedProperty<?>> unlisted = Lists.newArrayList();

		public Builder(Block blockx) {
			this.block = blockx;
		}

		public BlockStateContainer.Builder add(IProperty<?>... aiproperty) {
			for (IProperty iproperty : aiproperty) {
				this.listed.add(iproperty);
			}

			return this;
		}

		public BlockStateContainer.Builder add(IUnlistedProperty<?>... aiunlistedproperty) {
			for (IUnlistedProperty iunlistedproperty : aiunlistedproperty) {
				this.unlisted.add(iunlistedproperty);
			}

			return this;
		}

		public BlockStateContainer build() {
			IProperty[] aiproperty = new IProperty[this.listed.size()];
			aiproperty = this.listed.toArray(aiproperty);
			if (this.unlisted.size() == 0) {
				return new BlockStateContainer(this.block, aiproperty);
			} else {
				IUnlistedProperty[] aiunlistedproperty = new IUnlistedProperty[this.unlisted.size()];
				aiunlistedproperty = this.unlisted.toArray(aiunlistedproperty);
				return (BlockStateContainer) Reflector.newInstance(Reflector.ExtendedBlockState_Constructor, new Object[] { this.block, aiproperty, aiunlistedproperty });
			}
		}
	}

	static class StateImplementation extends BlockStateBase {
		private final Block block;
		private final ImmutableMap<IProperty<?>, Comparable<?>> properties;
		private ImmutableTable<IProperty<?>, Comparable<?>, IBlockState> propertyValueTable;

		private StateImplementation(Block blockx, ImmutableMap<IProperty<?>, Comparable<?>> immutablemap) {
			this.block = blockx;
			this.properties = immutablemap;
		}

		protected StateImplementation(Block blockx, ImmutableMap<IProperty<?>, Comparable<?>> immutablemap,
				ImmutableTable<IProperty<?>, Comparable<?>, IBlockState> immutabletable) {
			this.block = blockx;
			this.properties = immutablemap;
			this.propertyValueTable = immutabletable;
		}

		@Override
		public Collection<IProperty<?>> getPropertyKeys() {
			return Collections.<IProperty<?>> unmodifiableCollection(this.properties.keySet());
		}

		@Override
		public <T extends Comparable<T>> T getValue(IProperty<T> iproperty) {
			Comparable comparable = this.properties.get(iproperty);
			if (comparable == null) {
				throw new IllegalArgumentException("Cannot get property " + iproperty + " as it does not exist in " + this.block.getBlockState());
			} else {
				return (T) ((Comparable) iproperty.getValueClass().cast(comparable));
			}
		}

		@Override
		public <T extends Comparable<T>, V extends T> IBlockState withProperty(IProperty<T> iproperty, V comparable) {
			Comparable comparable1 = this.properties.get(iproperty);
			if (comparable1 == null) {
				throw new IllegalArgumentException("Cannot set property " + iproperty + " as it does not exist in " + this.block.getBlockState());
			} else if (comparable1 == comparable) {
				return this;
			} else {
				IBlockState iblockstate = this.propertyValueTable.get(iproperty, comparable);
				if (iblockstate == null) {
					throw new IllegalArgumentException(
							"Cannot set property " + iproperty + " to " + comparable + " on block " + Block.REGISTRY.getNameForObject(this.block) + ", it is not an allowed value");
				} else {
					return iblockstate;
				}
			}
		}

		@Override
		public ImmutableMap<IProperty<?>, Comparable<?>> getProperties() {
			return this.properties;
		}

		@Override
		public Block getBlock() {
			return this.block;
		}

		@Override
		public boolean equals(Object object) {
			return this == object;
		}

		@Override
		public int hashCode() {
			return this.properties.hashCode();
		}

		public void buildPropertyValueTable(Map<Map<IProperty<?>, Comparable<?>>, BlockStateContainer.StateImplementation> map) {
			if (this.propertyValueTable != null) {
				throw new IllegalStateException();
			} else {
				Table<IProperty<?>, Comparable<?>, IBlockState> table = HashBasedTable.<IProperty<?>, Comparable<?>, IBlockState> create();
				UnmodifiableIterator unmodifiableiterator = this.properties.entrySet().iterator();

				while (unmodifiableiterator.hasNext()) {
					Entry<IProperty<?>, Comparable<?>> entry = (Entry) unmodifiableiterator.next();
					IProperty<?> iproperty = entry.getKey();

					for (Comparable<?> comparable : iproperty.getAllowedValues()) {
						if (comparable != entry.getValue()) {
							table.put(iproperty, comparable, map.get(this.getPropertiesWithValue(iproperty, comparable)));
						}
					}
				}

				this.propertyValueTable = ImmutableTable.<IProperty<?>, Comparable<?>, IBlockState> copyOf(table);
			}
		}

		private Map<IProperty<?>, Comparable<?>> getPropertiesWithValue(IProperty<?> iproperty, Comparable<?> comparable) {
			HashMap hashmap = Maps.newHashMap(this.properties);
			hashmap.put(iproperty, comparable);
			return hashmap;
		}

		@Override
		public Material getMaterial() {
			return this.block.getMaterial(this);
		}

		@Override
		public boolean isFullBlock() {
			return this.block.isFullBlock(this);
		}

		@Override
		public boolean canEntitySpawn(Entity entity) {
			return this.block.canEntitySpawn(this, entity);
		}

		@Override
		public int getLightOpacity() {
			return this.block.getLightOpacity(this);
		}

		@Override
		public int getLightValue() {
			return this.block.getLightValue(this);
		}

		@Override
		public boolean isTranslucent() {
			return this.block.isTranslucent(this);
		}

		@Override
		public boolean useNeighborBrightness() {
			return this.block.getUseNeighborBrightness(this);
		}

		@Override
		public MapColor getMapColor() {
			return this.block.getMapColor(this);
		}

		@Override
		public IBlockState withRotation(Rotation rotation) {
			return this.block.withRotation(this, rotation);
		}

		@Override
		public IBlockState withMirror(Mirror mirror) {
			return this.block.withMirror(this, mirror);
		}

		@Override
		public boolean isFullCube() {
			return this.block.isFullCube(this);
		}

		@Override
		public boolean hasCustomBreakingProgress() {
			return this.block.hasCustomBreakingProgress(this);
		}

		@Override
		public EnumBlockRenderType getRenderType() {
			return this.block.getRenderType(this);
		}

		@Override
		public int getPackedLightmapCoords(IBlockAccess iblockaccess, BlockPos blockpos) {
			return this.block.getPackedLightmapCoords(this, iblockaccess, blockpos);
		}

		@Override
		public float getAmbientOcclusionLightValue() {
			return this.block.getAmbientOcclusionLightValue(this);
		}

		@Override
		public boolean isBlockNormalCube() {
			return this.block.isBlockNormalCube(this);
		}

		@Override
		public boolean isNormalCube() {
			return this.block.isNormalCube(this);
		}

		@Override
		public boolean canProvidePower() {
			return this.block.canProvidePower(this);
		}

		@Override
		public int getWeakPower(IBlockAccess iblockaccess, BlockPos blockpos, EnumFacing enumfacing) {
			return this.block.getWeakPower(this, iblockaccess, blockpos, enumfacing);
		}

		@Override
		public boolean hasComparatorInputOverride() {
			return this.block.hasComparatorInputOverride(this);
		}

		@Override
		public int getComparatorInputOverride(World world, BlockPos blockpos) {
			return this.block.getComparatorInputOverride(this, world, blockpos);
		}

		@Override
		public float getBlockHardness(World world, BlockPos blockpos) {
			return this.block.getBlockHardness(this, world, blockpos);
		}

		@Override
		public float getPlayerRelativeBlockHardness(EntityPlayer entityplayer, World world, BlockPos blockpos) {
			return this.block.getPlayerRelativeBlockHardness(this, entityplayer, world, blockpos);
		}

		@Override
		public int getStrongPower(IBlockAccess iblockaccess, BlockPos blockpos, EnumFacing enumfacing) {
			return this.block.getStrongPower(this, iblockaccess, blockpos, enumfacing);
		}

		@Override
		public EnumPushReaction getMobilityFlag() {
			return this.block.getMobilityFlag(this);
		}

		@Override
		public IBlockState getActualState(IBlockAccess iblockaccess, BlockPos blockpos) {
			return this.block.getActualState(this, iblockaccess, blockpos);
		}

		@Override
		public AxisAlignedBB getSelectedBoundingBox(World world, BlockPos blockpos) {
			return this.block.getSelectedBoundingBox(this, world, blockpos);
		}

		@Override
		public boolean shouldSideBeRendered(IBlockAccess iblockaccess, BlockPos blockpos, EnumFacing enumfacing) {
			return this.block.shouldSideBeRendered(this, iblockaccess, blockpos, enumfacing);
		}

		@Override
		public boolean isOpaqueCube() {
			return this.block.isOpaqueCube(this);
		}

		@Override
		@Nullable
		public AxisAlignedBB getCollisionBoundingBox(IBlockAccess iblockaccess, BlockPos blockpos) {
			return this.block.getCollisionBoundingBox(this, iblockaccess, blockpos);
		}

		@Override
		public void addCollisionBoxToList(World world, BlockPos blockpos, AxisAlignedBB axisalignedbb, List<AxisAlignedBB> list, @Nullable Entity entity, boolean flag) {
			this.block.addCollisionBoxToList(this, world, blockpos, axisalignedbb, list, entity, flag);
		}

		@Override
		public AxisAlignedBB getBoundingBox(IBlockAccess iblockaccess, BlockPos blockpos) {
			Block.EnumOffsetType block$enumoffsettype = this.block.getOffsetType();
			if (block$enumoffsettype != Block.EnumOffsetType.NONE && !(this.block instanceof BlockFlower)) {
				AxisAlignedBB axisalignedbb = this.block.getBoundingBox(this, iblockaccess, blockpos);
				axisalignedbb = BlockModelUtils.getOffsetBoundingBox(axisalignedbb, block$enumoffsettype, blockpos);
				return axisalignedbb;
			} else {
				return this.block.getBoundingBox(this, iblockaccess, blockpos);
			}
		}

		@Override
		public RayTraceResult collisionRayTrace(World world, BlockPos blockpos, Vec3d vec3d, Vec3d vec3d1) {
			return this.block.collisionRayTrace(this, world, blockpos, vec3d, vec3d1);
		}

		@Override
		public boolean isFullyOpaque() {
			return this.block.isFullyOpaque(this);
		}

		@Override
		public Vec3d getOffset(IBlockAccess iblockaccess, BlockPos blockpos) {
			return this.block.getOffset(this, iblockaccess, blockpos);
		}

		@Override
		public boolean onBlockEventReceived(World world, BlockPos blockpos, int i, int j) {
			return this.block.eventReceived(this, world, blockpos, i, j);
		}

		@Override
		public void neighborChanged(World world, BlockPos blockpos, Block blockx, BlockPos blockpos1) {
			this.block.neighborChanged(this, world, blockpos, blockx, blockpos1);
		}

		@Override
		public boolean causesSuffocation() {
			return this.block.causesSuffocation(this);
		}

		public ImmutableTable<IProperty<?>, Comparable<?>, IBlockState> getPropertyValueTable() {
			return this.propertyValueTable;
		}

		public int getLightOpacity(IBlockAccess iblockaccess, BlockPos blockpos) {
			return Reflector.callInt(this.block, Reflector.ForgeBlock_getLightOpacity, new Object[] { this, iblockaccess, blockpos });
		}

		public int getLightValue(IBlockAccess iblockaccess, BlockPos blockpos) {
			return Reflector.callInt(this.block, Reflector.ForgeBlock_getLightValue, new Object[] { this, iblockaccess, blockpos });
		}

		public boolean isSideSolid(IBlockAccess iblockaccess, BlockPos blockpos, EnumFacing enumfacing) {
			return Reflector.callBoolean(this.block, Reflector.ForgeBlock_isSideSolid, new Object[] { this, iblockaccess, blockpos, enumfacing });
		}

		public boolean doesSideBlockRendering(IBlockAccess iblockaccess, BlockPos blockpos, EnumFacing enumfacing) {
			return Reflector.callBoolean(this.block, Reflector.ForgeBlock_doesSideBlockRendering, new Object[] { this, iblockaccess, blockpos, enumfacing });
		}
	}
}
