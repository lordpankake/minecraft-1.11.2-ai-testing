package net.minecraft.client.multiplayer;

import com.google.common.collect.Sets;

import me.lpk.client.Client;

import java.util.Random;
import java.util.Set;
import javax.annotation.Nullable;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.MovingSoundMinecart;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.multiplayer.ChunkProviderClient;
import net.minecraft.client.multiplayer.PlayerControllerMP;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.client.particle.ParticleFirework;
import net.minecraft.crash.CrashReport;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.crash.ICrashReportDetail;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.Packet;
import net.minecraft.profiler.Profiler;
import net.minecraft.scoreboard.Scoreboard;
import net.optifine.Config;
import net.optifine.DynamicLights;
import net.optifine.PlayerControllerOF;
import net.optifine.Reflector;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.DimensionType;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.GameType;
import net.minecraft.world.World;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.WorldSettings;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.storage.SaveDataMemoryStorage;
import net.minecraft.world.storage.SaveHandlerMP;
import net.minecraft.world.storage.WorldInfo;

public class WorldClient extends World {
	private final NetHandlerPlayClient connection;
	private ChunkProviderClient clientChunkProvider;
	private final Set<Entity> entityList = Sets.newHashSet();
	private final Set<Entity> entitySpawnQueue = Sets.newHashSet();
	private final Minecraft mc = Minecraft.getMinecraft();
	private final Set<ChunkPos> previousActiveChunkSet = Sets.newHashSet();
	private int ambienceTicks;
	protected Set<ChunkPos> viewableChunks;
	private int playerChunkX = Integer.MIN_VALUE;
	private int playerChunkY = Integer.MIN_VALUE;
	private boolean playerUpdate = false;

	public WorldClient(NetHandlerPlayClient nethandlerplayclient, WorldSettings worldsettings, int i, EnumDifficulty enumdifficulty, Profiler profiler) {
		super(new SaveHandlerMP(), new WorldInfo(worldsettings, "MpServer"), makeWorldProvider(i), profiler, true);
		this.ambienceTicks = this.rand.nextInt(12000);
		this.viewableChunks = Sets.newHashSet();
		this.connection = nethandlerplayclient;
		this.getWorldInfo().setDifficulty(enumdifficulty);
		this.provider.setWorld(this);
		this.setSpawnPoint(new BlockPos(8, 64, 8));
		this.chunkProvider = this.createChunkProvider();
		this.mapStorage = new SaveDataMemoryStorage();
		this.calculateInitialSkylight();
		this.calculateInitialWeather();
		Reflector.call(this, Reflector.ForgeWorld_initCapabilities, new Object[0]);
		Reflector.postForgeBusEvent(Reflector.WorldEvent_Load_Constructor, new Object[] { this });
		if (this.mc.playerController != null && this.mc.playerController.getClass() == PlayerControllerMP.class) {
			this.mc.playerController = new PlayerControllerOF(this.mc, nethandlerplayclient);
		}

	}

	private static WorldProvider makeWorldProvider(int i) {
		return Reflector.DimensionManager_createProviderFor.exists()
				? (WorldProvider) Reflector.call(Reflector.DimensionManager_createProviderFor, new Object[] { Integer.valueOf(i) }) : DimensionType.getById(i).createDimension();
	}

	@Override
	public void tick() {
		super.tick();
		this.setTotalWorldTime(this.getTotalWorldTime() + 1L);
		if (this.getGameRules().getBoolean("doDaylightCycle")) {
			this.setWorldTime(this.getWorldTime() + 1L);
		}

		this.theProfiler.startSection("reEntryProcessing");

		for (int i = 0; i < 10 && !this.entitySpawnQueue.isEmpty(); ++i) {
			Entity entity = this.entitySpawnQueue.iterator().next();
			this.entitySpawnQueue.remove(entity);
			if (!this.loadedEntityList.contains(entity)) {
				this.spawnEntity(entity);
			}
		}

		this.theProfiler.endStartSection("chunkCache");
		this.clientChunkProvider.tick();
		this.theProfiler.endStartSection("blocks");
		this.updateBlocks();
		this.theProfiler.endSection();
	}

	public void invalidateBlockReceiveRegion(int var1, int var2, int var3, int var4, int var5, int var6) {
	}

	@Override
	protected IChunkProvider createChunkProvider() {
		this.clientChunkProvider = new ChunkProviderClient(this);
		return this.clientChunkProvider;
	}

	@Override
	protected boolean isChunkLoaded(int i, int j, boolean flag) {
		return flag || !this.getChunkProvider().provideChunk(i, j).isEmpty();
	}

	protected void buildChunkCoordList() {
		int i = MathHelper.floor(this.mc.player.posX / 16.0D);
		int j = MathHelper.floor(this.mc.player.posZ / 16.0D);
		if (i != this.playerChunkX || j != this.playerChunkY) {
			this.playerChunkX = i;
			this.playerChunkY = j;
			this.viewableChunks.clear();
			int k = this.mc.gameSettings.renderDistanceChunks;
			this.theProfiler.startSection("buildList");
			int l = MathHelper.floor(this.mc.player.posX / 16.0D);
			int i1 = MathHelper.floor(this.mc.player.posZ / 16.0D);

			for (int j1 = -k; j1 <= k; ++j1) {
				for (int k1 = -k; k1 <= k; ++k1) {
					this.viewableChunks.add(new ChunkPos(j1 + l, k1 + i1));
				}
			}

			this.theProfiler.endSection();
		}
	}

	@Override
	protected void updateBlocks() {
		this.buildChunkCoordList();
		if (this.ambienceTicks > 0) {
			--this.ambienceTicks;
		}

		this.previousActiveChunkSet.retainAll(this.viewableChunks);
		if (this.previousActiveChunkSet.size() == this.viewableChunks.size()) {
			this.previousActiveChunkSet.clear();
		}

		int i = 0;

		for (ChunkPos chunkpos : this.viewableChunks) {
			if (!this.previousActiveChunkSet.contains(chunkpos)) {
				int j = chunkpos.chunkXPos * 16;
				int k = chunkpos.chunkZPos * 16;
				this.theProfiler.startSection("getChunk");
				Chunk chunk = this.getChunkFromChunkCoords(chunkpos.chunkXPos, chunkpos.chunkZPos);
				this.playMoodSoundAndCheckLight(j, k, chunk);
				this.theProfiler.endSection();
				this.previousActiveChunkSet.add(chunkpos);
				++i;
				if (i >= 10) {
					return;
				}
			}
		}

	}

	public void doPreChunk(int i, int j, boolean flag) {
		if (flag) {
			this.clientChunkProvider.loadChunk(i, j);
		} else {
			this.clientChunkProvider.unloadChunk(i, j);
			this.markBlockRangeForRenderUpdate(i * 16, 0, j * 16, i * 16 + 15, 256, j * 16 + 15);
		}

	}

	@Override
	public boolean spawnEntity(Entity entity) {
		boolean flag = super.spawnEntity(entity);
		this.entityList.add(entity);
		if (flag) {
			if (entity instanceof EntityMinecart) {
				this.mc.getSoundHandler().playSound(new MovingSoundMinecart((EntityMinecart) entity));
			}
		} else {
			this.entitySpawnQueue.add(entity);
		}

		return flag;
	}

	@Override
	public void removeEntity(Entity entity) {
		super.removeEntity(entity);
		this.entityList.remove(entity);
	}

	@Override
	protected void onEntityAdded(Entity entity) {
		super.onEntityAdded(entity);
		if (this.entitySpawnQueue.contains(entity)) {
			this.entitySpawnQueue.remove(entity);
		}

	}

	@Override
	protected void onEntityRemoved(Entity entity) {
		super.onEntityRemoved(entity);
		if (this.entityList.contains(entity)) {
			if (entity.isEntityAlive()) {
				this.entitySpawnQueue.add(entity);
			} else {
				this.entityList.remove(entity);
			}
		}

	}

	public void addEntityToWorld(int i, Entity entity) {
		Entity entity1 = this.getEntityByID(i);
		if (entity1 != null) {
			this.removeEntity(entity1);
		}

		this.entityList.add(entity);
		entity.setEntityId(i);
		if (!this.spawnEntity(entity)) {
			this.entitySpawnQueue.add(entity);
		}

		this.entitiesById.addKey(i, entity);
	}

	@Override
	@Nullable
	public Entity getEntityByID(int i) {
		return i == this.mc.player.getEntityId() ? this.mc.player : super.getEntityByID(i);
	}

	public Entity removeEntityFromWorld(int i) {
		Entity entity = this.entitiesById.removeObject(i);
		if (entity != null) {
			this.entityList.remove(entity);
			this.removeEntity(entity);
		}

		return entity;
	}

	/** @deprecated */
	@Deprecated
	public boolean invalidateRegionAndSetBlock(BlockPos blockpos, IBlockState iblockstate) {
		int i = blockpos.getX();
		int j = blockpos.getY();
		int k = blockpos.getZ();
		this.invalidateBlockReceiveRegion(i, j, k, i, j, k);
		return super.setBlockState(blockpos, iblockstate, 3);
	}

	@Override
	public void sendQuittingDisconnectingPacket() {
		ServerData data = this.mc.getCurrentServerData();
		if (data != null) {
			Client.getAnalytics().onLeave(data);
		}
		this.connection.getNetworkManager().closeChannel(new TextComponentString("Quitting"));
	}

	@Override
	protected void updateWeather() {
	}

	@Override
	protected void playMoodSoundAndCheckLight(int i, int j, Chunk chunk) {
		super.playMoodSoundAndCheckLight(i, j, chunk);
		if (this.ambienceTicks == 0) {
			EntityPlayerSP entityplayersp = this.mc.player;
			if (entityplayersp == null) {
				return;
			}

			if (Math.abs(entityplayersp.chunkCoordX - chunk.xPosition) > 1 || Math.abs(entityplayersp.chunkCoordZ - chunk.zPosition) > 1) {
				return;
			}

			this.updateLCG = this.updateLCG * 3 + 1013904223;
			int k = this.updateLCG >> 2;
			int l = k & 15;
			int i1 = k >> 8 & 15;
			int j1 = k >> 16 & 255;
			j1 = j1 / 2;
			if (entityplayersp.posY > 160.0D) {
				j1 += 128;
			} else if (entityplayersp.posY > 96.0D) {
				j1 += 64;
			}

			BlockPos blockpos = new BlockPos(l + i, j1, i1 + j);
			IBlockState iblockstate = chunk.getBlockState(blockpos);
			l = l + i;
			i1 = i1 + j;
			double d0 = this.mc.player.getDistanceSq(l + 0.5D, j1 + 0.5D, i1 + 0.5D);
			if (d0 < 4.0D) {
				return;
			}

			if (d0 > 255.0D) {
				return;
			}

			if (iblockstate.getMaterial() == Material.AIR && this.getLight(blockpos) <= this.rand.nextInt(8) && this.getLightFor(EnumSkyBlock.SKY, blockpos) <= 0) {
				this.playSound(l + 0.5D, j1 + 0.5D, i1 + 0.5D, SoundEvents.AMBIENT_CAVE, SoundCategory.AMBIENT, 0.7F, 0.8F + this.rand.nextFloat() * 0.2F, false);
				this.ambienceTicks = this.rand.nextInt(12000) + 6000;
			}
		}

	}

	public void doVoidFogParticles(int i, int j, int k) {
		boolean flag = true;
		Random random = new Random();
		ItemStack itemstack = this.mc.player.getHeldItemMainhand();
		if (itemstack == null || Block.getBlockFromItem(itemstack.getItem()) != Blocks.BARRIER) {
			itemstack = this.mc.player.getHeldItemOffhand();
		}

		boolean flag1 = this.mc.playerController.getCurrentGameType() == GameType.CREATIVE && !itemstack.isEmpty() && itemstack.getItem() == Item.getItemFromBlock(Blocks.BARRIER);
		BlockPos.MutableBlockPos blockpos$mutableblockpos = new BlockPos.MutableBlockPos();

		for (int l = 0; l < 667; ++l) {
			this.showBarrierParticles(i, j, k, 16, random, flag1, blockpos$mutableblockpos);
			this.showBarrierParticles(i, j, k, 32, random, flag1, blockpos$mutableblockpos);
		}

	}

	public void showBarrierParticles(int i, int j, int k, int l, Random random, boolean flag, BlockPos.MutableBlockPos blockpos$mutableblockpos) {
		if (this.mc.gameSettings.particleSetting == 3) {
			return;
		}
		int i1 = i + this.rand.nextInt(l) - this.rand.nextInt(l);
		int j1 = j + this.rand.nextInt(l) - this.rand.nextInt(l);
		int k1 = k + this.rand.nextInt(l) - this.rand.nextInt(l);
		blockpos$mutableblockpos.setPos(i1, j1, k1);
		IBlockState iblockstate = this.getBlockState(blockpos$mutableblockpos);
		iblockstate.getBlock().randomDisplayTick(iblockstate, this, blockpos$mutableblockpos, random);
		if (flag && iblockstate.getBlock() == Blocks.BARRIER) {
			this.spawnParticle(EnumParticleTypes.BARRIER, i1 + 0.5F, j1 + 0.5F, k1 + 0.5F, 0.0D, 0.0D, 0.0D, new int[0]);
		}

	}

	public void removeAllEntities() {
		this.loadedEntityList.removeAll(this.unloadedEntityList);

		for (int i = 0; i < this.unloadedEntityList.size(); ++i) {
			Entity entity = this.unloadedEntityList.get(i);
			int j = entity.chunkCoordX;
			int k = entity.chunkCoordZ;
			if (entity.addedToChunk && this.isChunkLoaded(j, k, true)) {
				this.getChunkFromChunkCoords(j, k).removeEntity(entity);
			}
		}

		for (int i1 = 0; i1 < this.unloadedEntityList.size(); ++i1) {
			this.onEntityRemoved(this.unloadedEntityList.get(i1));
		}

		this.unloadedEntityList.clear();

		for (int j1 = 0; j1 < this.loadedEntityList.size(); ++j1) {
			Entity entity1 = this.loadedEntityList.get(j1);
			Entity entity2 = entity1.getRidingEntity();
			if (entity2 != null) {
				if (!entity2.isDead && entity2.isPassenger(entity1)) {
					continue;
				}

				entity1.dismountRidingEntity();
			}

			if (entity1.isDead) {
				int k1 = entity1.chunkCoordX;
				int l = entity1.chunkCoordZ;
				if (entity1.addedToChunk && this.isChunkLoaded(k1, l, true)) {
					this.getChunkFromChunkCoords(k1, l).removeEntity(entity1);
				}

				this.loadedEntityList.remove(j1--);
				this.onEntityRemoved(entity1);
			}
		}

	}

	@Override
	public CrashReportCategory addWorldInfoToCrashReport(CrashReport crashreport) {
		CrashReportCategory crashreportcategory = super.addWorldInfoToCrashReport(crashreport);
		crashreportcategory.setDetail("Forced entities", new ICrashReportDetail<String>() {
			@Override
			public String call() {
				return WorldClient.this.entityList.size() + " total; " + WorldClient.this.entityList;
			}
		});
		crashreportcategory.setDetail("Retry entities", new ICrashReportDetail<String>() {
			@Override
			public String call() {
				return WorldClient.this.entitySpawnQueue.size() + " total; " + WorldClient.this.entitySpawnQueue;
			}
		});
		crashreportcategory.setDetail("Server brand", new ICrashReportDetail<String>() {
			@Override
			public String call() throws Exception {
				return WorldClient.this.mc.player.getServerBrand();
			}
		});
		crashreportcategory.setDetail("Server type", new ICrashReportDetail<String>() {
			@Override
			public String call() throws Exception {
				return WorldClient.this.mc.getIntegratedServer() == null ? "Non-integrated multiplayer server" : "Integrated singleplayer server";
			}
		});
		return crashreportcategory;
	}

	@Override
	public void playSound(@Nullable EntityPlayer entityplayer, double d0, double d1, double d2, SoundEvent soundevent, SoundCategory soundcategory, float f, float f1) {
		if (entityplayer == this.mc.player) {
			this.playSound(d0, d1, d2, soundevent, soundcategory, f, f1, false);
		}

	}

	public void playSound(BlockPos blockpos, SoundEvent soundevent, SoundCategory soundcategory, float f, float f1, boolean flag) {
		this.playSound(blockpos.getX() + 0.5D, blockpos.getY() + 0.5D, blockpos.getZ() + 0.5D, soundevent, soundcategory, f, f1, flag);
	}

	@Override
	public void playSound(double d0, double d1, double d2, SoundEvent soundevent, SoundCategory soundcategory, float f, float f1, boolean flag) {
		double d3 = this.mc.getRenderViewEntity().getDistanceSq(d0, d1, d2);
		PositionedSoundRecord positionedsoundrecord = new PositionedSoundRecord(soundevent, soundcategory, f, f1, (float) d0, (float) d1, (float) d2);
		if (flag && d3 > 100.0D) {
			double d4 = Math.sqrt(d3) / 40.0D;
			this.mc.getSoundHandler().playDelayedSound(positionedsoundrecord, (int) (d4 * 20.0D));
		} else {
			this.mc.getSoundHandler().playSound(positionedsoundrecord);
		}

	}

	@Override
	public void makeFireworks(double d0, double d1, double d2, double d3, double d4, double d5, @Nullable NBTTagCompound nbttagcompound) {
		this.mc.effectRenderer.addEffect(new ParticleFirework.Starter(this, d0, d1, d2, d3, d4, d5, this.mc.effectRenderer, nbttagcompound));
	}

	@Override
	public void sendPacketToServer(Packet<?> packet) {
		this.connection.sendPacket(packet);
	}

	public void setWorldScoreboard(Scoreboard scoreboard) {
		this.worldScoreboard = scoreboard;
	}

	@Override
	public void setWorldTime(long i) {
		if (i < 0L) {
			i = -i;
			this.getGameRules().setOrCreateGameRule("doDaylightCycle", "false");
		} else {
			this.getGameRules().setOrCreateGameRule("doDaylightCycle", "true");
		}

		super.setWorldTime(i);
	}

	@Override
	public ChunkProviderClient getChunkProvider() {
		return (ChunkProviderClient) super.getChunkProvider();
	}

	@Override
	public int getCombinedLight(BlockPos blockpos, int i) {
		int j = super.getCombinedLight(blockpos, i);
		if (Config.isDynamicLights()) {
			j = DynamicLights.getCombinedLight(blockpos, j);
		}

		return j;
	}

	@Override
	public boolean setBlockState(BlockPos blockpos, IBlockState iblockstate, int i) {
		this.playerUpdate = this.isPlayerActing();
		boolean flag = super.setBlockState(blockpos, iblockstate, i);
		this.playerUpdate = false;
		return flag;
	}

	private boolean isPlayerActing() {
		if (this.mc.playerController instanceof PlayerControllerOF) {
			PlayerControllerOF playercontrollerof = (PlayerControllerOF) this.mc.playerController;
			return playercontrollerof.isActing();
		} else {
			return false;
		}
	}

	public boolean isPlayerUpdate() {
		return this.playerUpdate;
	}
}
