package net.minecraft.client.particle;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Queues;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import javax.annotation.Nullable;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.Barrier;
import net.minecraft.client.particle.IParticleFactory;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.particle.ParticleBlockDust;
import net.minecraft.client.particle.ParticleBreaking;
import net.minecraft.client.particle.ParticleBubble;
import net.minecraft.client.particle.ParticleCloud;
import net.minecraft.client.particle.ParticleCrit;
import net.minecraft.client.particle.ParticleDigging;
import net.minecraft.client.particle.ParticleDragonBreath;
import net.minecraft.client.particle.ParticleDrip;
import net.minecraft.client.particle.ParticleEmitter;
import net.minecraft.client.particle.ParticleEnchantmentTable;
import net.minecraft.client.particle.ParticleEndRod;
import net.minecraft.client.particle.ParticleExplosion;
import net.minecraft.client.particle.ParticleExplosionHuge;
import net.minecraft.client.particle.ParticleExplosionLarge;
import net.minecraft.client.particle.ParticleFallingDust;
import net.minecraft.client.particle.ParticleFirework;
import net.minecraft.client.particle.ParticleFlame;
import net.minecraft.client.particle.ParticleFootStep;
import net.minecraft.client.particle.ParticleHeart;
import net.minecraft.client.particle.ParticleLava;
import net.minecraft.client.particle.ParticleMobAppearance;
import net.minecraft.client.particle.ParticleNote;
import net.minecraft.client.particle.ParticlePortal;
import net.minecraft.client.particle.ParticleRain;
import net.minecraft.client.particle.ParticleRedstone;
import net.minecraft.client.particle.ParticleSmokeLarge;
import net.minecraft.client.particle.ParticleSmokeNormal;
import net.minecraft.client.particle.ParticleSnowShovel;
import net.minecraft.client.particle.ParticleSpell;
import net.minecraft.client.particle.ParticleSpit;
import net.minecraft.client.particle.ParticleSplash;
import net.minecraft.client.particle.ParticleSuspend;
import net.minecraft.client.particle.ParticleSuspendedTown;
import net.minecraft.client.particle.ParticleSweepAttack;
import net.minecraft.client.particle.ParticleTotem;
import net.minecraft.client.particle.ParticleWaterWake;
import net.minecraft.client.renderer.ActiveRenderInfo;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.crash.CrashReport;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.crash.ICrashReportDetail;
import net.minecraft.entity.Entity;
import net.optifine.Config;
import net.optifine.Reflector;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ReportedException;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class ParticleManager {
	private static final ResourceLocation PARTICLE_TEXTURES = new ResourceLocation("textures/particle/particles.png");
	protected World world;
	private final ArrayDeque<Particle>[][] fxLayers = new ArrayDeque[4][];
	private final Queue<ParticleEmitter> particleEmitters = Queues.newArrayDeque();
	private final TextureManager renderer;
	private final Random rand = new Random();
	private final Map<Integer, IParticleFactory> particleTypes = Maps.newHashMap();
	private final Queue<Particle> queueEntityFX = Queues.newArrayDeque();

	public ParticleManager(World worldx, TextureManager texturemanager) {
		this.world = worldx;
		this.renderer = texturemanager;

		for (int i = 0; i < 4; ++i) {
			this.fxLayers[i] = new ArrayDeque[2];

			for (int j = 0; j < 2; ++j) {
				this.fxLayers[i][j] = Queues.newArrayDeque();
			}
		}

		this.registerVanillaParticles();
	}

	private void registerVanillaParticles() {
		this.registerParticle(EnumParticleTypes.EXPLOSION_NORMAL.getParticleID(), new ParticleExplosion.Factory());
		this.registerParticle(EnumParticleTypes.SPIT.getParticleID(), new ParticleSpit.Factory());
		this.registerParticle(EnumParticleTypes.WATER_BUBBLE.getParticleID(), new ParticleBubble.Factory());
		this.registerParticle(EnumParticleTypes.WATER_SPLASH.getParticleID(), new ParticleSplash.Factory());
		this.registerParticle(EnumParticleTypes.WATER_WAKE.getParticleID(), new ParticleWaterWake.Factory());
		this.registerParticle(EnumParticleTypes.WATER_DROP.getParticleID(), new ParticleRain.Factory());
		this.registerParticle(EnumParticleTypes.SUSPENDED.getParticleID(), new ParticleSuspend.Factory());
		this.registerParticle(EnumParticleTypes.SUSPENDED_DEPTH.getParticleID(), new ParticleSuspendedTown.Factory());
		this.registerParticle(EnumParticleTypes.CRIT.getParticleID(), new ParticleCrit.Factory());
		this.registerParticle(EnumParticleTypes.CRIT_MAGIC.getParticleID(), new ParticleCrit.MagicFactory());
		this.registerParticle(EnumParticleTypes.SMOKE_NORMAL.getParticleID(), new ParticleSmokeNormal.Factory());
		this.registerParticle(EnumParticleTypes.SMOKE_LARGE.getParticleID(), new ParticleSmokeLarge.Factory());
		this.registerParticle(EnumParticleTypes.SPELL.getParticleID(), new ParticleSpell.Factory());
		this.registerParticle(EnumParticleTypes.SPELL_INSTANT.getParticleID(), new ParticleSpell.InstantFactory());
		this.registerParticle(EnumParticleTypes.SPELL_MOB.getParticleID(), new ParticleSpell.MobFactory());
		this.registerParticle(EnumParticleTypes.SPELL_MOB_AMBIENT.getParticleID(), new ParticleSpell.AmbientMobFactory());
		this.registerParticle(EnumParticleTypes.SPELL_WITCH.getParticleID(), new ParticleSpell.WitchFactory());
		this.registerParticle(EnumParticleTypes.DRIP_WATER.getParticleID(), new ParticleDrip.WaterFactory());
		this.registerParticle(EnumParticleTypes.DRIP_LAVA.getParticleID(), new ParticleDrip.LavaFactory());
		this.registerParticle(EnumParticleTypes.VILLAGER_ANGRY.getParticleID(), new ParticleHeart.AngryVillagerFactory());
		this.registerParticle(EnumParticleTypes.VILLAGER_HAPPY.getParticleID(), new ParticleSuspendedTown.HappyVillagerFactory());
		this.registerParticle(EnumParticleTypes.TOWN_AURA.getParticleID(), new ParticleSuspendedTown.Factory());
		this.registerParticle(EnumParticleTypes.NOTE.getParticleID(), new ParticleNote.Factory());
		this.registerParticle(EnumParticleTypes.PORTAL.getParticleID(), new ParticlePortal.Factory());
		this.registerParticle(EnumParticleTypes.ENCHANTMENT_TABLE.getParticleID(), new ParticleEnchantmentTable.EnchantmentTable());
		this.registerParticle(EnumParticleTypes.FLAME.getParticleID(), new ParticleFlame.Factory());
		this.registerParticle(EnumParticleTypes.LAVA.getParticleID(), new ParticleLava.Factory());
		this.registerParticle(EnumParticleTypes.FOOTSTEP.getParticleID(), new ParticleFootStep.Factory());
		this.registerParticle(EnumParticleTypes.CLOUD.getParticleID(), new ParticleCloud.Factory());
		this.registerParticle(EnumParticleTypes.REDSTONE.getParticleID(), new ParticleRedstone.Factory());
		this.registerParticle(EnumParticleTypes.FALLING_DUST.getParticleID(), new ParticleFallingDust.Factory());
		this.registerParticle(EnumParticleTypes.SNOWBALL.getParticleID(), new ParticleBreaking.SnowballFactory());
		this.registerParticle(EnumParticleTypes.SNOW_SHOVEL.getParticleID(), new ParticleSnowShovel.Factory());
		this.registerParticle(EnumParticleTypes.SLIME.getParticleID(), new ParticleBreaking.SlimeFactory());
		this.registerParticle(EnumParticleTypes.HEART.getParticleID(), new ParticleHeart.Factory());
		this.registerParticle(EnumParticleTypes.BARRIER.getParticleID(), new Barrier.Factory());
		this.registerParticle(EnumParticleTypes.ITEM_CRACK.getParticleID(), new ParticleBreaking.Factory());
		this.registerParticle(EnumParticleTypes.BLOCK_CRACK.getParticleID(), new ParticleDigging.Factory());
		this.registerParticle(EnumParticleTypes.BLOCK_DUST.getParticleID(), new ParticleBlockDust.Factory());
		this.registerParticle(EnumParticleTypes.EXPLOSION_HUGE.getParticleID(), new ParticleExplosionHuge.Factory());
		this.registerParticle(EnumParticleTypes.EXPLOSION_LARGE.getParticleID(), new ParticleExplosionLarge.Factory());
		this.registerParticle(EnumParticleTypes.FIREWORKS_SPARK.getParticleID(), new ParticleFirework.Factory());
		this.registerParticle(EnumParticleTypes.MOB_APPEARANCE.getParticleID(), new ParticleMobAppearance.Factory());
		this.registerParticle(EnumParticleTypes.DRAGON_BREATH.getParticleID(), new ParticleDragonBreath.Factory());
		this.registerParticle(EnumParticleTypes.END_ROD.getParticleID(), new ParticleEndRod.Factory());
		this.registerParticle(EnumParticleTypes.DAMAGE_INDICATOR.getParticleID(), new ParticleCrit.DamageIndicatorFactory());
		this.registerParticle(EnumParticleTypes.SWEEP_ATTACK.getParticleID(), new ParticleSweepAttack.Factory());
		this.registerParticle(EnumParticleTypes.TOTEM.getParticleID(), new ParticleTotem.Factory());
	}

	public void registerParticle(int i, IParticleFactory iparticlefactory) {
		this.particleTypes.put(Integer.valueOf(i), iparticlefactory);
	}

	public void emitParticleAtEntity(Entity entity, EnumParticleTypes enumparticletypes) {
		if (Minecraft.getMinecraft().gameSettings.particleSetting == 3) {
			return;
		}
		this.particleEmitters.add(new ParticleEmitter(this.world, entity, enumparticletypes));
	}

	public void emitParticleAtEntity(Entity entity, EnumParticleTypes enumparticletypes, int i) {
		if (Minecraft.getMinecraft().gameSettings.particleSetting == 3) {
			return;
		}
		this.particleEmitters.add(new ParticleEmitter(this.world, entity, enumparticletypes, i));
	}

	@Nullable
	public Particle spawnEffectParticle(int i, double d0, double d1, double d2, double d3, double d4, double d5, int... aint) {
		if (Minecraft.getMinecraft().gameSettings.particleSetting == 3) {
			return null;
		}
		IParticleFactory iparticlefactory = this.particleTypes.get(Integer.valueOf(i));
		if (iparticlefactory != null) {
			Particle particle = iparticlefactory.createParticle(i, this.world, d0, d1, d2, d3, d4, d5, aint);
			if (particle != null) {
				this.addEffect(particle);
				return particle;
			}
		}

		return null;
	}

	public void addEffect(Particle particle) {
		if (particle != null) {
			if (!(particle instanceof ParticleFirework.Spark) || Config.isFireworkParticles()) {
				this.queueEntityFX.add(particle);
			}
		}
	}

	public void updateEffects() {
		for (int i = 0; i < 4; ++i) {
			this.updateEffectLayer(i);
		}

		if (!this.particleEmitters.isEmpty()) {
			ArrayList arraylist = Lists.newArrayList();

			for (ParticleEmitter particleemitter : this.particleEmitters) {
				particleemitter.onUpdate();
				if (!particleemitter.isAlive()) {
					arraylist.add(particleemitter);
				}
			}

			this.particleEmitters.removeAll(arraylist);
		}

		if (!this.queueEntityFX.isEmpty()) {
			for (Particle particle = this.queueEntityFX.poll(); particle != null; particle = this.queueEntityFX.poll()) {
				int j = particle.getFXLayer();
				int k = particle.isTransparent() ? 0 : 1;
				if (this.fxLayers[j][k].size() >= 16384) {
					this.fxLayers[j][k].removeFirst();
				}

				if (!(particle instanceof Barrier) || !ParticleManager.reuseBarrierParticle(particle, this.fxLayers[j][k])) {
					this.fxLayers[j][k].add(particle);
				}
			}
		}

	}

	private void updateEffectLayer(int i) {
		this.world.theProfiler.startSection(i + "");

		for (int j = 0; j < 2; ++j) {
			this.world.theProfiler.startSection(j + "");
			ParticleManager.tickParticleList(this.fxLayers[i][j]);
			this.world.theProfiler.endSection();
		}

		this.world.theProfiler.endSection();
	}

	private static void tickParticleList(Queue<Particle> queue) {
		if (!queue.isEmpty()) {
			Iterator iterator = queue.iterator();

			while (iterator.hasNext()) {
				Particle particle = (Particle) iterator.next();
				ParticleManager.tickParticle(particle);
				if (!particle.isAlive()) {
					iterator.remove();
				}
			}
		}

	}

	private static void tickParticle(final Particle particle) {
		try {
			if (Minecraft.getMinecraft().gameSettings.particleSetting == 3) {
				particle.setExpired();
				return;
			} 
			particle.onUpdate();
		} catch (Throwable throwable) {
			CrashReport crashreport = CrashReport.makeCrashReport(throwable, "Ticking Particle");
			CrashReportCategory crashreportcategory = crashreport.makeCategory("Particle being ticked");
			final int i = particle.getFXLayer();
			crashreportcategory.setDetail("Particle", new ICrashReportDetail<String>() {
				@Override
				public String call() throws Exception {
					return particle.toString();
				}
			});
			crashreportcategory.setDetail("Particle Type", new ICrashReportDetail<String>() {
				@Override
				public String call() throws Exception {
					return i == 0 ? "MISC_TEXTURE" : (i == 1 ? "TERRAIN_TEXTURE" : (i == 3 ? "ENTITY_PARTICLE_TEXTURE" : "Unknown - " + i));
				}
			});
			throw new ReportedException(crashreport);
		}
	}

	public void renderParticles(Entity entity, float f) {
		if (Minecraft.getMinecraft().gameSettings.particleSetting == 3) {
			return;
		}
		float f1 = ActiveRenderInfo.getRotationX();
		float f2 = ActiveRenderInfo.getRotationZ();
		float f3 = ActiveRenderInfo.getRotationYZ();
		float f4 = ActiveRenderInfo.getRotationXY();
		float f5 = ActiveRenderInfo.getRotationXZ();
		Particle.interpPosX = entity.lastTickPosX + (entity.posX - entity.lastTickPosX) * f;
		Particle.interpPosY = entity.lastTickPosY + (entity.posY - entity.lastTickPosY) * f;
		Particle.interpPosZ = entity.lastTickPosZ + (entity.posZ - entity.lastTickPosZ) * f;
		Particle.cameraViewDir = entity.getLook(f);
		GlStateManager.enableBlend();
		GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
		GlStateManager.alphaFunc(516, 0.003921569F);

		for (int i = 0; i < 3; ++i) {
			final int j = i;

			for (int k = 0; k < 2; ++k) {
				if (!this.fxLayers[j][k].isEmpty()) {
					switch (k) {
					case 0:
						GlStateManager.depthMask(false);
						break;
					case 1:
						GlStateManager.depthMask(true);
					}

					switch (j) {
					case 0:
					default:
						this.renderer.bindTexture(PARTICLE_TEXTURES);
						break;
					case 1:
						this.renderer.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
					}

					GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
					Tessellator tessellator = Tessellator.getInstance();
					VertexBuffer vertexbuffer = tessellator.getBuffer();
					vertexbuffer.begin(7, DefaultVertexFormats.PARTICLE_POSITION_TEX_COLOR_LMAP);

					for (final Particle particle : this.fxLayers[j][k]) {
						try {
							particle.renderParticle(vertexbuffer, entity, f, f1, f5, f2, f3, f4);
						} catch (Throwable throwable) {
							CrashReport crashreport = CrashReport.makeCrashReport(throwable, "Rendering Particle");
							CrashReportCategory crashreportcategory = crashreport.makeCategory("Particle being rendered");
							crashreportcategory.setDetail("Particle", new ICrashReportDetail<String>() {
								@Override
								public String call() throws Exception {
									return particle.toString();
								}
							});
							crashreportcategory.setDetail("Particle Type", new ICrashReportDetail<String>() {
								@Override
								public String call() throws Exception {
									return j == 0 ? "MISC_TEXTURE" : (j == 1 ? "TERRAIN_TEXTURE" : (j == 3 ? "ENTITY_PARTICLE_TEXTURE" : "Unknown - " + j));
								}
							});
							throw new ReportedException(crashreport);
						}
					}

					tessellator.draw();
				}
			}
		}

		GlStateManager.depthMask(true);
		GlStateManager.disableBlend();
		GlStateManager.alphaFunc(516, 0.1F);
	}

	public void renderLitParticles(Entity entity, float f) {
		if (Minecraft.getMinecraft().gameSettings.particleSetting == 3) {
			return;
		}
		float f1 = 0.017453292F;
		float f2 = MathHelper.cos(entity.rotationYaw * 0.017453292F);
		float f3 = MathHelper.sin(entity.rotationYaw * 0.017453292F);
		float f4 = -f3 * MathHelper.sin(entity.rotationPitch * 0.017453292F);
		float f5 = f2 * MathHelper.sin(entity.rotationPitch * 0.017453292F);
		float f6 = MathHelper.cos(entity.rotationPitch * 0.017453292F);

		for (int i = 0; i < 2; ++i) {
			ArrayDeque<Particle> arraydeque = this.fxLayers[3][i];
			if (!arraydeque.isEmpty()) {
				Tessellator tessellator = Tessellator.getInstance();
				VertexBuffer vertexbuffer = tessellator.getBuffer();

				for (Particle particle : arraydeque) {
					particle.renderParticle(vertexbuffer, entity, f, f2, f6, f3, f4, f5);
				}
			}
		}

	}

	public void clearEffects(@Nullable World worldx) {
		this.world = worldx;

		for (int i = 0; i < 4; ++i) {
			for (int j = 0; j < 2; ++j) {
				this.fxLayers[i][j].clear();
			}
		}

		this.particleEmitters.clear();
	}

	public void addBlockDestroyEffects(BlockPos blockpos, IBlockState iblockstate) {
		if (Minecraft.getMinecraft().gameSettings.particleSetting == 3) {
			return;
		}
		boolean flag;
		if (Reflector.ForgeBlock_addDestroyEffects.exists() && Reflector.ForgeBlock_isAir.exists()) {
			Block block = iblockstate.getBlock();
			flag = !Reflector.callBoolean(block, Reflector.ForgeBlock_isAir, new Object[] { iblockstate, this.world, blockpos })
					&& !Reflector.callBoolean(block, Reflector.ForgeBlock_addDestroyEffects, new Object[] { this.world, blockpos, this });
		} else {
			flag = iblockstate.getMaterial() != Material.AIR;
		}

		if (flag) {
			iblockstate = iblockstate.getActualState(this.world, blockpos);
			boolean flag1 = true;

			for (int i = 0; i < 4; ++i) {
				for (int j = 0; j < 4; ++j) {
					for (int k = 0; k < 4; ++k) {
						double d0 = (i + 0.5D) / 4.0D;
						double d1 = (j + 0.5D) / 4.0D;
						double d2 = (k + 0.5D) / 4.0D;
						this.addEffect(
								(new ParticleDigging(this.world, blockpos.getX() + d0, blockpos.getY() + d1, blockpos.getZ() + d2, d0 - 0.5D, d1 - 0.5D, d2 - 0.5D, iblockstate))
										.setBlockPos(blockpos));
					}
				}
			}
		}

	}

	public void addBlockHitEffects(BlockPos blockpos, EnumFacing enumfacing) {
		if (Minecraft.getMinecraft().gameSettings.particleSetting == 3) {
			return;
		}
		IBlockState iblockstate = this.world.getBlockState(blockpos);
		if (iblockstate.getRenderType() != EnumBlockRenderType.INVISIBLE) {
			int i = blockpos.getX();
			int j = blockpos.getY();
			int k = blockpos.getZ();
			float f = 0.1F;
			AxisAlignedBB axisalignedbb = iblockstate.getBoundingBox(this.world, blockpos);
			double d0 = i + this.rand.nextDouble() * (axisalignedbb.maxX - axisalignedbb.minX - 0.20000000298023224D) + 0.10000000149011612D + axisalignedbb.minX;
			double d1 = j + this.rand.nextDouble() * (axisalignedbb.maxY - axisalignedbb.minY - 0.20000000298023224D) + 0.10000000149011612D + axisalignedbb.minY;
			double d2 = k + this.rand.nextDouble() * (axisalignedbb.maxZ - axisalignedbb.minZ - 0.20000000298023224D) + 0.10000000149011612D + axisalignedbb.minZ;
			if (enumfacing == EnumFacing.DOWN) {
				d1 = j + axisalignedbb.minY - 0.10000000149011612D;
			}

			if (enumfacing == EnumFacing.UP) {
				d1 = j + axisalignedbb.maxY + 0.10000000149011612D;
			}

			if (enumfacing == EnumFacing.NORTH) {
				d2 = k + axisalignedbb.minZ - 0.10000000149011612D;
			}

			if (enumfacing == EnumFacing.SOUTH) {
				d2 = k + axisalignedbb.maxZ + 0.10000000149011612D;
			}

			if (enumfacing == EnumFacing.WEST) {
				d0 = i + axisalignedbb.minX - 0.10000000149011612D;
			}

			if (enumfacing == EnumFacing.EAST) {
				d0 = i + axisalignedbb.maxX + 0.10000000149011612D;
			}

			this.addEffect((new ParticleDigging(this.world, d0, d1, d2, 0.0D, 0.0D, 0.0D, iblockstate)).setBlockPos(blockpos).multiplyVelocity(0.2F).multipleParticleScaleBy(0.6F));
		}

	}

	public String getStatistics() {
		int i = 0;

		for (int j = 0; j < 4; ++j) {
			for (int k = 0; k < 2; ++k) {
				i += this.fxLayers[j][k].size();
			}
		}

		return "" + i;
	}

	private static boolean reuseBarrierParticle(Particle particle, ArrayDeque<Particle> arraydeque) {
		if (Minecraft.getMinecraft().gameSettings.particleSetting == 3) {
			return false;
		}
		for (Particle particle1 : arraydeque) {
			if (particle1 instanceof Barrier && particle.prevPosX == particle1.prevPosX && particle.prevPosY == particle1.prevPosY && particle.prevPosZ == particle1.prevPosZ) {
				particle1.particleAge = 0;
				return true;
			}
		}

		return false;
	}

	public void addBlockHitEffects(BlockPos blockpos, RayTraceResult raytraceresult) {
		if (Minecraft.getMinecraft().gameSettings.particleSetting == 3) {
			return;
		}
		IBlockState iblockstate = this.world.getBlockState(blockpos);
		if (iblockstate != null) {
			boolean flag = Reflector.callBoolean(iblockstate.getBlock(), Reflector.ForgeBlock_addHitEffects, new Object[] { iblockstate, this.world, raytraceresult, this });
			if (iblockstate != null && !flag) {
				this.addBlockHitEffects(blockpos, raytraceresult.sideHit);
			}

		}
	}
}
