package net.minecraft.client.gui;

import java.io.IOException;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiOptionButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.GameSettings;
import net.optifine.Config;
import net.optifine.GuiAnimationSettingsOF;
import net.optifine.GuiDetailSettingsOF;
import net.optifine.GuiOptionButtonOF;
import net.optifine.GuiOptionSliderOF;
import net.optifine.GuiOtherSettingsOF;
import net.optifine.GuiPerformanceSettingsOF;
import net.optifine.GuiQualitySettingsOF;
import net.optifine.Lang;
import net.optifine.TooltipManager;
import shadersmod.client.GuiShaders;

public class GuiVideoSettings extends GuiScreen {
	private GuiScreen parentGuiScreen;
	protected String screenTitle = "Video Settings";
	private GameSettings guiGameSettings;
	private static GameSettings.Options[] videoOptions = new GameSettings.Options[] { GameSettings.Options.GRAPHICS, GameSettings.Options.RENDER_DISTANCE,
			GameSettings.Options.AMBIENT_OCCLUSION, GameSettings.Options.FRAMERATE_LIMIT, GameSettings.Options.AO_LEVEL, GameSettings.Options.VIEW_BOBBING,
			GameSettings.Options.GUI_SCALE, GameSettings.Options.USE_VBO, GameSettings.Options.GAMMA, GameSettings.Options.ATTACK_INDICATOR, GameSettings.Options.DYNAMIC_LIGHTS,
			GameSettings.Options.DYNAMIC_FOV };
	private TooltipManager tooltipManager = new TooltipManager(this);

	public GuiVideoSettings(GuiScreen guiscreen, GameSettings gamesettings) {
		this.parentGuiScreen = guiscreen;
		this.guiGameSettings = gamesettings;
	}

	@Override
	public void initGui() {
		this.screenTitle = I18n.format("options.videoTitle", new Object[0]);
		this.buttonList.clear();

		for (int i = 0; i < videoOptions.length; ++i) {
			GameSettings.Options gamesettings$options = videoOptions[i];
			if (gamesettings$options != null) {
				int j = this.width / 2 - 155 + i % 2 * 160;
				int k = this.height / 6 + 21 * (i / 2) - 12;
				if (gamesettings$options.getEnumFloat()) {
					this.buttonList.add(new GuiOptionSliderOF(gamesettings$options.returnEnumOrdinal(), j, k, gamesettings$options));
				} else {
					this.buttonList.add(
							new GuiOptionButtonOF(gamesettings$options.returnEnumOrdinal(), j, k, gamesettings$options, this.guiGameSettings.getKeyBinding(gamesettings$options)));
				}
			}
		}

		int l = this.height / 6 + 21 * (videoOptions.length / 2) - 12;
		int i1 = 0;
		i1 = this.width / 2 - 155 + 0;
		this.buttonList.add(new GuiOptionButton(231, i1, l, Lang.get("of.options.shaders")));
		i1 = this.width / 2 - 155 + 160;
		this.buttonList.add(new GuiOptionButton(202, i1, l, Lang.get("of.options.quality")));
		l = l + 21;
		i1 = this.width / 2 - 155 + 0;
		this.buttonList.add(new GuiOptionButton(201, i1, l, Lang.get("of.options.details")));
		i1 = this.width / 2 - 155 + 160;
		this.buttonList.add(new GuiOptionButton(212, i1, l, Lang.get("of.options.performance")));
		l = l + 21;
		i1 = this.width / 2 - 155 + 0;
		this.buttonList.add(new GuiOptionButton(211, i1, l, Lang.get("of.options.animations")));
		i1 = this.width / 2 - 155 + 160;
		this.buttonList.add(new GuiOptionButton(222, i1, l, Lang.get("of.options.other")));
		l = l + 21;
		this.buttonList.add(new GuiButton(200, this.width / 2 - 100, this.height / 6 + 168 + 11, I18n.format("gui.done", new Object[0])));
	}

	@Override
	protected void actionPerformed(GuiButton guibutton) throws IOException {
		if (guibutton.enabled) {
			int i = this.guiGameSettings.guiScale;
			if (guibutton.id < 200 && guibutton instanceof GuiOptionButton) {
				this.guiGameSettings.setOptionValue(((GuiOptionButton) guibutton).returnEnumOptions(), 1);
				guibutton.displayString = this.guiGameSettings.getKeyBinding(GameSettings.Options.getEnumOptions(guibutton.id));
			}

			if (guibutton.id == 200) {
				this.mc.gameSettings.saveOptions();
				this.mc.displayGuiScreen(this.parentGuiScreen);
			}

			if (this.guiGameSettings.guiScale != i) {
				ScaledResolution scaledresolution = new ScaledResolution(this.mc);
				int j = scaledresolution.getScaledWidth();
				int k = scaledresolution.getScaledHeight();
				this.setWorldAndResolution(this.mc, j, k);
			}

			if (guibutton.id == 201) {
				this.mc.gameSettings.saveOptions();
				GuiDetailSettingsOF guidetailsettingsof = new GuiDetailSettingsOF(this, this.guiGameSettings);
				this.mc.displayGuiScreen(guidetailsettingsof);
			}

			if (guibutton.id == 202) {
				this.mc.gameSettings.saveOptions();
				GuiQualitySettingsOF guiqualitysettingsof = new GuiQualitySettingsOF(this, this.guiGameSettings);
				this.mc.displayGuiScreen(guiqualitysettingsof);
			}

			if (guibutton.id == 211) {
				this.mc.gameSettings.saveOptions();
				GuiAnimationSettingsOF guianimationsettingsof = new GuiAnimationSettingsOF(this, this.guiGameSettings);
				this.mc.displayGuiScreen(guianimationsettingsof);
			}

			if (guibutton.id == 212) {
				this.mc.gameSettings.saveOptions();
				GuiPerformanceSettingsOF guiperformancesettingsof = new GuiPerformanceSettingsOF(this, this.guiGameSettings);
				this.mc.displayGuiScreen(guiperformancesettingsof);
			}

			if (guibutton.id == 222) {
				this.mc.gameSettings.saveOptions();
				GuiOtherSettingsOF guiothersettingsof = new GuiOtherSettingsOF(this, this.guiGameSettings);
				this.mc.displayGuiScreen(guiothersettingsof);
			}

			if (guibutton.id == 231) {
				if (Config.isAntialiasing() || Config.isAntialiasingConfigured()) {
					Config.showGuiMessage(Lang.get("of.message.shaders.aa1"), Lang.get("of.message.shaders.aa2"));
					return;
				}

				if (Config.isAnisotropicFiltering()) {
					Config.showGuiMessage(Lang.get("of.message.shaders.af1"), Lang.get("of.message.shaders.af2"));
					return;
				}

				if (Config.isFastRender()) {
					Config.showGuiMessage(Lang.get("of.message.shaders.fr1"), Lang.get("of.message.shaders.fr2"));
					return;
				}

				if (Config.getGameSettings().anaglyph) {
					Config.showGuiMessage(Lang.get("of.message.shaders.an1"), Lang.get("of.message.shaders.an2"));
					return;
				}

				this.mc.gameSettings.saveOptions();
				GuiShaders guishaders = new GuiShaders(this, this.guiGameSettings);
				this.mc.displayGuiScreen(guishaders);
			}

		}
	}

	@Override
	public void drawScreen(int i, int j, float f) {
		this.drawDefaultBackground();
		this.drawCenteredString(this.fontRendererObj, this.screenTitle, this.width / 2, 15, 16777215);
		String s = Config.getVersionDisplay();
		this.drawString(this.fontRendererObj, s, 2, this.height - 10, 8421504);
		String s2 = "Minecraft 1.11.2";
		int k = this.fontRendererObj.getStringWidth(s2);
		this.drawString(this.fontRendererObj, s2, this.width - k - 2, this.height - 10, 8421504);
		super.drawScreen(i, j, f);
		this.tooltipManager.drawTooltips(i, j, this.buttonList);
	}

	public static int getButtonWidth(GuiButton guibutton) {
		return guibutton.width;
	}

	public static int getButtonHeight(GuiButton guibutton) {
		return guibutton.height;
	}

	public static void drawGradientRect(GuiScreen guiscreen, int i, int j, int k, int l, int i1, int j1) {
		guiscreen.drawGradientRect(i, j, k, l, i1, j1);
	}
}
