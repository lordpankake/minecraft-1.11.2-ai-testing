package net.minecraft.client.gui.ingame;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.UnmodifiableIterator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import net.minecraft.block.Block;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.ClientBrandRetriever;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.optifine.Reflector;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.WorldType;
import net.minecraft.world.chunk.Chunk;
import org.lwjgl.opengl.Display;

public class GuiOverlayDebug extends Gui {
	private final Minecraft mc;
	private final FontRenderer fontRenderer;

	public GuiOverlayDebug(Minecraft minecraft) {
		this.mc = minecraft;
		this.fontRenderer = minecraft.fontRendererObj;
	}

	public void renderDebugInfo(ScaledResolution scaledresolution) {
		this.mc.mcProfiler.startSection("debug");
		GlStateManager.pushMatrix();
		this.renderDebugInfoLeft();
		this.renderDebugInfoRight(scaledresolution);
		GlStateManager.popMatrix();
		this.mc.mcProfiler.endSection();
	}

	protected void renderDebugInfoLeft() {
		List list = this.call();
		list.add("");
		list.add("Debug: Pie [shift]: " + (this.mc.gameSettings.showDebugProfilerChart ? "visible" : "hidden") + " FPS [alt]: "
				+ (this.mc.gameSettings.showLagometer ? "visible" : "hidden"));
		list.add("For help: press F3 + Q");

		for (int i = 0; i < list.size(); ++i) {
			String s = (String) list.get(i);
			if (!Strings.isNullOrEmpty(s)) {
				int j = this.fontRenderer.FONT_HEIGHT;
				int k = this.fontRenderer.getStringWidth(s);
				boolean flag = true;
				int l = 2 + j * i;
				drawRect(1, l - 1, 2 + k + 1, l + j - 1, -1873784752);
				this.fontRenderer.drawString(s, 2, l, 14737632);
			}
		}

	}

	protected void renderDebugInfoRight(ScaledResolution scaledresolution) {
		List list = this.getDebugInfoRight();

		for (int i = 0; i < list.size(); ++i) {
			String s = (String) list.get(i);
			if (!Strings.isNullOrEmpty(s)) {
				int j = this.fontRenderer.FONT_HEIGHT;
				int k = this.fontRenderer.getStringWidth(s);
				int l = scaledresolution.getScaledWidth() - 2 - k;
				int i1 = 2 + j * i;
				drawRect(l - 1, i1 - 1, l + k + 1, i1 + j - 1, -1873784752);
				this.fontRenderer.drawString(s, l, i1, 14737632);
			}
		}

	}

	protected List<String> call() {
		BlockPos blockpos = new BlockPos(this.mc.getRenderViewEntity().posX, this.mc.getRenderViewEntity().getEntityBoundingBox().minY, this.mc.getRenderViewEntity().posZ);
		if (this.mc.isReducedDebug()) {
			return Lists.newArrayList(new String[] { "Minecraft 1.11.2 (" + this.mc.getVersion() + "/" + ClientBrandRetriever.getClientModName() + ")", this.mc.debug,
					this.mc.renderGlobal.getDebugInfoRenders(), this.mc.renderGlobal.getDebugInfoEntities(),
					"P: " + this.mc.effectRenderer.getStatistics() + ". T: " + this.mc.world.getDebugLoadedEntities(), this.mc.world.getProviderName(), "",
					String.format("Chunk-relative: %d %d %d",
							new Object[] { Integer.valueOf(blockpos.getX() & 15), Integer.valueOf(blockpos.getY() & 15), Integer.valueOf(blockpos.getZ() & 15) }) });
		} else {
			Entity entity = this.mc.getRenderViewEntity();
			EnumFacing enumfacing = entity.getHorizontalFacing();
			String s = "Invalid";
			switch (enumfacing) {
			case NORTH:
				s = "Towards negative Z";
				break;
			case SOUTH:
				s = "Towards positive Z";
				break;
			case WEST:
				s = "Towards negative X";
				break;
			case EAST:
				s = "Towards positive X";
			default:
				break;
			}

			ArrayList arraylist = Lists.newArrayList(new String[] {
					"Minecraft 1.11.2 (" + this.mc.getVersion() + "/" + ClientBrandRetriever.getClientModName()
							+ ("release".equalsIgnoreCase(this.mc.getVersionType()) ? "" : "/" + this.mc.getVersionType()) + ")",
					this.mc.debug, this.mc.renderGlobal.getDebugInfoRenders(), this.mc.renderGlobal.getDebugInfoEntities(),
					"P: " + this.mc.effectRenderer.getStatistics() + ". T: " + this.mc.world.getDebugLoadedEntities(), this.mc.world.getProviderName(), "",
					String.format("XYZ: %.3f / %.5f / %.3f",
							new Object[] { Double.valueOf(this.mc.getRenderViewEntity().posX), Double.valueOf(this.mc.getRenderViewEntity().getEntityBoundingBox().minY),
									Double.valueOf(this.mc.getRenderViewEntity().posZ) }),
					String.format("Block: %d %d %d", new Object[] { Integer.valueOf(blockpos.getX()), Integer.valueOf(blockpos.getY()), Integer.valueOf(blockpos.getZ()) }),
					String.format("Chunk: %d %d %d in %d %d %d",
							new Object[] { Integer.valueOf(blockpos.getX() & 15), Integer.valueOf(blockpos.getY() & 15), Integer.valueOf(blockpos.getZ() & 15),
									Integer.valueOf(blockpos.getX() >> 4), Integer.valueOf(blockpos.getY() >> 4), Integer.valueOf(blockpos.getZ() >> 4) }),
					String.format("Facing: %s (%s) (%.1f / %.1f)", new Object[] { enumfacing, s, Float.valueOf(MathHelper.wrapDegrees(entity.rotationYaw)),
							Float.valueOf(MathHelper.wrapDegrees(entity.rotationPitch)) }) });
			if (this.mc.world != null) {
				Chunk chunk = this.mc.world.getChunkFromBlockCoords(blockpos);
				if (this.mc.world.isBlockLoaded(blockpos) && blockpos.getY() >= 0 && blockpos.getY() < 256) {
					if (!chunk.isEmpty()) {
						arraylist.add("Biome: " + chunk.getBiome(blockpos, this.mc.world.getBiomeProvider()).getBiomeName());
						arraylist.add("Light: " + chunk.getLightSubtracted(blockpos, 0) + " (" + chunk.getLightFor(EnumSkyBlock.SKY, blockpos) + " sky, "
								+ chunk.getLightFor(EnumSkyBlock.BLOCK, blockpos) + " block)");
						DifficultyInstance difficultyinstance = this.mc.world.getDifficultyForLocation(blockpos);
						if (this.mc.isIntegratedServerRunning() && this.mc.getIntegratedServer() != null) {
							EntityPlayerMP entityplayermp = this.mc.getIntegratedServer().getPlayerList().getPlayerByUUID(this.mc.player.getUniqueID());
							if (entityplayermp != null) {
								difficultyinstance = entityplayermp.world.getDifficultyForLocation(new BlockPos(entityplayermp));
							}
						}

						arraylist.add(String.format("Local Difficulty: %.2f // %.2f (Day %d)", new Object[] { Float.valueOf(difficultyinstance.getAdditionalDifficulty()),
								Float.valueOf(difficultyinstance.getClampedAdditionalDifficulty()), Long.valueOf(this.mc.world.getWorldTime() / 24000L) }));
					} else {
						arraylist.add("Waiting for chunk...");
					}
				} else {
					arraylist.add("Outside of world...");
				}
			}

			if (this.mc.entityRenderer != null && this.mc.entityRenderer.isShaderActive()) {
				arraylist.add("Shader: " + this.mc.entityRenderer.getShaderGroup().getShaderGroupName());
			}

			if (this.mc.objectMouseOver != null && this.mc.objectMouseOver.typeOfHit == RayTraceResult.Type.BLOCK && this.mc.objectMouseOver.getBlockPos() != null) {
				BlockPos blockpos1 = this.mc.objectMouseOver.getBlockPos();
				arraylist.add(String.format("Looking at: %d %d %d",
						new Object[] { Integer.valueOf(blockpos1.getX()), Integer.valueOf(blockpos1.getY()), Integer.valueOf(blockpos1.getZ()) }));
			}

			return arraylist;
		}
	}

	protected <T extends Comparable<T>> List<String> getDebugInfoRight() {
		long i = Runtime.getRuntime().maxMemory();
		long j = Runtime.getRuntime().totalMemory();
		long k = Runtime.getRuntime().freeMemory();
		long l = j - k;
		ArrayList arraylist = Lists
				.newArrayList(new String[] { String.format("Java: %s %dbit", new Object[] { System.getProperty("java.version"), Integer.valueOf(this.mc.isJava64bit() ? 64 : 32) }),
						String.format("Mem: % 2d%% %03d/%03dMB", new Object[] { Long.valueOf(l * 100L / i), Long.valueOf(bytesToMb(l)), Long.valueOf(bytesToMb(i)) }),
						String.format("Allocated: % 2d%% %03dMB", new Object[] { Long.valueOf(j * 100L / i), Long.valueOf(bytesToMb(j)) }), "",
						String.format("CPU: %s", new Object[] { OpenGlHelper.getCpu() }), "",
						String.format("Display: %dx%d (%s)",
								new Object[] { Integer.valueOf(Display.getWidth()), Integer.valueOf(Display.getHeight()), GlStateManager.glGetString(7936) }),
				GlStateManager.glGetString(7937), GlStateManager.glGetString(7938) });
		if (Reflector.FMLCommonHandler_getBrandings.exists()) {
			Object object = Reflector.call(Reflector.FMLCommonHandler_instance, new Object[0]);
			arraylist.add("");
			arraylist.addAll((Collection) Reflector.call(object, Reflector.FMLCommonHandler_getBrandings, new Object[] { Boolean.valueOf(false) }));
		}

		if (this.mc.isReducedDebug()) {
			return arraylist;
		} else {
			if (this.mc.objectMouseOver != null && this.mc.objectMouseOver.typeOfHit == RayTraceResult.Type.BLOCK && this.mc.objectMouseOver.getBlockPos() != null) {
				BlockPos blockpos = this.mc.objectMouseOver.getBlockPos();
				IBlockState iblockstate = this.mc.world.getBlockState(blockpos);
				if (this.mc.world.getWorldType() != WorldType.DEBUG_WORLD) {
					iblockstate = iblockstate.getActualState(this.mc.world, blockpos);
				}

				arraylist.add("");
				arraylist.add(String.valueOf(Block.REGISTRY.getNameForObject(iblockstate.getBlock())));

				IProperty iproperty;
				String s;
				for (UnmodifiableIterator unmodifiableiterator = iblockstate.getProperties().entrySet().iterator(); unmodifiableiterator.hasNext(); arraylist
						.add(iproperty.getName() + ": " + s)) {
					Entry entry = (Entry) unmodifiableiterator.next();
					iproperty = (IProperty) entry.getKey();
					Comparable comparable = (Comparable) entry.getValue();
					s = iproperty.getName(comparable);
					if (Boolean.TRUE.equals(comparable)) {
						s = TextFormatting.GREEN + s;
					} else if (Boolean.FALSE.equals(comparable)) {
						s = TextFormatting.RED + s;
					}
				}
			}

			return arraylist;
		}
	}

	private static long bytesToMb(long i) {
		return i / 1024L / 1024L;
	}
}
