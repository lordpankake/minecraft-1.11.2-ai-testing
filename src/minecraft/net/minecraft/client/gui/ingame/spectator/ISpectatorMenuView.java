package net.minecraft.client.gui.ingame.spectator;

import java.util.List;
import net.minecraft.util.text.ITextComponent;

public interface ISpectatorMenuView {
	List<ISpectatorMenuObject> getItems();

	ITextComponent getPrompt();
}
