package net.minecraft.client.gui.ingame.spectator;

public interface ISpectatorMenuRecipient {
	void onSpectatorMenuClosed(SpectatorMenu p_175257_1_);
}
