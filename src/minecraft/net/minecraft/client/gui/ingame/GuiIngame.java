package net.minecraft.client.gui.ingame;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;
import javax.annotation.Nullable;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiNewChat;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.gui.ingame.GuiBossOverlay;
import net.minecraft.client.gui.ingame.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.MobEffects;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.scoreboard.Score;
import net.minecraft.scoreboard.ScoreObjective;
import net.minecraft.scoreboard.ScorePlayerTeam;
import net.minecraft.scoreboard.Scoreboard;
import net.optifine.Config;
import net.optifine.CustomColors;
import net.optifine.Reflector;
import net.optifine.ReflectorForge;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.FoodStats;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.border.WorldBorder;

public class GuiIngame extends Gui {
	private static final ResourceLocation VIGNETTE_TEX_PATH = new ResourceLocation("textures/misc/vignette.png");
	private static final ResourceLocation WIDGETS_TEX_PATH = new ResourceLocation("textures/gui/widgets.png");
	private static final ResourceLocation PUMPKIN_BLUR_TEX_PATH = new ResourceLocation("textures/misc/pumpkinblur.png");
	private final Random rand = new Random();
	private final Minecraft mc;
	private final RenderItem itemRenderer;
	private final GuiNewChat persistantChatGUI;
	private int updateCounter;
	private String overlayMessage = "";
	private int overlayMessageTime;
	private boolean animateOverlayMessageColor;
	public float prevVignetteBrightness = 1.0F;
	private int remainingHighlightTicks;
	private ItemStack highlightingItemStack = ItemStack.EMPTY;
	private final GuiOverlayDebug overlayDebug;
	private final GuiSubtitleOverlay overlaySubtitle;
	private final GuiSpectator spectatorGui;
	private final GuiPlayerTabOverlay overlayPlayerList;
	private final GuiBossOverlay overlayBoss;
	private int titlesTimer;
	private String displayedTitle = "";
	private String displayedSubTitle = "";
	private int titleFadeIn;
	private int titleDisplayTime;
	private int titleFadeOut;
	private int playerHealth;
	private int lastPlayerHealth;
	private long lastSystemTime;
	private long healthUpdateCounter;

	public GuiIngame(Minecraft minecraft) {
		this.mc = minecraft;
		this.itemRenderer = minecraft.getRenderItem();
		this.overlayDebug = new GuiOverlayDebug(minecraft);
		this.spectatorGui = new GuiSpectator(minecraft);
		this.persistantChatGUI = new GuiNewChat(minecraft);
		this.overlayPlayerList = new GuiPlayerTabOverlay(minecraft, this);
		this.overlayBoss = new GuiBossOverlay(minecraft);
		this.overlaySubtitle = new GuiSubtitleOverlay(minecraft);
		this.setDefaultTitlesTimes();
	}

	public void setDefaultTitlesTimes() {
		this.titleFadeIn = 10;
		this.titleDisplayTime = 70;
		this.titleFadeOut = 20;
	}

	public void renderGameOverlay(float f) {
		ScaledResolution scaledresolution = new ScaledResolution(this.mc);
		int i = scaledresolution.getScaledWidth();
		int j = scaledresolution.getScaledHeight();
		FontRenderer fontrenderer = this.getFontRenderer();
		GlStateManager.enableBlend();
		if (Config.isVignetteEnabled()) {
			this.renderVignette(this.mc.player.getBrightness(f), scaledresolution);
		} else {
			GlStateManager.enableDepth();
			GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE,
					GlStateManager.DestFactor.ZERO);
		}

		ItemStack itemstack = this.mc.player.inventory.armorItemInSlot(3);
		if (this.mc.gameSettings.thirdPersonView == 0 && itemstack.getItem() == Item.getItemFromBlock(Blocks.PUMPKIN)) {
			this.renderPumpkinOverlay(scaledresolution);
		}

		if (!this.mc.player.isPotionActive(MobEffects.NAUSEA)) {
			float f1 = this.mc.player.prevTimeInPortal + (this.mc.player.timeInPortal - this.mc.player.prevTimeInPortal) * f;
			if (f1 > 0.0F) {
				this.renderPortal(f1, scaledresolution);
			}
		}

		if (this.mc.playerController.isSpectator()) {
			this.spectatorGui.renderTooltip(scaledresolution, f);
		} else {
			this.renderHotbar(scaledresolution, f);
		}

		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(ICONS);
		GlStateManager.enableBlend();
		this.renderAttackIndicator(f, scaledresolution);
		GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE,
				GlStateManager.DestFactor.ZERO);
		this.mc.mcProfiler.startSection("bossHealth");
		this.overlayBoss.renderBossHealth();
		this.mc.mcProfiler.endSection();
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(ICONS);
		if (this.mc.playerController.shouldDrawHUD()) {
			this.renderPlayerStats(scaledresolution);
		}

		this.renderMountHealth(scaledresolution);
		GlStateManager.disableBlend();
		if (this.mc.player.getSleepTimer() > 0) {
			this.mc.mcProfiler.startSection("sleep");
			GlStateManager.disableDepth();
			GlStateManager.disableAlpha();
			int j1 = this.mc.player.getSleepTimer();
			float f2 = j1 / 100.0F;
			if (f2 > 1.0F) {
				f2 = 1.0F - (j1 - 100) / 10.0F;
			}

			int k = (int) (220.0F * f2) << 24 | 1052704;
			drawRect(0, 0, i, j, k);
			GlStateManager.enableAlpha();
			GlStateManager.enableDepth();
			this.mc.mcProfiler.endSection();
		}

		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		int k1 = i / 2 - 91;
		if (this.mc.player.isRidingHorse()) {
			this.renderHorseJumpBar(scaledresolution, k1);
		} else if (this.mc.playerController.gameIsSurvivalOrAdventure()) {
			this.renderExpBar(scaledresolution, k1);
		}

		if (this.mc.gameSettings.heldItemTooltips && !this.mc.playerController.isSpectator()) {
			this.renderSelectedItem(scaledresolution);
		} else if (this.mc.player.isSpectator()) {
			this.spectatorGui.renderSelectedItem(scaledresolution);
		}

		this.renderPotionEffects(scaledresolution);
		if (this.mc.gameSettings.showDebugInfo) {
			this.overlayDebug.renderDebugInfo(scaledresolution);
		}

		if (this.overlayMessageTime > 0) {
			this.mc.mcProfiler.startSection("overlayMessage");
			float f3 = this.overlayMessageTime - f;
			int l1 = (int) (f3 * 255.0F / 20.0F);
			if (l1 > 255) {
				l1 = 255;
			}

			if (l1 > 8) {
				GlStateManager.pushMatrix();
				GlStateManager.translate(i / 2, j - 68, 0.0F);
				GlStateManager.enableBlend();
				GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE,
						GlStateManager.DestFactor.ZERO);
				int l = 16777215;
				if (this.animateOverlayMessageColor) {
					l = MathHelper.hsvToRGB(f3 / 50.0F, 0.7F, 0.6F) & 16777215;
				}

				fontrenderer.drawString(this.overlayMessage, -fontrenderer.getStringWidth(this.overlayMessage) / 2, -4, l + (l1 << 24 & -16777216));
				GlStateManager.disableBlend();
				GlStateManager.popMatrix();
			}

			this.mc.mcProfiler.endSection();
		}

		this.overlaySubtitle.renderSubtitles(scaledresolution);
		if (this.titlesTimer > 0) {
			this.mc.mcProfiler.startSection("titleAndSubtitle");
			float f4 = this.titlesTimer - f;
			int i2 = 255;
			if (this.titlesTimer > this.titleFadeOut + this.titleDisplayTime) {
				float f5 = this.titleFadeIn + this.titleDisplayTime + this.titleFadeOut - f4;
				i2 = (int) (f5 * 255.0F / this.titleFadeIn);
			}

			if (this.titlesTimer <= this.titleFadeOut) {
				i2 = (int) (f4 * 255.0F / this.titleFadeOut);
			}

			i2 = MathHelper.clamp(i2, 0, 255);
			if (i2 > 8) {
				GlStateManager.pushMatrix();
				GlStateManager.translate(i / 2, j / 2, 0.0F);
				GlStateManager.enableBlend();
				GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE,
						GlStateManager.DestFactor.ZERO);
				GlStateManager.pushMatrix();
				GlStateManager.scale(4.0F, 4.0F, 4.0F);
				int j2 = i2 << 24 & -16777216;
				fontrenderer.drawString(this.displayedTitle, -fontrenderer.getStringWidth(this.displayedTitle) / 2, -10.0F, 16777215 | j2, true);
				GlStateManager.popMatrix();
				GlStateManager.pushMatrix();
				GlStateManager.scale(2.0F, 2.0F, 2.0F);
				fontrenderer.drawString(this.displayedSubTitle, -fontrenderer.getStringWidth(this.displayedSubTitle) / 2, 5.0F, 16777215 | j2, true);
				GlStateManager.popMatrix();
				GlStateManager.disableBlend();
				GlStateManager.popMatrix();
			}

			this.mc.mcProfiler.endSection();
		}

		Scoreboard scoreboard = this.mc.world.getScoreboard();
		ScoreObjective scoreobjective = null;
		ScorePlayerTeam scoreplayerteam = scoreboard.getPlayersTeam(this.mc.player.getName());
		if (scoreplayerteam != null) {
			int i1 = scoreplayerteam.getChatFormat().getColorIndex();
			if (i1 >= 0) {
				scoreobjective = scoreboard.getObjectiveInDisplaySlot(3 + i1);
			}
		}

		ScoreObjective scoreobjective1 = scoreobjective != null ? scoreobjective : scoreboard.getObjectiveInDisplaySlot(1);
		if (scoreobjective1 != null) {
			this.renderScoreboard(scoreobjective1, scaledresolution);
		}

		GlStateManager.enableBlend();
		GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE,
				GlStateManager.DestFactor.ZERO);
		GlStateManager.disableAlpha();
		GlStateManager.pushMatrix();
		GlStateManager.translate(0.0F, j - 48, 0.0F);
		this.mc.mcProfiler.startSection("chat");
		this.persistantChatGUI.drawChat(this.updateCounter);
		this.mc.mcProfiler.endSection();
		GlStateManager.popMatrix();
		scoreobjective1 = scoreboard.getObjectiveInDisplaySlot(0);
		if (this.mc.gameSettings.keyBindPlayerList.isKeyDown()
				&& (!this.mc.isIntegratedServerRunning() || this.mc.player.connection.getPlayerInfoMap().size() > 1 || scoreobjective1 != null)) {
			this.overlayPlayerList.updatePlayerList(true);
			this.overlayPlayerList.renderPlayerlist(i, scoreboard, scoreobjective1);
		} else {
			this.overlayPlayerList.updatePlayerList(false);
		}

		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		GlStateManager.disableLighting();
		GlStateManager.enableAlpha();
	}

	private void renderAttackIndicator(float f, ScaledResolution scaledresolution) {
		GameSettings gamesettings = this.mc.gameSettings;
		if (gamesettings.thirdPersonView == 0) {
			if (this.mc.playerController.isSpectator() && this.mc.pointedEntity == null) {
				RayTraceResult raytraceresult = this.mc.objectMouseOver;
				if (raytraceresult == null || raytraceresult.typeOfHit != RayTraceResult.Type.BLOCK) {
					return;
				}

				BlockPos blockpos = raytraceresult.getBlockPos();
				IBlockState iblockstate = this.mc.world.getBlockState(blockpos);
				if (!ReflectorForge.blockHasTileEntity(iblockstate) || !(this.mc.world.getTileEntity(blockpos) instanceof IInventory)) {
					return;
				}
			}

			int l = scaledresolution.getScaledWidth();
			int i1 = scaledresolution.getScaledHeight();
			if (gamesettings.showDebugInfo && !gamesettings.hideGUI && !this.mc.player.hasReducedDebug() && !gamesettings.reducedDebugInfo) {
				GlStateManager.pushMatrix();
				GlStateManager.translate(l / 2, i1 / 2, this.zLevel);
				Entity entity = this.mc.getRenderViewEntity();
				GlStateManager.rotate(entity.prevRotationPitch + (entity.rotationPitch - entity.prevRotationPitch) * f, -1.0F, 0.0F, 0.0F);
				GlStateManager.rotate(entity.prevRotationYaw + (entity.rotationYaw - entity.prevRotationYaw) * f, 0.0F, 1.0F, 0.0F);
				GlStateManager.scale(-1.0F, -1.0F, -1.0F);
				OpenGlHelper.renderDirections(10);
				GlStateManager.popMatrix();
			} else {
				GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.ONE_MINUS_DST_COLOR, GlStateManager.DestFactor.ONE_MINUS_SRC_COLOR, GlStateManager.SourceFactor.ONE,
						GlStateManager.DestFactor.ZERO);
				GlStateManager.enableAlpha();
				this.drawTexturedModalRect(l / 2 - 7, i1 / 2 - 7, 0, 0, 16, 16);
				if (this.mc.gameSettings.attackIndicator == 1) {
					float f1 = this.mc.player.getCooledAttackStrength(0.0F);
					boolean flag = false;
					if (this.mc.pointedEntity != null && this.mc.pointedEntity instanceof EntityLivingBase && f1 >= 1.0F) {
						flag = this.mc.player.getCooldownPeriod() > 5.0F;
						flag = flag & ((EntityLivingBase) this.mc.pointedEntity).isEntityAlive();
					}

					int i = i1 / 2 - 7 + 16;
					int j = l / 2 - 8;
					if (flag) {
						this.drawTexturedModalRect(j, i, 68, 94, 16, 16);
					} else if (f1 < 1.0F) {
						int k = (int) (f1 * 17.0F);
						this.drawTexturedModalRect(j, i, 36, 94, 16, 4);
						this.drawTexturedModalRect(j, i, 52, 94, k, 4);
					}
				}
			}
		}

	}

	protected void renderPotionEffects(ScaledResolution scaledresolution) {
		Collection collection = this.mc.player.getActivePotionEffects();
		if (!collection.isEmpty()) {
			this.mc.getTextureManager().bindTexture(GuiContainer.INVENTORY_BACKGROUND);
			GlStateManager.enableBlend();
			int i = 0;
			int j = 0;
			Iterator iterator = Ordering.natural().reverse().sortedCopy(collection).iterator();

			while (true) {
				PotionEffect potioneffect;
				Potion potion;
				boolean flag;
				while (true) {
					if (!iterator.hasNext()) {
						return;
					}

					potioneffect = (PotionEffect) iterator.next();
					potion = potioneffect.getPotion();
					flag = potion.hasStatusIcon();
					if (!Reflector.ForgePotion_shouldRenderHUD.exists()) {
						break;
					}

					if (Reflector.callBoolean(potion, Reflector.ForgePotion_shouldRenderHUD, new Object[] { potioneffect })) {
						this.mc.getTextureManager().bindTexture(GuiContainer.INVENTORY_BACKGROUND);
						flag = true;
						break;
					}
				}

				if (flag && potioneffect.doesShowParticles()) {
					int k = scaledresolution.getScaledWidth();
					int l = 1;

					int i1 = potion.getStatusIconIndex();
					if (potion.isBeneficial()) {
						++i;
						k = k - 25 * i;
					} else {
						++j;
						k = k - 25 * j;
						l += 26;
					}

					GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
					float f = 1.0F;
					if (potioneffect.getIsAmbient()) {
						this.drawTexturedModalRect(k, l, 165, 166, 24, 24);
					} else {
						this.drawTexturedModalRect(k, l, 141, 166, 24, 24);
						if (potioneffect.getDuration() <= 200) {
							int j1 = 10 - potioneffect.getDuration() / 20;
							f = MathHelper.clamp(potioneffect.getDuration() / 10.0F / 5.0F * 0.5F, 0.0F, 0.5F)
									+ MathHelper.cos(potioneffect.getDuration() * 3.1415927F / 5.0F) * MathHelper.clamp(j1 / 10.0F * 0.25F, 0.0F, 0.25F);
						}
					}

					GlStateManager.color(1.0F, 1.0F, 1.0F, f);
					if (Reflector.ForgePotion_renderHUDEffect.exists()) {
						if (potion.hasStatusIcon()) {
							this.drawTexturedModalRect(k + 3, l + 3, i1 % 8 * 18, 198 + i1 / 8 * 18, 18, 18);
						}

						Reflector.call(potion, Reflector.ForgePotion_renderHUDEffect,
								new Object[] { Integer.valueOf(k), Integer.valueOf(l), potioneffect, this.mc, Float.valueOf(f) });
					} else {
						this.drawTexturedModalRect(k + 3, l + 3, i1 % 8 * 18, 198 + i1 / 8 * 18, 18, 18);
					}
				}
			}
		}
	}

	protected void renderHotbar(ScaledResolution scaledresolution, float f) {
		if (this.mc.getRenderViewEntity() instanceof EntityPlayer) {
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
			this.mc.getTextureManager().bindTexture(WIDGETS_TEX_PATH);
			EntityPlayer entityplayer = (EntityPlayer) this.mc.getRenderViewEntity();
			ItemStack itemstack = entityplayer.getHeldItemOffhand();
			EnumHandSide enumhandside = entityplayer.getPrimaryHand().opposite();
			int i = scaledresolution.getScaledWidth() / 2;
			float f1 = this.zLevel;
			boolean flag = true;
			boolean flag1 = true;
			this.zLevel = -90.0F;
			this.drawTexturedModalRect(i - 91, scaledresolution.getScaledHeight() - 22, 0, 0, 182, 22);
			this.drawTexturedModalRect(i - 91 - 1 + entityplayer.inventory.currentItem * 20, scaledresolution.getScaledHeight() - 22 - 1, 0, 22, 24, 22);
			if (!itemstack.isEmpty()) {
				if (enumhandside == EnumHandSide.LEFT) {
					this.drawTexturedModalRect(i - 91 - 29, scaledresolution.getScaledHeight() - 23, 24, 22, 29, 24);
				} else {
					this.drawTexturedModalRect(i + 91, scaledresolution.getScaledHeight() - 23, 53, 22, 29, 24);
				}
			}

			this.zLevel = f1;
			GlStateManager.enableRescaleNormal();
			GlStateManager.enableBlend();
			GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE,
					GlStateManager.DestFactor.ZERO);
			RenderHelper.enableGUIStandardItemLighting();

			for (int j = 0; j < 9; ++j) {
				int k = i - 90 + j * 20 + 2;
				int l = scaledresolution.getScaledHeight() - 16 - 3;
				this.renderHotbarItem(k, l, f, entityplayer, entityplayer.inventory.mainInventory.get(j));
			}

			if (!itemstack.isEmpty()) {
				int j1 = scaledresolution.getScaledHeight() - 16 - 3;
				if (enumhandside == EnumHandSide.LEFT) {
					this.renderHotbarItem(i - 91 - 26, j1, f, entityplayer, itemstack);
				} else {
					this.renderHotbarItem(i + 91 + 10, j1, f, entityplayer, itemstack);
				}
			}

			if (this.mc.gameSettings.attackIndicator == 2) {
				float f2 = this.mc.player.getCooledAttackStrength(0.0F);
				if (f2 < 1.0F) {
					int k1 = scaledresolution.getScaledHeight() - 20;
					int l1 = i + 91 + 6;
					if (enumhandside == EnumHandSide.RIGHT) {
						l1 = i - 91 - 22;
					}

					this.mc.getTextureManager().bindTexture(Gui.ICONS);
					int i1 = (int) (f2 * 19.0F);
					GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
					this.drawTexturedModalRect(l1, k1, 0, 94, 18, 18);
					this.drawTexturedModalRect(l1, k1 + 18 - i1, 18, 112 - i1, 18, i1);
				}
			}

			RenderHelper.disableStandardItemLighting();
			GlStateManager.disableRescaleNormal();
			GlStateManager.disableBlend();
		}

	}

	public void renderHorseJumpBar(ScaledResolution scaledresolution, int i) {
		this.mc.mcProfiler.startSection("jumpBar");
		this.mc.getTextureManager().bindTexture(Gui.ICONS);
		float f = this.mc.player.getHorseJumpPower();
		boolean flag = true;
		int j = (int) (f * 183.0F);
		int k = scaledresolution.getScaledHeight() - 32 + 3;
		this.drawTexturedModalRect(i, k, 0, 84, 182, 5);
		if (j > 0) {
			this.drawTexturedModalRect(i, k, 0, 89, j, 5);
		}

		this.mc.mcProfiler.endSection();
	}

	public void renderExpBar(ScaledResolution scaledresolution, int i) {
		this.mc.mcProfiler.startSection("expBar");
		this.mc.getTextureManager().bindTexture(Gui.ICONS);
		int j = this.mc.player.xpBarCap();
		if (j > 0) {
			boolean flag = true;
			int k = (int) (this.mc.player.experience * 183.0F);
			int l = scaledresolution.getScaledHeight() - 32 + 3;
			this.drawTexturedModalRect(i, l, 0, 64, 182, 5);
			if (k > 0) {
				this.drawTexturedModalRect(i, l, 0, 69, k, 5);
			}
		}

		this.mc.mcProfiler.endSection();
		if (this.mc.player.experienceLevel > 0) {
			this.mc.mcProfiler.startSection("expLevel");
			int j1 = 8453920;
			if (Config.isCustomColors()) {
				j1 = CustomColors.getExpBarTextColor(j1);
			}

			String s = "" + this.mc.player.experienceLevel;
			int k1 = (scaledresolution.getScaledWidth() - this.getFontRenderer().getStringWidth(s)) / 2;
			int i1 = scaledresolution.getScaledHeight() - 31 - 4;
			this.getFontRenderer().drawString(s, k1 + 1, i1, 0);
			this.getFontRenderer().drawString(s, k1 - 1, i1, 0);
			this.getFontRenderer().drawString(s, k1, i1 + 1, 0);
			this.getFontRenderer().drawString(s, k1, i1 - 1, 0);
			this.getFontRenderer().drawString(s, k1, i1, j1);
			this.mc.mcProfiler.endSection();
		}

	}

	public void renderSelectedItem(ScaledResolution scaledresolution) {
		this.mc.mcProfiler.startSection("selectedItemName");
		if (this.remainingHighlightTicks > 0 && !this.highlightingItemStack.isEmpty()) {
			String s = this.highlightingItemStack.getDisplayName();
			if (this.highlightingItemStack.hasDisplayName()) {
				s = TextFormatting.ITALIC + s;
			}

			int i = (scaledresolution.getScaledWidth() - this.getFontRenderer().getStringWidth(s)) / 2;
			int j = scaledresolution.getScaledHeight() - 59;
			if (!this.mc.playerController.shouldDrawHUD()) {
				j += 14;
			}

			int k = (int) (this.remainingHighlightTicks * 256.0F / 10.0F);
			if (k > 255) {
				k = 255;
			}

			if (k > 0) {
				GlStateManager.pushMatrix();
				GlStateManager.enableBlend();
				GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE,
						GlStateManager.DestFactor.ZERO);
				this.getFontRenderer().drawStringWithShadow(s, i, j, 16777215 + (k << 24));
				GlStateManager.disableBlend();
				GlStateManager.popMatrix();
			}
		}

		this.mc.mcProfiler.endSection();
	}

	private void renderScoreboard(ScoreObjective scoreobjective, ScaledResolution scaledresolution) {
		Scoreboard scoreboard = scoreobjective.getScoreboard();
		Collection collection = scoreboard.getSortedScores(scoreobjective);
		ArrayList arraylist = Lists.newArrayList(Iterables.filter(collection, new Predicate<Score>() {
			@Override
			public boolean apply(@Nullable Score score2) {
				return score2.getPlayerName() != null && !score2.getPlayerName().startsWith("#");
			}
		}));
		ArrayList<Score> arraylist1;
		if (arraylist.size() > 15) {
			arraylist1 = Lists.newArrayList(Iterables.skip(arraylist, collection.size() - 15));
		} else {
			arraylist1 = arraylist;
		}

		int i = this.getFontRenderer().getStringWidth(scoreobjective.getDisplayName());

		for (Score score : arraylist1) {
			ScorePlayerTeam scoreplayerteam = scoreboard.getPlayersTeam(score.getPlayerName());
			String s = ScorePlayerTeam.formatPlayerName(scoreplayerteam, score.getPlayerName()) + ": " + TextFormatting.RED + score.getScorePoints();
			i = Math.max(i, this.getFontRenderer().getStringWidth(s));
		}

		int i1 = arraylist1.size() * this.getFontRenderer().FONT_HEIGHT;
		int j1 = scaledresolution.getScaledHeight() / 2 + i1 / 3;
		boolean flag = true;
		int k1 = scaledresolution.getScaledWidth() - i - 3;
		int j = 0;

		for (Score score1 : arraylist1) {
			++j;
			ScorePlayerTeam scoreplayerteam1 = scoreboard.getPlayersTeam(score1.getPlayerName());
			String s1 = ScorePlayerTeam.formatPlayerName(scoreplayerteam1, score1.getPlayerName());
			String s2 = TextFormatting.RED + "" + score1.getScorePoints();
			int k = j1 - j * this.getFontRenderer().FONT_HEIGHT;
			int l = scaledresolution.getScaledWidth() - 3 + 2;
			drawRect(k1 - 2, k, l, k + this.getFontRenderer().FONT_HEIGHT, 1342177280);
			this.getFontRenderer().drawString(s1, k1, k, 553648127);
			this.getFontRenderer().drawString(s2, l - this.getFontRenderer().getStringWidth(s2), k, 553648127);
			if (j == arraylist1.size()) {
				String s3 = scoreobjective.getDisplayName();
				drawRect(k1 - 2, k - this.getFontRenderer().FONT_HEIGHT - 1, l, k - 1, 1610612736);
				drawRect(k1 - 2, k - 1, l, k, 1342177280);
				this.getFontRenderer().drawString(s3, k1 + i / 2 - this.getFontRenderer().getStringWidth(s3) / 2, k - this.getFontRenderer().FONT_HEIGHT, 553648127);
			}
		}

	}

	private void renderPlayerStats(ScaledResolution scaledresolution) {
		if (this.mc.getRenderViewEntity() instanceof EntityPlayer) {
			EntityPlayer entityplayer = (EntityPlayer) this.mc.getRenderViewEntity();
			int i = MathHelper.ceil(entityplayer.getHealth());
			boolean flag = this.healthUpdateCounter > this.updateCounter && (this.healthUpdateCounter - this.updateCounter) / 3L % 2L == 1L;
			if (i < this.playerHealth && entityplayer.hurtResistantTime > 0) {
				this.lastSystemTime = Minecraft.getSystemTime();
				this.healthUpdateCounter = this.updateCounter + 20;
			} else if (i > this.playerHealth && entityplayer.hurtResistantTime > 0) {
				this.lastSystemTime = Minecraft.getSystemTime();
				this.healthUpdateCounter = this.updateCounter + 10;
			}

			if (Minecraft.getSystemTime() - this.lastSystemTime > 1000L) {
				this.playerHealth = i;
				this.lastPlayerHealth = i;
				this.lastSystemTime = Minecraft.getSystemTime();
			}

			this.playerHealth = i;
			int j = this.lastPlayerHealth;
			this.rand.setSeed(this.updateCounter * 312871);
			FoodStats foodstats = entityplayer.getFoodStats();
			int k = foodstats.getFoodLevel();
			IAttributeInstance iattributeinstance = entityplayer.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH);
			int l = scaledresolution.getScaledWidth() / 2 - 91;
			int i1 = scaledresolution.getScaledWidth() / 2 + 91;
			int j1 = scaledresolution.getScaledHeight() - 39;
			float f = (float) iattributeinstance.getAttributeValue();
			int k1 = MathHelper.ceil(entityplayer.getAbsorptionAmount());
			int l1 = MathHelper.ceil((f + k1) / 2.0F / 10.0F);
			int i2 = Math.max(10 - (l1 - 2), 3);
			int j2 = j1 - (l1 - 1) * i2 - 10;
			int k2 = j1 - 10;
			int l2 = k1;
			int i3 = entityplayer.getTotalArmorValue();
			int j3 = -1;
			if (entityplayer.isPotionActive(MobEffects.REGENERATION)) {
				j3 = this.updateCounter % MathHelper.ceil(f + 5.0F);
			}

			this.mc.mcProfiler.startSection("armor");

			for (int k3 = 0; k3 < 10; ++k3) {
				if (i3 > 0) {
					int l3 = l + k3 * 8;
					if (k3 * 2 + 1 < i3) {
						this.drawTexturedModalRect(l3, j2, 34, 9, 9, 9);
					}

					if (k3 * 2 + 1 == i3) {
						this.drawTexturedModalRect(l3, j2, 25, 9, 9, 9);
					}

					if (k3 * 2 + 1 > i3) {
						this.drawTexturedModalRect(l3, j2, 16, 9, 9, 9);
					}
				}
			}

			this.mc.mcProfiler.endStartSection("health");

			for (int l4 = MathHelper.ceil((f + k1) / 2.0F) - 1; l4 >= 0; --l4) {
				int i5 = 16;
				if (entityplayer.isPotionActive(MobEffects.POISON)) {
					i5 += 36;
				} else if (entityplayer.isPotionActive(MobEffects.WITHER)) {
					i5 += 72;
				}

				byte b0 = 0;
				if (flag) {
					b0 = 1;
				}

				int i4 = MathHelper.ceil((l4 + 1) / 10.0F) - 1;
				int j4 = l + l4 % 10 * 8;
				int k4 = j1 - i4 * i2;
				if (i <= 4) {
					k4 += this.rand.nextInt(2);
				}

				if (l2 <= 0 && l4 == j3) {
					k4 -= 2;
				}

				byte b1 = 0;
				if (entityplayer.world.getWorldInfo().isHardcoreModeEnabled()) {
					b1 = 5;
				}

				this.drawTexturedModalRect(j4, k4, 16 + b0 * 9, 9 * b1, 9, 9);
				if (flag) {
					if (l4 * 2 + 1 < j) {
						this.drawTexturedModalRect(j4, k4, i5 + 54, 9 * b1, 9, 9);
					}

					if (l4 * 2 + 1 == j) {
						this.drawTexturedModalRect(j4, k4, i5 + 63, 9 * b1, 9, 9);
					}
				}

				if (l2 > 0) {
					if (l2 == k1 && k1 % 2 == 1) {
						this.drawTexturedModalRect(j4, k4, i5 + 153, 9 * b1, 9, 9);
						--l2;
					} else {
						this.drawTexturedModalRect(j4, k4, i5 + 144, 9 * b1, 9, 9);
						l2 -= 2;
					}
				} else {
					if (l4 * 2 + 1 < i) {
						this.drawTexturedModalRect(j4, k4, i5 + 36, 9 * b1, 9, 9);
					}

					if (l4 * 2 + 1 == i) {
						this.drawTexturedModalRect(j4, k4, i5 + 45, 9 * b1, 9, 9);
					}
				}
			}

			Entity entity = entityplayer.getRidingEntity();
			if (entity == null || !(entity instanceof EntityLivingBase)) {
				this.mc.mcProfiler.endStartSection("food");

				for (int j5 = 0; j5 < 10; ++j5) {
					int l5 = j1;
					int j6 = 16;
					byte b2 = 0;
					if (entityplayer.isPotionActive(MobEffects.HUNGER)) {
						j6 += 36;
						b2 = 13;
					}

					if (entityplayer.getFoodStats().getSaturationLevel() <= 0.0F && this.updateCounter % (k * 3 + 1) == 0) {
						l5 = j1 + (this.rand.nextInt(3) - 1);
					}

					int i7 = i1 - j5 * 8 - 9;
					this.drawTexturedModalRect(i7, l5, 16 + b2 * 9, 27, 9, 9);
					if (j5 * 2 + 1 < k) {
						this.drawTexturedModalRect(i7, l5, j6 + 36, 27, 9, 9);
					}

					if (j5 * 2 + 1 == k) {
						this.drawTexturedModalRect(i7, l5, j6 + 45, 27, 9, 9);
					}
				}
			}

			this.mc.mcProfiler.endStartSection("air");
			if (entityplayer.isInsideOfMaterial(Material.WATER)) {
				int k5 = this.mc.player.getAir();
				int i6 = MathHelper.ceil((k5 - 2) * 10.0D / 300.0D);
				int k6 = MathHelper.ceil(k5 * 10.0D / 300.0D) - i6;

				for (int l6 = 0; l6 < i6 + k6; ++l6) {
					if (l6 < i6) {
						this.drawTexturedModalRect(i1 - l6 * 8 - 9, k2, 16, 18, 9, 9);
					} else {
						this.drawTexturedModalRect(i1 - l6 * 8 - 9, k2, 25, 18, 9, 9);
					}
				}
			}

			this.mc.mcProfiler.endSection();
		}

	}

	private void renderMountHealth(ScaledResolution scaledresolution) {
		if (this.mc.getRenderViewEntity() instanceof EntityPlayer) {
			EntityPlayer entityplayer = (EntityPlayer) this.mc.getRenderViewEntity();
			Entity entity = entityplayer.getRidingEntity();
			if (entity instanceof EntityLivingBase) {
				this.mc.mcProfiler.endStartSection("mountHealth");
				EntityLivingBase entitylivingbase = (EntityLivingBase) entity;
				int i = (int) Math.ceil(entitylivingbase.getHealth());
				float f = entitylivingbase.getMaxHealth();
				int j = (int) (f + 0.5F) / 2;
				if (j > 30) {
					j = 30;
				}

				int k = scaledresolution.getScaledHeight() - 39;
				int l = scaledresolution.getScaledWidth() / 2 + 91;
				int i1 = k;
				int j1 = 0;

				for (boolean flag = false; j > 0; j1 += 20) {
					int k1 = Math.min(j, 10);
					j -= k1;

					for (int l1 = 0; l1 < k1; ++l1) {
						boolean flag1 = true;
						byte b0 = 0;
						int i2 = l - l1 * 8 - 9;
						this.drawTexturedModalRect(i2, i1, 52 + b0 * 9, 9, 9, 9);
						if (l1 * 2 + 1 + j1 < i) {
							this.drawTexturedModalRect(i2, i1, 88, 9, 9, 9);
						}

						if (l1 * 2 + 1 + j1 == i) {
							this.drawTexturedModalRect(i2, i1, 97, 9, 9, 9);
						}
					}

					i1 -= 10;
				}
			}
		}

	}

	private void renderPumpkinOverlay(ScaledResolution scaledresolution) {
		GlStateManager.disableDepth();
		GlStateManager.depthMask(false);
		GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE,
				GlStateManager.DestFactor.ZERO);
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		GlStateManager.disableAlpha();
		this.mc.getTextureManager().bindTexture(PUMPKIN_BLUR_TEX_PATH);
		Tessellator tessellator = Tessellator.getInstance();
		VertexBuffer vertexbuffer = tessellator.getBuffer();
		vertexbuffer.begin(7, DefaultVertexFormats.POSITION_TEX);
		vertexbuffer.pos(0.0D, scaledresolution.getScaledHeight(), -90.0D).tex(0.0D, 1.0D).endVertex();
		vertexbuffer.pos(scaledresolution.getScaledWidth(), scaledresolution.getScaledHeight(), -90.0D).tex(1.0D, 1.0D).endVertex();
		vertexbuffer.pos(scaledresolution.getScaledWidth(), 0.0D, -90.0D).tex(1.0D, 0.0D).endVertex();
		vertexbuffer.pos(0.0D, 0.0D, -90.0D).tex(0.0D, 0.0D).endVertex();
		tessellator.draw();
		GlStateManager.depthMask(true);
		GlStateManager.enableDepth();
		GlStateManager.enableAlpha();
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
	}

	private void renderVignette(float f, ScaledResolution scaledresolution) {
		if (Config.isVignetteEnabled()) {
			f = 1.0F - f;
			f = MathHelper.clamp(f, 0.0F, 1.0F);
			WorldBorder worldborder = this.mc.world.getWorldBorder();
			float f1 = (float) worldborder.getClosestDistance(this.mc.player);
			double d0 = Math.min(worldborder.getResizeSpeed() * worldborder.getWarningTime() * 1000.0D, Math.abs(worldborder.getTargetSize() - worldborder.getDiameter()));
			double d1 = Math.max(worldborder.getWarningDistance(), d0);
			if (f1 < d1) {
				f1 = 1.0F - (float) (f1 / d1);
			} else {
				f1 = 0.0F;
			}

			this.prevVignetteBrightness = (float) (this.prevVignetteBrightness + (f - this.prevVignetteBrightness) * 0.01D);
			GlStateManager.disableDepth();
			GlStateManager.depthMask(false);
			GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.ZERO, GlStateManager.DestFactor.ONE_MINUS_SRC_COLOR, GlStateManager.SourceFactor.ONE,
					GlStateManager.DestFactor.ZERO);
			if (f1 > 0.0F) {
				GlStateManager.color(0.0F, f1, f1, 1.0F);
			} else {
				GlStateManager.color(this.prevVignetteBrightness, this.prevVignetteBrightness, this.prevVignetteBrightness, 1.0F);
			}

			this.mc.getTextureManager().bindTexture(VIGNETTE_TEX_PATH);
			Tessellator tessellator = Tessellator.getInstance();
			VertexBuffer vertexbuffer = tessellator.getBuffer();
			vertexbuffer.begin(7, DefaultVertexFormats.POSITION_TEX);
			vertexbuffer.pos(0.0D, scaledresolution.getScaledHeight(), -90.0D).tex(0.0D, 1.0D).endVertex();
			vertexbuffer.pos(scaledresolution.getScaledWidth(), scaledresolution.getScaledHeight(), -90.0D).tex(1.0D, 1.0D).endVertex();
			vertexbuffer.pos(scaledresolution.getScaledWidth(), 0.0D, -90.0D).tex(1.0D, 0.0D).endVertex();
			vertexbuffer.pos(0.0D, 0.0D, -90.0D).tex(0.0D, 0.0D).endVertex();
			tessellator.draw();
			GlStateManager.depthMask(true);
			GlStateManager.enableDepth();
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
			GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE,
					GlStateManager.DestFactor.ZERO);
		}
	}

	private void renderPortal(float f, ScaledResolution scaledresolution) {
		if (f < 1.0F) {
			f = f * f;
			f = f * f;
			f = f * 0.8F + 0.2F;
		}

		GlStateManager.disableAlpha();
		GlStateManager.disableDepth();
		GlStateManager.depthMask(false);
		GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE,
				GlStateManager.DestFactor.ZERO);
		GlStateManager.color(1.0F, 1.0F, 1.0F, f);
		this.mc.getTextureManager().bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
		TextureAtlasSprite textureatlassprite = this.mc.getBlockRendererDispatcher().getBlockModelShapes().getTexture(Blocks.PORTAL.getDefaultState());
		float f1 = textureatlassprite.getMinU();
		float f2 = textureatlassprite.getMinV();
		float f3 = textureatlassprite.getMaxU();
		float f4 = textureatlassprite.getMaxV();
		Tessellator tessellator = Tessellator.getInstance();
		VertexBuffer vertexbuffer = tessellator.getBuffer();
		vertexbuffer.begin(7, DefaultVertexFormats.POSITION_TEX);
		vertexbuffer.pos(0.0D, scaledresolution.getScaledHeight(), -90.0D).tex(f1, f4).endVertex();
		vertexbuffer.pos(scaledresolution.getScaledWidth(), scaledresolution.getScaledHeight(), -90.0D).tex(f3, f4).endVertex();
		vertexbuffer.pos(scaledresolution.getScaledWidth(), 0.0D, -90.0D).tex(f3, f2).endVertex();
		vertexbuffer.pos(0.0D, 0.0D, -90.0D).tex(f1, f2).endVertex();
		tessellator.draw();
		GlStateManager.depthMask(true);
		GlStateManager.enableDepth();
		GlStateManager.enableAlpha();
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
	}

	private void renderHotbarItem(int i, int j, float f, EntityPlayer entityplayer, ItemStack itemstack) {
		if (!itemstack.isEmpty()) {
			float f1 = itemstack.getAnimationsToGo() - f;
			if (f1 > 0.0F) {
				GlStateManager.pushMatrix();
				float f2 = 1.0F + f1 / 5.0F;
				GlStateManager.translate(i + 8, j + 12, 0.0F);
				GlStateManager.scale(1.0F / f2, (f2 + 1.0F) / 2.0F, 1.0F);
				GlStateManager.translate((-(i + 8)), (-(j + 12)), 0.0F);
			}

			this.itemRenderer.renderItemAndEffectIntoGUI(entityplayer, itemstack, i, j);
			if (f1 > 0.0F) {
				GlStateManager.popMatrix();
			}

			this.itemRenderer.renderItemOverlays(this.mc.fontRendererObj, itemstack, i, j);
		}

	}

	public void updateTick() {
		if (this.overlayMessageTime > 0) {
			--this.overlayMessageTime;
		}

		if (this.titlesTimer > 0) {
			--this.titlesTimer;
			if (this.titlesTimer <= 0) {
				this.displayedTitle = "";
				this.displayedSubTitle = "";
			}
		}

		++this.updateCounter;
		if (this.mc.player != null) {
			ItemStack itemstack = this.mc.player.inventory.getCurrentItem();
			if (itemstack.isEmpty()) {
				this.remainingHighlightTicks = 0;
			} else if (!this.highlightingItemStack.isEmpty() && itemstack.getItem() == this.highlightingItemStack.getItem()
					&& ItemStack.areItemStackTagsEqual(itemstack, this.highlightingItemStack)
					&& (itemstack.isItemStackDamageable() || itemstack.getMetadata() == this.highlightingItemStack.getMetadata())) {
				if (this.remainingHighlightTicks > 0) {
					--this.remainingHighlightTicks;
				}
			} else {
				this.remainingHighlightTicks = 40;
			}

			this.highlightingItemStack = itemstack;
		}

	}

	public void setRecordPlayingMessage(String s) {
		this.setOverlayMessage(I18n.format("record.nowPlaying", new Object[] { s }), true);
	}

	public void setOverlayMessage(String s, boolean flag) {
		this.overlayMessage = s;
		this.overlayMessageTime = 60;
		this.animateOverlayMessageColor = flag;
	}

	public void displayTitle(String s, String s1, int i, int j, int k) {
		if (s == null && s1 == null && i < 0 && j < 0 && k < 0) {
			this.displayedTitle = "";
			this.displayedSubTitle = "";
			this.titlesTimer = 0;
		} else if (s != null) {
			this.displayedTitle = s;
			this.titlesTimer = this.titleFadeIn + this.titleDisplayTime + this.titleFadeOut;
		} else if (s1 != null) {
			this.displayedSubTitle = s1;
		} else {
			if (i >= 0) {
				this.titleFadeIn = i;
			}

			if (j >= 0) {
				this.titleDisplayTime = j;
			}

			if (k >= 0) {
				this.titleFadeOut = k;
			}

			if (this.titlesTimer > 0) {
				this.titlesTimer = this.titleFadeIn + this.titleDisplayTime + this.titleFadeOut;
			}
		}

	}

	public void setOverlayMessage(ITextComponent itextcomponent, boolean flag) {
		this.setOverlayMessage(itextcomponent.getUnformattedText(), flag);
	}

	public GuiNewChat getChatGUI() {
		return this.persistantChatGUI;
	}

	public int getUpdateCounter() {
		return this.updateCounter;
	}

	public FontRenderer getFontRenderer() {
		return this.mc.fontRendererObj;
	}

	public GuiSpectator getSpectatorGui() {
		return this.spectatorGui;
	}

	public GuiPlayerTabOverlay getTabList() {
		return this.overlayPlayerList;
	}

	public void resetPlayersOverlayFooterHeader() {
		this.overlayPlayerList.resetFooterHeader();
		this.overlayBoss.clearBossInfos();
	}

	public GuiBossOverlay getBossOverlay() {
		return this.overlayBoss;
	}
}
