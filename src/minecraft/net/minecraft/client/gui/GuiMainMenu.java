package net.minecraft.client.gui;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiButtonLanguage;
import net.minecraft.client.gui.GuiLanguage;
import net.minecraft.client.gui.GuiOptions;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ingame.GuiConfirmOpenLink;
import net.minecraft.client.gui.multiplayer.GuiMultiplayer;
import net.minecraft.client.gui.singleplayer.GuiWorldSelection;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.resources.IResource;
import net.optifine.CustomPanorama;
import net.optifine.CustomPanoramaProperties;
import net.optifine.Reflector;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StringUtils;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextFormatting;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.util.glu.Project;

public class GuiMainMenu extends GuiScreen {
	private static final Logger LOGGER = LogManager.getLogger();
	private static final Random RANDOM = new Random();
	private final float updateCounter;
	private String splashText;
	private int panoramaTimer;
	private DynamicTexture viewportTexture;
	private final Object threadLock = new Object();
	public static final String MORE_INFO_TEXT = "Please click " + TextFormatting.UNDERLINE + "here" + TextFormatting.RESET + " for more information.";
	private int openGLWarning2Width;
	private int openGLWarning1Width;
	private int openGLWarningX1;
	private int openGLWarningY1;
	private int openGLWarningX2;
	private int openGLWarningY2;
	private String openGLWarning1;
	private String openGLWarning2;
	private String openGLWarningLink;
	private static final ResourceLocation SPLASH_TEXTS = new ResourceLocation("texts/splashes.txt");
	private static final ResourceLocation MINECRAFT_TITLE_TEXTURES = new ResourceLocation("textures/gui/title/minecraft.png");
	private static final ResourceLocation[] TITLE_PANORAMA_PATHS = new ResourceLocation[] { new ResourceLocation("textures/gui/title/background/panorama_0.png"),
			new ResourceLocation("textures/gui/title/background/panorama_1.png"), new ResourceLocation("textures/gui/title/background/panorama_2.png"),
			new ResourceLocation("textures/gui/title/background/panorama_3.png"), new ResourceLocation("textures/gui/title/background/panorama_4.png"),
			new ResourceLocation("textures/gui/title/background/panorama_5.png") };
	private ResourceLocation backgroundTexture;
	private GuiButton modButton;
	private GuiScreen modUpdateNotification;

	public GuiMainMenu() {
		this.openGLWarning2 = MORE_INFO_TEXT;
		this.splashText = "missingno";
		IResource iresource = null;

		try {
			ArrayList arraylist = Lists.newArrayList();
			iresource = Minecraft.getMinecraft().getResourceManager().getResource(SPLASH_TEXTS);
			BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(iresource.getInputStream(), Charsets.UTF_8));

			String s;
			while ((s = bufferedreader.readLine()) != null) {
				s = s.trim();
				if (!s.isEmpty()) {
					arraylist.add(s);
				}
			}

			if (!arraylist.isEmpty()) {
				while (true) {
					this.splashText = (String) arraylist.get(RANDOM.nextInt(arraylist.size()));
					if (this.splashText.hashCode() != 125780783) {
						break;
					}
				}
			}
		} catch (IOException var8) {
			;
		} finally {
			IOUtils.closeQuietly(iresource);
		}

		this.updateCounter = RANDOM.nextFloat();
		this.openGLWarning1 = "";
		if (!GLContext.getCapabilities().OpenGL20 && !OpenGlHelper.areShadersSupported()) {
			this.openGLWarning1 = I18n.format("title.oldgl1", new Object[0]);
			this.openGLWarning2 = I18n.format("title.oldgl2", new Object[0]);
			this.openGLWarningLink = "https://help.mojang.com/customer/portal/articles/325948?ref=game";
		}

		String s1 = System.getProperty("java.version");
		if (s1 != null && (s1.startsWith("1.6") || s1.startsWith("1.7"))) {
			this.openGLWarning1 = I18n.format("title.oldjava1", new Object[0]);
			this.openGLWarning2 = I18n.format("title.oldjava2", new Object[0]);
			this.openGLWarningLink = "https://help.mojang.com/customer/portal/articles/2636196?ref=game";
		}

	}

	@Override
	public void updateScreen() {
		++this.panoramaTimer;
	}

	@Override
	public boolean doesGuiPauseGame() {
		return false;
	}

	@Override
	protected void keyTyped(char var1, int var2) throws IOException {
	}

	@Override
	public void initGui() {
		this.viewportTexture = new DynamicTexture(256, 256);
		this.backgroundTexture = this.mc.getTextureManager().getDynamicTextureLocation("background", this.viewportTexture);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		if (calendar.get(2) + 1 == 12 && calendar.get(5) == 24) {
			this.splashText = "Merry X-mas!";
		} else if (calendar.get(2) + 1 == 1 && calendar.get(5) == 1) {
			this.splashText = "Happy new year!";
		} else if (calendar.get(2) + 1 == 10 && calendar.get(5) == 31) {
			this.splashText = "OOoooOOOoooo! Spooky!";
		}

		boolean flag = true;
		int i = this.height / 4 + 48;

		this.addSingleplayerMultiplayerButtons(i, 24);

		this.buttonList.add(new GuiButton(0, this.width / 2 - 100, i + 72 + 12, 98, 20, I18n.format("menu.options", new Object[0])));
		this.buttonList.add(new GuiButton(4, this.width / 2 + 2, i + 72 + 12, 98, 20, I18n.format("menu.quit", new Object[0])));
		this.buttonList.add(new GuiButtonLanguage(5, this.width / 2 - 124, i + 72 + 12));
		synchronized (this.threadLock) {
			this.openGLWarning1Width = this.fontRendererObj.getStringWidth(this.openGLWarning1);
			this.openGLWarning2Width = this.fontRendererObj.getStringWidth(this.openGLWarning2);
			int j = Math.max(this.openGLWarning1Width, this.openGLWarning2Width);
			this.openGLWarningX1 = (this.width - j) / 2;
			this.openGLWarningY1 = this.buttonList.get(0).yPosition - 24;
			this.openGLWarningX2 = this.openGLWarningX1 + j;
			this.openGLWarningY2 = this.openGLWarningY1 + 24;
		}

		if (Reflector.NotificationModUpdateScreen_init.exists()) {
			this.modUpdateNotification = (GuiScreen) Reflector.call(Reflector.NotificationModUpdateScreen_init, new Object[] { this, this.modButton });
		}

	}

	private void addSingleplayerMultiplayerButtons(int i, int j) {
		this.buttonList.add(new GuiButton(1, this.width / 2 - 100, i, I18n.format("menu.singleplayer", new Object[0])));
		this.buttonList.add(new GuiButton(2, this.width / 2 - 100, i + j * 1, I18n.format("menu.multiplayer", new Object[0])));
		if (Reflector.GuiModList_Constructor.exists()) {
			this.buttonList.add(this.modButton = new GuiButton(6, this.width / 2 - 100, i + j * 2, 98, 20, I18n.format("fml.menu.mods", new Object[0])));
		}

	}

	@Override
	protected void actionPerformed(GuiButton guibutton) throws IOException {
		if (guibutton.id == 0) {
			this.mc.displayGuiScreen(new GuiOptions(this, this.mc.gameSettings));
		}

		if (guibutton.id == 5) {
			this.mc.displayGuiScreen(new GuiLanguage(this, this.mc.gameSettings, this.mc.getLanguageManager()));
		}

		if (guibutton.id == 1) {
			this.mc.displayGuiScreen(new GuiWorldSelection(this));
		}

		if (guibutton.id == 2) {
			this.mc.displayGuiScreen(new GuiMultiplayer(this));
		}

		if (guibutton.id == 4) {
			this.mc.shutdown();
		}

		if (guibutton.id == 6 && Reflector.GuiModList_Constructor.exists()) {
			this.mc.displayGuiScreen((GuiScreen) Reflector.newInstance(Reflector.GuiModList_Constructor, new Object[] { this }));
		}
	}

	@Override
	public void confirmClicked(boolean flag, int i) {
		if (i == 13) {
			if (flag) {
				try {
					Class oclass = Class.forName("java.awt.Desktop");
					Object object = oclass.getMethod("getDesktop", new Class[0]).invoke((Object) null, new Object[0]);
					oclass.getMethod("browse", new Class[] { URI.class }).invoke(object, new Object[] { new URI(this.openGLWarningLink) });
				} catch (Throwable throwable) {
					LOGGER.error("Couldn\'t open link", throwable);
				}
			}

			this.mc.displayGuiScreen(this);
		}

	}

	private void drawPanorama(int var1, int var2, float f) {
		Tessellator tessellator = Tessellator.getInstance();
		VertexBuffer vertexbuffer = tessellator.getBuffer();
		GlStateManager.matrixMode(5889);
		GlStateManager.pushMatrix();
		GlStateManager.loadIdentity();
		Project.gluPerspective(120.0F, 1.0F, 0.05F, 10.0F);
		GlStateManager.matrixMode(5888);
		GlStateManager.pushMatrix();
		GlStateManager.loadIdentity();
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		GlStateManager.rotate(180.0F, 1.0F, 0.0F, 0.0F);
		GlStateManager.rotate(90.0F, 0.0F, 0.0F, 1.0F);
		GlStateManager.enableBlend();
		GlStateManager.disableAlpha();
		GlStateManager.disableCull();
		GlStateManager.depthMask(false);
		GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE,
				GlStateManager.DestFactor.ZERO);
		boolean flag = true;
		int i = 64;
		CustomPanoramaProperties custompanoramaproperties = CustomPanorama.getCustomPanoramaProperties();
		if (custompanoramaproperties != null) {
			i = custompanoramaproperties.getBlur1();
		}

		for (int j = 0; j < i; ++j) {
			GlStateManager.pushMatrix();
			float f1 = (j % 8 / 8.0F - 0.5F) / 64.0F;
			float f2 = (j / 8 / 8.0F - 0.5F) / 64.0F;
			float f3 = 0.0F;
			GlStateManager.translate(f1, f2, 0.0F);
			GlStateManager.rotate(MathHelper.sin((this.panoramaTimer + f) / 400.0F) * 25.0F + 20.0F, 1.0F, 0.0F, 0.0F);
			GlStateManager.rotate(-(this.panoramaTimer + f) * 0.1F, 0.0F, 1.0F, 0.0F);

			for (int k = 0; k < 6; ++k) {
				GlStateManager.pushMatrix();
				if (k == 1) {
					GlStateManager.rotate(90.0F, 0.0F, 1.0F, 0.0F);
				}

				if (k == 2) {
					GlStateManager.rotate(180.0F, 0.0F, 1.0F, 0.0F);
				}

				if (k == 3) {
					GlStateManager.rotate(-90.0F, 0.0F, 1.0F, 0.0F);
				}

				if (k == 4) {
					GlStateManager.rotate(90.0F, 1.0F, 0.0F, 0.0F);
				}

				if (k == 5) {
					GlStateManager.rotate(-90.0F, 1.0F, 0.0F, 0.0F);
				}

				ResourceLocation[] aresourcelocation = TITLE_PANORAMA_PATHS;
				if (custompanoramaproperties != null) {
					aresourcelocation = custompanoramaproperties.getPanoramaLocations();
				}

				this.mc.getTextureManager().bindTexture(aresourcelocation[k]);
				vertexbuffer.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR);
				int l = 255 / (j + 1);
				float f4 = 0.0F;
				vertexbuffer.pos(-1.0D, -1.0D, 1.0D).tex(0.0D, 0.0D).color(255, 255, 255, l).endVertex();
				vertexbuffer.pos(1.0D, -1.0D, 1.0D).tex(1.0D, 0.0D).color(255, 255, 255, l).endVertex();
				vertexbuffer.pos(1.0D, 1.0D, 1.0D).tex(1.0D, 1.0D).color(255, 255, 255, l).endVertex();
				vertexbuffer.pos(-1.0D, 1.0D, 1.0D).tex(0.0D, 1.0D).color(255, 255, 255, l).endVertex();
				tessellator.draw();
				GlStateManager.popMatrix();
			}

			GlStateManager.popMatrix();
			GlStateManager.colorMask(true, true, true, false);
		}

		vertexbuffer.setTranslation(0.0D, 0.0D, 0.0D);
		GlStateManager.colorMask(true, true, true, true);
		GlStateManager.matrixMode(5889);
		GlStateManager.popMatrix();
		GlStateManager.matrixMode(5888);
		GlStateManager.popMatrix();
		GlStateManager.depthMask(true);
		GlStateManager.enableCull();
		GlStateManager.enableDepth();
	}

	private void rotateAndBlurSkybox(float var1) {
		this.mc.getTextureManager().bindTexture(this.backgroundTexture);
		GlStateManager.glTexParameteri(3553, 10241, 9729);
		GlStateManager.glTexParameteri(3553, 10240, 9729);
		GlStateManager.glCopyTexSubImage2D(3553, 0, 0, 0, 0, 0, 256, 256);
		GlStateManager.enableBlend();
		GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE,
				GlStateManager.DestFactor.ZERO);
		GlStateManager.colorMask(true, true, true, false);
		Tessellator tessellator = Tessellator.getInstance();
		VertexBuffer vertexbuffer = tessellator.getBuffer();
		vertexbuffer.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR);
		GlStateManager.disableAlpha();
		boolean flag = true;
		int i = 3;
		CustomPanoramaProperties custompanoramaproperties = CustomPanorama.getCustomPanoramaProperties();
		if (custompanoramaproperties != null) {
			i = custompanoramaproperties.getBlur2();
		}

		for (int j = 0; j < i; ++j) {
			float f = 1.0F / (j + 1);
			int k = this.width;
			int l = this.height;
			float f1 = (j - 1) / 256.0F;
			vertexbuffer.pos(k, l, this.zLevel).tex(0.0F + f1, 1.0D).color(1.0F, 1.0F, 1.0F, f).endVertex();
			vertexbuffer.pos(k, 0.0D, this.zLevel).tex(1.0F + f1, 1.0D).color(1.0F, 1.0F, 1.0F, f).endVertex();
			vertexbuffer.pos(0.0D, 0.0D, this.zLevel).tex(1.0F + f1, 0.0D).color(1.0F, 1.0F, 1.0F, f).endVertex();
			vertexbuffer.pos(0.0D, l, this.zLevel).tex(0.0F + f1, 0.0D).color(1.0F, 1.0F, 1.0F, f).endVertex();
		}

		tessellator.draw();
		GlStateManager.enableAlpha();
		GlStateManager.colorMask(true, true, true, true);
	}

	private void renderSkybox(int i, int j, float f) {
		this.mc.getFramebuffer().unbindFramebuffer();
		GlStateManager.viewport(0, 0, 256, 256);
		this.drawPanorama(i, j, f);
		this.rotateAndBlurSkybox(f);
		int k = 3;
		CustomPanoramaProperties custompanoramaproperties = CustomPanorama.getCustomPanoramaProperties();
		if (custompanoramaproperties != null) {
			k = custompanoramaproperties.getBlur3();
		}

		for (int l = 0; l < k; ++l) {
			this.rotateAndBlurSkybox(f);
			this.rotateAndBlurSkybox(f);
		}

		this.mc.getFramebuffer().bindFramebuffer(true);
		GlStateManager.viewport(0, 0, this.mc.displayWidth, this.mc.displayHeight);
		float f3 = 120.0F / (this.width > this.height ? this.width : this.height);
		float f1 = this.height * f3 / 256.0F;
		float f2 = this.width * f3 / 256.0F;
		int i1 = this.width;
		int j1 = this.height;
		Tessellator tessellator = Tessellator.getInstance();
		VertexBuffer vertexbuffer = tessellator.getBuffer();
		vertexbuffer.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR);
		vertexbuffer.pos(0.0D, j1, this.zLevel).tex(0.5F - f1, 0.5F + f2).color(1.0F, 1.0F, 1.0F, 1.0F).endVertex();
		vertexbuffer.pos(i1, j1, this.zLevel).tex(0.5F - f1, 0.5F - f2).color(1.0F, 1.0F, 1.0F, 1.0F).endVertex();
		vertexbuffer.pos(i1, 0.0D, this.zLevel).tex(0.5F + f1, 0.5F - f2).color(1.0F, 1.0F, 1.0F, 1.0F).endVertex();
		vertexbuffer.pos(0.0D, 0.0D, this.zLevel).tex(0.5F + f1, 0.5F + f2).color(1.0F, 1.0F, 1.0F, 1.0F).endVertex();
		tessellator.draw();
	}

	@Override
	public void drawScreen(int i, int j, float f) {
		GlStateManager.disableAlpha();
		this.renderSkybox(i, j, f);
		GlStateManager.enableAlpha();
		boolean flag = true;
		int k = this.width / 2 - 137;
		boolean flag1 = true;
		int l = -2130706433;
		int i1 = 16777215;
		int j1 = 0;
		int k1 = Integer.MIN_VALUE;
		CustomPanoramaProperties custompanoramaproperties = CustomPanorama.getCustomPanoramaProperties();
		if (custompanoramaproperties != null) {
			l = custompanoramaproperties.getOverlay1Top();
			i1 = custompanoramaproperties.getOverlay1Bottom();
			j1 = custompanoramaproperties.getOverlay2Top();
			k1 = custompanoramaproperties.getOverlay2Bottom();
		}

		if (l != 0 || i1 != 0) {
			this.drawGradientRect(0, 0, this.width, this.height, l, i1);
		}

		if (j1 != 0 || k1 != 0) {
			this.drawGradientRect(0, 0, this.width, this.height, j1, k1);
		}

		this.mc.getTextureManager().bindTexture(MINECRAFT_TITLE_TEXTURES);
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		if (this.updateCounter < 1.0E-4D) {
			this.drawTexturedModalRect(k + 0, 30, 0, 0, 99, 44);
			this.drawTexturedModalRect(k + 99, 30, 129, 0, 27, 44);
			this.drawTexturedModalRect(k + 99 + 26, 30, 126, 0, 3, 44);
			this.drawTexturedModalRect(k + 99 + 26 + 3, 30, 99, 0, 26, 44);
			this.drawTexturedModalRect(k + 155, 30, 0, 45, 155, 44);
		} else {
			this.drawTexturedModalRect(k + 0, 30, 0, 0, 155, 44);
			this.drawTexturedModalRect(k + 155, 30, 0, 45, 155, 44);
		}

		if (Reflector.ForgeHooksClient_renderMainMenu.exists()) {
			this.splashText = Reflector.callString(Reflector.ForgeHooksClient_renderMainMenu,
					new Object[] { this, this.fontRendererObj, Integer.valueOf(this.width), Integer.valueOf(this.height), this.splashText });
		}

		GlStateManager.pushMatrix();
		GlStateManager.translate(this.width / 2 + 90, 70.0F, 0.0F);
		GlStateManager.rotate(-20.0F, 0.0F, 0.0F, 1.0F);
		float f1 = 1.8F - MathHelper.abs(MathHelper.sin(Minecraft.getSystemTime() % 1000L / 1000.0F * 6.2831855F) * 0.1F);
		f1 = f1 * 100.0F / (this.fontRendererObj.getStringWidth(this.splashText) + 32);
		GlStateManager.scale(f1, f1, f1);
		this.drawCenteredString(this.fontRendererObj, this.splashText, 0, -8, -256);
		GlStateManager.popMatrix();
		String s = "Minecraft 1.11.2" + ("release".equalsIgnoreCase(this.mc.getVersionType()) ? "" : "/" + this.mc.getVersionType());

		if (Reflector.FMLCommonHandler_getBrandings.exists()) {
			Object object = Reflector.call(Reflector.FMLCommonHandler_instance, new Object[0]);
			List list = Lists.reverse((List) Reflector.call(object, Reflector.FMLCommonHandler_getBrandings, new Object[] { Boolean.valueOf(true) }));

			for (int l1 = 0; l1 < list.size(); ++l1) {
				String s1 = (String) list.get(l1);
				if (!Strings.isNullOrEmpty(s1)) {
					this.drawString(this.fontRendererObj, s1, 2, this.height - (10 + l1 * (this.fontRendererObj.FONT_HEIGHT + 1)), 16777215);
				}
			}
		} else {
			this.drawString(this.fontRendererObj, s, 2, this.height - 10, -1);
		}

		String s2 = "Copyright Mojang AB. Do not distribute!";
		this.drawString(this.fontRendererObj, "Copyright Mojang AB. Do not distribute!",
				this.width - this.fontRendererObj.getStringWidth("Copyright Mojang AB. Do not distribute!") - 2, this.height - 10, -1);
		if (this.openGLWarning1 != null && !this.openGLWarning1.isEmpty()) {
			drawRect(this.openGLWarningX1 - 2, this.openGLWarningY1 - 2, this.openGLWarningX2 + 2, this.openGLWarningY2 - 1, 1428160512);
			this.drawString(this.fontRendererObj, this.openGLWarning1, this.openGLWarningX1, this.openGLWarningY1, -1);
			this.drawString(this.fontRendererObj, this.openGLWarning2, (this.width - this.openGLWarning2Width) / 2, this.buttonList.get(0).yPosition - 12, -1);
		}

		super.drawScreen(i, j, f);
		if (this.modUpdateNotification != null) {
			this.modUpdateNotification.drawScreen(i, j, f);
		}

	}

	@Override
	protected void mouseClicked(int i, int j, int k) throws IOException {
		super.mouseClicked(i, j, k);
		synchronized (this.threadLock) {
			if (!this.openGLWarning1.isEmpty() && !StringUtils.isNullOrEmpty(this.openGLWarningLink) && i >= this.openGLWarningX1 && i <= this.openGLWarningX2
					&& j >= this.openGLWarningY1 && j <= this.openGLWarningY2) {
				GuiConfirmOpenLink guiconfirmopenlink = new GuiConfirmOpenLink(this, this.openGLWarningLink, 13, true);
				guiconfirmopenlink.disableSecurityWarning();
				this.mc.displayGuiScreen(guiconfirmopenlink);
			}
		}

		if (Reflector.ForgeHooksClient_mainMenuMouseClick.exists()) {
			Reflector.call(Reflector.ForgeHooksClient_mainMenuMouseClick,
					new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), this.fontRendererObj, Integer.valueOf(this.width) });
		}

	}
}
