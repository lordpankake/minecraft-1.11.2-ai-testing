package net.minecraft.client.gui;

import com.ibm.icu.text.ArabicShaping;
import com.ibm.icu.text.ArabicShapingException;
import com.ibm.icu.text.Bidi;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Random;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.resources.IResource;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.client.resources.IResourceManagerReloadListener;
import net.minecraft.client.settings.GameSettings;
import net.optifine.Config;
import net.optifine.CustomColors;
import net.optifine.FontUtils;
import net.minecraft.util.ResourceLocation;
import org.apache.commons.io.IOUtils;

public class FontRenderer implements IResourceManagerReloadListener {
	private static final ResourceLocation[] UNICODE_PAGE_LOCATIONS = new ResourceLocation[256];
	private float[] charWidth = new float[256];
	public int FONT_HEIGHT = 9;
	public Random fontRandom = new Random();
	private final byte[] glyphWidth = new byte[65536];
	private final int[] colorCode = new int[32];
	private ResourceLocation locationFontTexture;
	private final TextureManager renderEngine;
	private float posX;
	private float posY;
	private boolean unicodeFlag;
	private boolean bidiFlag;
	private float red;
	private float blue;
	private float green;
	private float alpha;
	private int textColor;
	private boolean randomStyle;
	private boolean boldStyle;
	private boolean italicStyle;
	private boolean underlineStyle;
	private boolean strikethroughStyle;
	public GameSettings gameSettings;
	public ResourceLocation locationFontTextureBase;
	public boolean enabled = true;
	public float offsetBold = 1.0F;

	public FontRenderer(GameSettings gamesettings, ResourceLocation resourcelocation, TextureManager texturemanager, boolean flag) {
		this.gameSettings = gamesettings;
		this.locationFontTextureBase = resourcelocation;
		this.locationFontTexture = resourcelocation;
		this.renderEngine = texturemanager;
		this.unicodeFlag = flag;
		this.locationFontTexture = FontUtils.getHdFontLocation(this.locationFontTextureBase);
		this.bindTexture(this.locationFontTexture);

		for (int i = 0; i < 32; ++i) {
			int j = (i >> 3 & 1) * 85;
			int k = (i >> 2 & 1) * 170 + j;
			int l = (i >> 1 & 1) * 170 + j;
			int i1 = (i >> 0 & 1) * 170 + j;
			if (i == 6) {
				k += 85;
			}

			if (gamesettings.anaglyph) {
				int j1 = (k * 30 + l * 59 + i1 * 11) / 100;
				int k1 = (k * 30 + l * 70) / 100;
				int l1 = (k * 30 + i1 * 70) / 100;
				k = j1;
				l = k1;
				i1 = l1;
			}

			if (i >= 16) {
				k /= 4;
				l /= 4;
				i1 /= 4;
			}

			this.colorCode[i] = (k & 255) << 16 | (l & 255) << 8 | i1 & 255;
		}

		this.readGlyphSizes();
	}

	@Override
	public void onResourceManagerReload(IResourceManager var1) {
		this.locationFontTexture = FontUtils.getHdFontLocation(this.locationFontTextureBase);

		for (int i = 0; i < UNICODE_PAGE_LOCATIONS.length; ++i) {
			UNICODE_PAGE_LOCATIONS[i] = null;
		}

		this.readFontTexture();
		this.readGlyphSizes();
	}

	private void readFontTexture() {
		IResource iresource = null;

		BufferedImage bufferedimage;
		try {
			iresource = this.getResource(this.locationFontTexture);
			bufferedimage = TextureUtil.readBufferedImage(iresource.getInputStream());
		} catch (IOException ioexception) {
			throw new RuntimeException(ioexception);
		} finally {
			IOUtils.closeQuietly(iresource);
		}

		Properties properties = FontUtils.readFontProperties(this.locationFontTexture);
		int i = bufferedimage.getWidth();
		int j = bufferedimage.getHeight();
		int k = i / 16;
		int l = j / 16;
		float f = i / 128.0F;
		float f1 = Config.limit(f, 1.0F, 2.0F);
		this.offsetBold = 1.0F / f1;
		float f2 = FontUtils.readFloat(properties, "offsetBold", -1.0F);
		if (f2 >= 0.0F) {
			this.offsetBold = f2;
		}

		int[] aint = new int[i * j];
		bufferedimage.getRGB(0, 0, i, j, aint, 0, i);

		for (int i1 = 0; i1 < 256; ++i1) {
			int j1 = i1 % 16;
			int k1 = i1 / 16;
			int l1 = 0;

			for (l1 = k - 1; l1 >= 0; --l1) {
				int i2 = j1 * k + l1;
				boolean flag = true;

				for (int j2 = 0; j2 < l && flag; ++j2) {
					int k2 = (k1 * l + j2) * i;
					int l2 = aint[i2 + k2];
					int i3 = l2 >> 24 & 255;
					if (i3 > 16) {
						flag = false;
					}
				}

				if (!flag) {
					break;
				}
			}

			if (i1 == 32) {
				if (k <= 8) {
					l1 = (int) (2.0F * f);
				} else {
					l1 = (int) (1.5F * f);
				}
			}

			this.charWidth[i1] = (l1 + 1) / f + 1.0F;
		}

		FontUtils.readCustomCharWidths(properties, this.charWidth);
	}

	private void readGlyphSizes() {
		IResource iresource = null;

		try {
			iresource = this.getResource(new ResourceLocation("font/glyph_sizes.bin"));
			iresource.getInputStream().read(this.glyphWidth);
		} catch (IOException ioexception) {
			throw new RuntimeException(ioexception);
		} finally {
			IOUtils.closeQuietly(iresource);
		}

	}

	private float renderChar(char c0, boolean flag) {
		if (c0 != 32 && c0 != 160) {
			int i = "\u00c0\u00c1\u00c2\u00c8\u00ca\u00cb\u00cd\u00d3\u00d4\u00d5\u00da\u00df\u00e3\u00f5\u011f\u0130\u0131\u0152\u0153\u015e\u015f\u0174\u0175\u017e\u0207\u0000\u0000\u0000\u0000\u0000\u0000\u0000 !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\u0000\u00c7\u00fc\u00e9\u00e2\u00e4\u00e0\u00e5\u00e7\u00ea\u00eb\u00e8\u00ef\u00ee\u00ec\u00c4\u00c5\u00c9\u00e6\u00c6\u00f4\u00f6\u00f2\u00fb\u00f9\u00ff\u00d6\u00dc\u00f8\u00a3\u00d8\u00d7\u0192\u00e1\u00ed\u00f3\u00fa\u00f1\u00d1\u00aa\u00ba\u00bf\u00ae\u00ac\u00bd\u00bc\u00a1\u00ab\u00bb\u2591\u2592\u2593\u2502\u2524\u2561\u2562\u2556\u2555\u2563\u2551\u2557\u255d\u255c\u255b\u2510\u2514\u2534\u252c\u251c\u2500\u253c\u255e\u255f\u255a\u2554\u2569\u2566\u2560\u2550\u256c\u2567\u2568\u2564\u2565\u2559\u2558\u2552\u2553\u256b\u256a\u2518\u250c\u2588\u2584\u258c\u2590\u2580\u03b1\u03b2\u0393\u03c0\u03a3\u03c3\u03bc\u03c4\u03a6\u0398\u03a9\u03b4\u221e\u2205\u2208\u2229\u2261\u00b1\u2265\u2264\u2320\u2321\u00f7\u2248\u00b0\u2219\u00b7\u221a\u207f\u00b2\u25a0\u0000"
					.indexOf(c0);
			return i != -1 && !this.unicodeFlag ? this.renderDefaultChar(i, flag) : this.renderUnicodeChar(c0, flag);
		} else {
			return !this.unicodeFlag ? this.charWidth[c0] : 4.0F;
		}
	}

	private float renderDefaultChar(int i, boolean flag) {
		int j = i % 16 * 8;
		int k = i / 16 * 8;
		int l = flag ? 1 : 0;
		this.bindTexture(this.locationFontTexture);
		float f = this.charWidth[i];
		float f1 = 7.99F;
		GlStateManager.glBegin(5);
		GlStateManager.glTexCoord2f(j / 128.0F, k / 128.0F);
		GlStateManager.glVertex3f(this.posX + l, this.posY, 0.0F);
		GlStateManager.glTexCoord2f(j / 128.0F, (k + 7.99F) / 128.0F);
		GlStateManager.glVertex3f(this.posX - l, this.posY + 7.99F, 0.0F);
		GlStateManager.glTexCoord2f((j + f1 - 1.0F) / 128.0F, k / 128.0F);
		GlStateManager.glVertex3f(this.posX + f1 - 1.0F + l, this.posY, 0.0F);
		GlStateManager.glTexCoord2f((j + f1 - 1.0F) / 128.0F, (k + 7.99F) / 128.0F);
		GlStateManager.glVertex3f(this.posX + f1 - 1.0F - l, this.posY + 7.99F, 0.0F);
		GlStateManager.glEnd();
		return f;
	}

	private static ResourceLocation getUnicodePageLocation(int i) {
		if (UNICODE_PAGE_LOCATIONS[i] == null) {
			UNICODE_PAGE_LOCATIONS[i] = new ResourceLocation(String.format("textures/font/unicode_page_%02x.png", new Object[] { Integer.valueOf(i) }));
			UNICODE_PAGE_LOCATIONS[i] = FontUtils.getHdFontLocation(UNICODE_PAGE_LOCATIONS[i]);
		}

		return UNICODE_PAGE_LOCATIONS[i];
	}

	private void loadGlyphTexture(int i) {
		this.bindTexture(FontRenderer.getUnicodePageLocation(i));
	}

	private float renderUnicodeChar(char c0, boolean flag) {
		int i = this.glyphWidth[c0] & 255;
		if (i == 0) {
			return 0.0F;
		} else {
			int j = c0 / 256;
			this.loadGlyphTexture(j);
			int k = i >>> 4;
			int l = i & 15;
			float f = k;
			float f1 = l + 1;
			float f2 = c0 % 16 * 16 + f;
			float f3 = (c0 & 255) / 16 * 16;
			float f4 = f1 - f - 0.02F;
			float f5 = flag ? 1.0F : 0.0F;
			GlStateManager.glBegin(5);
			GlStateManager.glTexCoord2f(f2 / 256.0F, f3 / 256.0F);
			GlStateManager.glVertex3f(this.posX + f5, this.posY, 0.0F);
			GlStateManager.glTexCoord2f(f2 / 256.0F, (f3 + 15.98F) / 256.0F);
			GlStateManager.glVertex3f(this.posX - f5, this.posY + 7.99F, 0.0F);
			GlStateManager.glTexCoord2f((f2 + f4) / 256.0F, f3 / 256.0F);
			GlStateManager.glVertex3f(this.posX + f4 / 2.0F + f5, this.posY, 0.0F);
			GlStateManager.glTexCoord2f((f2 + f4) / 256.0F, (f3 + 15.98F) / 256.0F);
			GlStateManager.glVertex3f(this.posX + f4 / 2.0F - f5, this.posY + 7.99F, 0.0F);
			GlStateManager.glEnd();
			return (f1 - f) / 2.0F + 1.0F;
		}
	}

	public int drawStringWithShadow(String s, float f, float f1, int i) {
		return this.drawString(s, f, f1, i, true);
	}

	public int drawString(String s, int i, int j, int k) {
		return !this.enabled ? 0 : this.drawString(s, i, j, k, false);
	}

	public int drawString(String s, float f, float f1, int i, boolean flag) {
		this.enableAlpha();
		this.resetStyles();
		int j;
		if (flag) {
			j = this.renderString(s, f + 1.0F, f1 + 1.0F, i, true);
			j = Math.max(j, this.renderString(s, f, f1, i, false));
		} else {
			j = this.renderString(s, f, f1, i, false);
		}

		return j;
	}

	private static String bidiReorder(String s) {
		try {
			Bidi bidi = new Bidi((new ArabicShaping(8)).shape(s), 127);
			bidi.setReorderingMode(0);
			return bidi.writeReordered(2);
		} catch (ArabicShapingException var3) {
			return s;
		}
	}

	private void resetStyles() {
		this.randomStyle = false;
		this.boldStyle = false;
		this.italicStyle = false;
		this.underlineStyle = false;
		this.strikethroughStyle = false;
	}

	private void renderStringAtPos(String s, boolean flag) {
		for (int i = 0; i < s.length(); ++i) {
			char c0 = s.charAt(i);
			if (c0 == 167 && i + 1 < s.length()) {
				int i1 = "0123456789abcdefklmnor".indexOf(String.valueOf(s.charAt(i + 1)).toLowerCase(Locale.ROOT).charAt(0));
				if (i1 < 16) {
					this.randomStyle = false;
					this.boldStyle = false;
					this.strikethroughStyle = false;
					this.underlineStyle = false;
					this.italicStyle = false;
					if (i1 < 0 || i1 > 15) {
						i1 = 15;
					}

					if (flag) {
						i1 += 16;
					}

					int j1 = this.colorCode[i1];
					if (Config.isCustomColors()) {
						j1 = CustomColors.getTextColor(i1, j1);
					}

					this.textColor = j1;
					this.setColor((j1 >> 16) / 255.0F, (j1 >> 8 & 255) / 255.0F, (j1 & 255) / 255.0F, this.alpha);
				} else if (i1 == 16) {
					this.randomStyle = true;
				} else if (i1 == 17) {
					this.boldStyle = true;
				} else if (i1 == 18) {
					this.strikethroughStyle = true;
				} else if (i1 == 19) {
					this.underlineStyle = true;
				} else if (i1 == 20) {
					this.italicStyle = true;
				} else if (i1 == 21) {
					this.randomStyle = false;
					this.boldStyle = false;
					this.strikethroughStyle = false;
					this.underlineStyle = false;
					this.italicStyle = false;
					this.setColor(this.red, this.blue, this.green, this.alpha);
				}

				++i;
			} else {
				int j = "\u00c0\u00c1\u00c2\u00c8\u00ca\u00cb\u00cd\u00d3\u00d4\u00d5\u00da\u00df\u00e3\u00f5\u011f\u0130\u0131\u0152\u0153\u015e\u015f\u0174\u0175\u017e\u0207\u0000\u0000\u0000\u0000\u0000\u0000\u0000 !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\u0000\u00c7\u00fc\u00e9\u00e2\u00e4\u00e0\u00e5\u00e7\u00ea\u00eb\u00e8\u00ef\u00ee\u00ec\u00c4\u00c5\u00c9\u00e6\u00c6\u00f4\u00f6\u00f2\u00fb\u00f9\u00ff\u00d6\u00dc\u00f8\u00a3\u00d8\u00d7\u0192\u00e1\u00ed\u00f3\u00fa\u00f1\u00d1\u00aa\u00ba\u00bf\u00ae\u00ac\u00bd\u00bc\u00a1\u00ab\u00bb\u2591\u2592\u2593\u2502\u2524\u2561\u2562\u2556\u2555\u2563\u2551\u2557\u255d\u255c\u255b\u2510\u2514\u2534\u252c\u251c\u2500\u253c\u255e\u255f\u255a\u2554\u2569\u2566\u2560\u2550\u256c\u2567\u2568\u2564\u2565\u2559\u2558\u2552\u2553\u256b\u256a\u2518\u250c\u2588\u2584\u258c\u2590\u2580\u03b1\u03b2\u0393\u03c0\u03a3\u03c3\u03bc\u03c4\u03a6\u0398\u03a9\u03b4\u221e\u2205\u2208\u2229\u2261\u00b1\u2265\u2264\u2320\u2321\u00f7\u2248\u00b0\u2219\u00b7\u221a\u207f\u00b2\u25a0\u0000"
						.indexOf(c0);
				if (this.randomStyle && j != -1) {
					int k = this.getCharWidth(c0);

					char c1;
					while (true) {
						j = this.fontRandom.nextInt(
								"\u00c0\u00c1\u00c2\u00c8\u00ca\u00cb\u00cd\u00d3\u00d4\u00d5\u00da\u00df\u00e3\u00f5\u011f\u0130\u0131\u0152\u0153\u015e\u015f\u0174\u0175\u017e\u0207\u0000\u0000\u0000\u0000\u0000\u0000\u0000 !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\u0000\u00c7\u00fc\u00e9\u00e2\u00e4\u00e0\u00e5\u00e7\u00ea\u00eb\u00e8\u00ef\u00ee\u00ec\u00c4\u00c5\u00c9\u00e6\u00c6\u00f4\u00f6\u00f2\u00fb\u00f9\u00ff\u00d6\u00dc\u00f8\u00a3\u00d8\u00d7\u0192\u00e1\u00ed\u00f3\u00fa\u00f1\u00d1\u00aa\u00ba\u00bf\u00ae\u00ac\u00bd\u00bc\u00a1\u00ab\u00bb\u2591\u2592\u2593\u2502\u2524\u2561\u2562\u2556\u2555\u2563\u2551\u2557\u255d\u255c\u255b\u2510\u2514\u2534\u252c\u251c\u2500\u253c\u255e\u255f\u255a\u2554\u2569\u2566\u2560\u2550\u256c\u2567\u2568\u2564\u2565\u2559\u2558\u2552\u2553\u256b\u256a\u2518\u250c\u2588\u2584\u258c\u2590\u2580\u03b1\u03b2\u0393\u03c0\u03a3\u03c3\u03bc\u03c4\u03a6\u0398\u03a9\u03b4\u221e\u2205\u2208\u2229\u2261\u00b1\u2265\u2264\u2320\u2321\u00f7\u2248\u00b0\u2219\u00b7\u221a\u207f\u00b2\u25a0\u0000"
										.length());
						c1 = "\u00c0\u00c1\u00c2\u00c8\u00ca\u00cb\u00cd\u00d3\u00d4\u00d5\u00da\u00df\u00e3\u00f5\u011f\u0130\u0131\u0152\u0153\u015e\u015f\u0174\u0175\u017e\u0207\u0000\u0000\u0000\u0000\u0000\u0000\u0000 !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\u0000\u00c7\u00fc\u00e9\u00e2\u00e4\u00e0\u00e5\u00e7\u00ea\u00eb\u00e8\u00ef\u00ee\u00ec\u00c4\u00c5\u00c9\u00e6\u00c6\u00f4\u00f6\u00f2\u00fb\u00f9\u00ff\u00d6\u00dc\u00f8\u00a3\u00d8\u00d7\u0192\u00e1\u00ed\u00f3\u00fa\u00f1\u00d1\u00aa\u00ba\u00bf\u00ae\u00ac\u00bd\u00bc\u00a1\u00ab\u00bb\u2591\u2592\u2593\u2502\u2524\u2561\u2562\u2556\u2555\u2563\u2551\u2557\u255d\u255c\u255b\u2510\u2514\u2534\u252c\u251c\u2500\u253c\u255e\u255f\u255a\u2554\u2569\u2566\u2560\u2550\u256c\u2567\u2568\u2564\u2565\u2559\u2558\u2552\u2553\u256b\u256a\u2518\u250c\u2588\u2584\u258c\u2590\u2580\u03b1\u03b2\u0393\u03c0\u03a3\u03c3\u03bc\u03c4\u03a6\u0398\u03a9\u03b4\u221e\u2205\u2208\u2229\u2261\u00b1\u2265\u2264\u2320\u2321\u00f7\u2248\u00b0\u2219\u00b7\u221a\u207f\u00b2\u25a0\u0000"
								.charAt(j);
						if (k == this.getCharWidth(c1)) {
							break;
						}
					}

					c0 = c1;
				}

				float f1 = j != -1 && !this.unicodeFlag ? this.offsetBold : 0.5F;
				boolean flag1 = (c0 == 0 || j == -1 || this.unicodeFlag) && flag;
				if (flag1) {
					this.posX -= f1;
					this.posY -= f1;
				}

				float f = this.renderChar(c0, this.italicStyle);
				if (flag1) {
					this.posX += f1;
					this.posY += f1;
				}

				if (this.boldStyle) {
					this.posX += f1;
					if (flag1) {
						this.posX -= f1;
						this.posY -= f1;
					}

					this.renderChar(c0, this.italicStyle);
					this.posX -= f1;
					if (flag1) {
						this.posX += f1;
						this.posY += f1;
					}

					f += f1;
				}

				if (this.strikethroughStyle) {
					Tessellator tessellator = Tessellator.getInstance();
					VertexBuffer vertexbuffer = tessellator.getBuffer();
					GlStateManager.disableTexture2D();
					vertexbuffer.begin(7, DefaultVertexFormats.POSITION);
					vertexbuffer.pos(this.posX, this.posY + this.FONT_HEIGHT / 2, 0.0D).endVertex();
					vertexbuffer.pos(this.posX + f, this.posY + this.FONT_HEIGHT / 2, 0.0D).endVertex();
					vertexbuffer.pos(this.posX + f, this.posY + this.FONT_HEIGHT / 2 - 1.0F, 0.0D).endVertex();
					vertexbuffer.pos(this.posX, this.posY + this.FONT_HEIGHT / 2 - 1.0F, 0.0D).endVertex();
					tessellator.draw();
					GlStateManager.enableTexture2D();
				}

				if (this.underlineStyle) {
					Tessellator tessellator1 = Tessellator.getInstance();
					VertexBuffer vertexbuffer1 = tessellator1.getBuffer();
					GlStateManager.disableTexture2D();
					vertexbuffer1.begin(7, DefaultVertexFormats.POSITION);
					int l = this.underlineStyle ? -1 : 0;
					vertexbuffer1.pos(this.posX + l, this.posY + this.FONT_HEIGHT, 0.0D).endVertex();
					vertexbuffer1.pos(this.posX + f, this.posY + this.FONT_HEIGHT, 0.0D).endVertex();
					vertexbuffer1.pos(this.posX + f, this.posY + this.FONT_HEIGHT - 1.0F, 0.0D).endVertex();
					vertexbuffer1.pos(this.posX + l, this.posY + this.FONT_HEIGHT - 1.0F, 0.0D).endVertex();
					tessellator1.draw();
					GlStateManager.enableTexture2D();
				}

				this.posX += f;
			}
		}

	}

	private int renderStringAligned(String s, int i, int j, int k, int l, boolean flag) {
		if (this.bidiFlag) {
			String bidiReorder = FontRenderer.bidiReorder(s);
			int i1 = this.getStringWidth(bidiReorder);
			i = i + k - i1;
		}

		return this.renderString(s, i, j, l, flag);
	}

	private int renderString(String s, float f, float f1, int i, boolean flag) {
		if (s == null) {
			return 0;
		} else {
			if (this.bidiFlag) {
				s = FontRenderer.bidiReorder(s);
			}

			if ((i & -67108864) == 0) {
				i |= -16777216;
			}

			if (flag) {
				i = (i & 16579836) >> 2 | i & -16777216;
			}

			this.red = (i >> 16 & 255) / 255.0F;
			this.blue = (i >> 8 & 255) / 255.0F;
			this.green = (i & 255) / 255.0F;
			this.alpha = (i >> 24 & 255) / 255.0F;
			this.setColor(this.red, this.blue, this.green, this.alpha);
			this.posX = f;
			this.posY = f1;
			this.renderStringAtPos(s, flag);
			return (int) this.posX;
		}
	}

	public int getStringWidth(String s) {
		if (s == null) {
			return 0;
		} else {
			float f = 0.0F;
			boolean flag = false;

			for (int i = 0; i < s.length(); ++i) {
				char c0 = s.charAt(i);
				float f1 = this.getCharWidthFloat(c0);
				if (f1 < 0.0F && i < s.length() - 1) {
					++i;
					c0 = s.charAt(i);
					if (c0 != 108 && c0 != 76) {
						if (c0 == 114 || c0 == 82) {
							flag = false;
						}
					} else {
						flag = true;
					}

					f1 = 0.0F;
				}

				f += f1;
				if (flag && f1 > 0.0F) {
					f += this.unicodeFlag ? 1.0F : this.offsetBold;
				}
			}

			return (int) f;
		}
	}

	public int getCharWidth(char c0) {
		return Math.round(this.getCharWidthFloat(c0));
	}

	private float getCharWidthFloat(char c0) {
		if (c0 == 167) {
			return -1.0F;
		} else if (c0 != 32 && c0 != 160) {
			int i = "\u00c0\u00c1\u00c2\u00c8\u00ca\u00cb\u00cd\u00d3\u00d4\u00d5\u00da\u00df\u00e3\u00f5\u011f\u0130\u0131\u0152\u0153\u015e\u015f\u0174\u0175\u017e\u0207\u0000\u0000\u0000\u0000\u0000\u0000\u0000 !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\u0000\u00c7\u00fc\u00e9\u00e2\u00e4\u00e0\u00e5\u00e7\u00ea\u00eb\u00e8\u00ef\u00ee\u00ec\u00c4\u00c5\u00c9\u00e6\u00c6\u00f4\u00f6\u00f2\u00fb\u00f9\u00ff\u00d6\u00dc\u00f8\u00a3\u00d8\u00d7\u0192\u00e1\u00ed\u00f3\u00fa\u00f1\u00d1\u00aa\u00ba\u00bf\u00ae\u00ac\u00bd\u00bc\u00a1\u00ab\u00bb\u2591\u2592\u2593\u2502\u2524\u2561\u2562\u2556\u2555\u2563\u2551\u2557\u255d\u255c\u255b\u2510\u2514\u2534\u252c\u251c\u2500\u253c\u255e\u255f\u255a\u2554\u2569\u2566\u2560\u2550\u256c\u2567\u2568\u2564\u2565\u2559\u2558\u2552\u2553\u256b\u256a\u2518\u250c\u2588\u2584\u258c\u2590\u2580\u03b1\u03b2\u0393\u03c0\u03a3\u03c3\u03bc\u03c4\u03a6\u0398\u03a9\u03b4\u221e\u2205\u2208\u2229\u2261\u00b1\u2265\u2264\u2320\u2321\u00f7\u2248\u00b0\u2219\u00b7\u221a\u207f\u00b2\u25a0\u0000"
					.indexOf(c0);
			if (c0 > 0 && i != -1 && !this.unicodeFlag) {
				return this.charWidth[i];
			} else if (this.glyphWidth[c0] != 0) {
				int j = this.glyphWidth[c0] & 255;
				int k = j >>> 4;
				int l = j & 15;
				++l;
				return (l - k) / 2 + 1;
			} else {
				return 0.0F;
			}
		} else {
			return this.charWidth[32];
		}
	}

	public String trimStringToWidth(String s, int i) {
		return this.trimStringToWidth(s, i, false);
	}

	public String trimStringToWidth(String s, int i, boolean flag) {
		StringBuilder stringbuilder = new StringBuilder();
		float f = 0.0F;
		int j = flag ? s.length() - 1 : 0;
		int k = flag ? -1 : 1;
		boolean flag1 = false;
		boolean flag2 = false;

		for (int l = j; l >= 0 && l < s.length() && f < i; l += k) {
			char c0 = s.charAt(l);
			float f1 = this.getCharWidthFloat(c0);
			if (flag1) {
				flag1 = false;
				if (c0 != 108 && c0 != 76) {
					if (c0 == 114 || c0 == 82) {
						flag2 = false;
					}
				} else {
					flag2 = true;
				}
			} else if (f1 < 0.0F) {
				flag1 = true;
			} else {
				f += f1;
				if (flag2) {
					++f;
				}
			}

			if (f > i) {
				break;
			}

			if (flag) {
				stringbuilder.insert(0, c0);
			} else {
				stringbuilder.append(c0);
			}
		}

		return stringbuilder.toString();
	}

	private static String trimStringNewline(String s) {
		while (s != null && s.endsWith("\n")) {
			s = s.substring(0, s.length() - 1);
		}

		return s;
	}

	public void drawSplitString(String s, int i, int j, int k, int l) {
		this.resetStyles();
		this.textColor = l;
		s = FontRenderer.trimStringNewline(s);
		this.renderSplitString(s, i, j, k, false);
	}

	private void renderSplitString(String s, int i, int j, int k, boolean flag) {
		for (String s1 : this.listFormattedStringToWidth(s, k)) {
			this.renderStringAligned(s1, i, j, k, this.textColor, flag);
			j += this.FONT_HEIGHT;
		}

	}

	public int getWordWrappedHeight(String s, int i) {
		return this.FONT_HEIGHT * this.listFormattedStringToWidth(s, i).size();
	}

	public void setUnicodeFlag(boolean flag) {
		this.unicodeFlag = flag;
	}

	public boolean getUnicodeFlag() {
		return this.unicodeFlag;
	}

	public void setBidiFlag(boolean flag) {
		this.bidiFlag = flag;
	}

	public List<String> listFormattedStringToWidth(String s, int i) {
		return Arrays.<String> asList(this.wrapFormattedStringToWidth(s, i).split("\n"));
	}

	String wrapFormattedStringToWidth(String s, int i) {
		int j = this.sizeStringToWidth(s, i);
		if (s.length() <= j) {
			return s;
		} else {
			String s1 = s.substring(0, j);
			char c0 = s.charAt(j);
			boolean flag = c0 == 32 || c0 == 10;
			String s2 = getFormatFromString(s1) + s.substring(j + (flag ? 1 : 0));
			return s1 + "\n" + this.wrapFormattedStringToWidth(s2, i);
		}
	}

	private int sizeStringToWidth(String s, int i) {
		int j = s.length();
		float f = 0.0F;
		int k = 0;
		int l = -1;

		for (boolean flag = false; k < j; ++k) {
			char c0 = s.charAt(k);
			switch (c0) {
			case '\n':
				--k;
				break;
			case ' ':
				l = k;
			default:
				f += this.getCharWidthFloat(c0);
				if (flag) {
					++f;
				}
				break;
			case '\u00a7':
				if (k < j - 1) {
					++k;
					char c1 = s.charAt(k);
					if (c1 != 108 && c1 != 76) {
						if (c1 == 114 || c1 == 82 || isFormatColor(c1)) {
							flag = false;
						}
					} else {
						flag = true;
					}
				}
			}

			if (c0 == 10) {
				++k;
				l = k;
				break;
			}

			if (f > i) {
				break;
			}
		}

		return k != j && l != -1 && l < k ? l : k;
	}

	private static boolean isFormatColor(char c0) {
		return c0 >= 48 && c0 <= 57 || c0 >= 97 && c0 <= 102 || c0 >= 65 && c0 <= 70;
	}

	private static boolean isFormatSpecial(char c0) {
		return c0 >= 107 && c0 <= 111 || c0 >= 75 && c0 <= 79 || c0 == 114 || c0 == 82;
	}

	public static String getFormatFromString(String s) {
		String s1 = "";
		int i = -1;
		int j = s.length();

		while ((i = s.indexOf(167, i + 1)) != -1) {
			if (i < j - 1) {
				char c0 = s.charAt(i + 1);
				if (isFormatColor(c0)) {
					s1 = "\u00a7" + c0;
				} else if (isFormatSpecial(c0)) {
					s1 = s1 + "\u00a7" + c0;
				}
			}
		}

		return s1;
	}

	public boolean getBidiFlag() {
		return this.bidiFlag;
	}

	public int getColorCode(char c0) {
		int i = "0123456789abcdef".indexOf(c0);
		if (i >= 0 && i < this.colorCode.length) {
			int j = this.colorCode[i];
			if (Config.isCustomColors()) {
				j = CustomColors.getTextColor(i, j);
			}

			return j;
		} else {
			return 16777215;
		}
	}

	protected void setColor(float f, float f1, float f2, float f3) {
		GlStateManager.color(f, f1, f2, f3);
	}

	protected void enableAlpha() {
		GlStateManager.enableAlpha();
	}

	protected void bindTexture(ResourceLocation resourcelocation) {
		this.renderEngine.bindTexture(resourcelocation);
	}

	protected IResource getResource(ResourceLocation resourcelocation) throws IOException {
		return Minecraft.getMinecraft().getResourceManager().getResource(resourcelocation);
	}
}
