package net.minecraft.client.renderer;

import net.minecraft.client.renderer.texture.Stitcher;

public class StitcherException extends RuntimeException {
	public StitcherException(Stitcher.Holder holderIn, String message) {
		super(message);
	}
}
