package net.minecraft.server.integrated;

import com.google.common.collect.Lists;
import com.google.common.util.concurrent.Futures;
import com.mojang.authlib.GameProfileRepository;
import com.mojang.authlib.minecraft.MinecraftSessionService;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.util.concurrent.FutureTask;
import net.minecraft.client.ClientBrandRetriever;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ThreadLanServerPing;
import net.minecraft.command.ServerCommandManager;
import net.minecraft.crash.CrashReport;
import net.minecraft.crash.ICrashReportDetail;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.profiler.Snooper;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.integrated.IntegratedPlayerList;
import net.minecraft.server.integrated.IntegratedServerCommandManager;
import net.minecraft.server.management.PlayerProfileCache;
import net.optifine.Reflector;
import net.optifine.WorldServerOF;
import net.minecraft.util.CryptManager;
import net.minecraft.util.HttpUtil;
import net.minecraft.util.Util;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.GameType;
import net.minecraft.world.ServerWorldEventHandler;
import net.minecraft.world.WorldServer;
import net.minecraft.world.WorldServerMulti;
import net.minecraft.world.WorldSettings;
import net.minecraft.world.WorldType;
import net.minecraft.world.storage.ISaveHandler;
import net.minecraft.world.storage.WorldInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class IntegratedServer extends MinecraftServer {
	private static final Logger LOGGER = LogManager.getLogger();
	private final Minecraft mc;
	private final WorldSettings theWorldSettings;
	private boolean isGamePaused;
	private boolean isPublic;
	private ThreadLanServerPing lanServerPing;

	public IntegratedServer(Minecraft minecraft, String s, String s1, WorldSettings worldsettings, YggdrasilAuthenticationService yggdrasilauthenticationservice,
			MinecraftSessionService minecraftsessionservice, GameProfileRepository gameprofilerepository, PlayerProfileCache playerprofilecache) {
		super(new File(minecraft.mcDataDir, "saves"), minecraft.getProxy(), minecraft.getDataFixer(), yggdrasilauthenticationservice, minecraftsessionservice,
				gameprofilerepository, playerprofilecache);
		this.setServerOwner(minecraft.getSession().getUsername());
		this.setFolderName(s);
		this.setWorldName(s1);
		this.canCreateBonusChest(worldsettings.isBonusChestEnabled());
		this.setBuildLimit(256);
		this.setPlayerList(new IntegratedPlayerList(this));
		this.mc = minecraft;
		this.theWorldSettings = worldsettings;
	}

	@Override
	public ServerCommandManager createCommandManager() {
		return new IntegratedServerCommandManager(this);
	}

	@Override
	public void loadAllWorlds(String s, String s1, long var3, WorldType var5, String var6) {
		this.convertMapIfNeeded(s);
		ISaveHandler isavehandler = this.getActiveAnvilConverter().getSaveLoader(s, true);
		this.setResourcePackFromWorld(this.getFolderName(), isavehandler);
		WorldInfo worldinfo = isavehandler.loadWorldInfo();
		if (Reflector.DimensionManager.exists()) {
			WorldServer worldserver = (WorldServer) (new WorldServerOF(this, isavehandler, worldinfo, 0, this.theProfiler)).init();
			worldserver.initialize(this.theWorldSettings);
			Integer[] ainteger = ((Integer[]) Reflector.call(Reflector.DimensionManager_getStaticDimensionIDs, new Object[0]));
			Integer[] ainteger1 = ainteger;
			int i = ainteger.length;

			for (int j = 0; j < i; ++j) {
				int k = ainteger1[j].intValue();
				WorldServer worldserver1 = k == 0 ? worldserver : (WorldServer) ((WorldServer) (new WorldServerMulti(this, isavehandler, k, worldserver, this.theProfiler)).init());
				worldserver1.addEventListener(new ServerWorldEventHandler(this, worldserver1));
				if (!this.isSinglePlayer()) {
					worldserver1.getWorldInfo().setGameType(this.getGameType());
				}

				if (Reflector.EventBus.exists()) {
					Reflector.postForgeBusEvent(Reflector.WorldEvent_Load_Constructor, new Object[] { worldserver1 });
				}
			}

			this.getPlayerList().setPlayerManager(new WorldServer[] { worldserver });
			if (worldserver.getWorldInfo().getDifficulty() == null) {
				this.setDifficultyForAllWorlds(this.mc.gameSettings.difficulty);
			}
		} else {
			this.worlds = new WorldServer[3];
			this.timeOfLastDimensionTick = new long[this.worlds.length][100];
			this.setResourcePackFromWorld(this.getFolderName(), isavehandler);
			if (worldinfo == null) {
				worldinfo = new WorldInfo(this.theWorldSettings, s1);
			} else {
				worldinfo.setWorldName(s1);
			}

			for (int l = 0; l < this.worlds.length; ++l) {
				byte b0 = 0;
				if (l == 1) {
					b0 = -1;
				}

				if (l == 2) {
					b0 = 1;
				}

				if (l == 0) {
					this.worlds[l] = (WorldServer) (new WorldServerOF(this, isavehandler, worldinfo, b0, this.theProfiler)).init();

					this.worlds[l].initialize(this.theWorldSettings);
				} else {
					this.worlds[l] = (WorldServer) (new WorldServerMulti(this, isavehandler, b0, this.worlds[0], this.theProfiler)).init();
				}

				this.worlds[l].addEventListener(new ServerWorldEventHandler(this, this.worlds[l]));
			}

			this.getPlayerList().setPlayerManager(this.worlds);
			if (this.worlds[0].getWorldInfo().getDifficulty() == null) {
				this.setDifficultyForAllWorlds(this.mc.gameSettings.difficulty);
			}
		}

		this.initialWorldChunkLoad();
	}

	@Override
	public boolean init() throws IOException {
		LOGGER.info("Starting integrated minecraft server version 1.11.2");
		this.setOnlineMode(true);
		this.setCanSpawnAnimals(true);
		this.setCanSpawnNPCs(true);
		this.setAllowPvp(true);
		this.setAllowFlight(true);
		LOGGER.info("Generating keypair");
		this.setKeyPair(CryptManager.generateKeyPair());
		if (Reflector.FMLCommonHandler_handleServerAboutToStart.exists()) {
			Object object = Reflector.call(Reflector.FMLCommonHandler_instance, new Object[0]);
			if (!Reflector.callBoolean(object, Reflector.FMLCommonHandler_handleServerAboutToStart, new Object[] { this })) {
				return false;
			}
		}

		this.loadAllWorlds(this.getFolderName(), this.getWorldName(), this.theWorldSettings.getSeed(), this.theWorldSettings.getTerrainType(),
				this.theWorldSettings.getGeneratorOptions());
		this.setMOTD(this.getServerOwner() + " - " + this.worlds[0].getWorldInfo().getWorldName());
		if (Reflector.FMLCommonHandler_handleServerStarting.exists()) {
			Object object1 = Reflector.call(Reflector.FMLCommonHandler_instance, new Object[0]);
			if (Reflector.FMLCommonHandler_handleServerStarting.getReturnType() == Boolean.TYPE) {
				return Reflector.callBoolean(object1, Reflector.FMLCommonHandler_handleServerStarting, new Object[] { this });
			}

			Reflector.callVoid(object1, Reflector.FMLCommonHandler_handleServerStarting, new Object[] { this });
		}

		return true;
	}

	@Override
	public void tick() {
		boolean flag = this.isGamePaused;
		this.isGamePaused = Minecraft.getMinecraft().getConnection() != null && Minecraft.getMinecraft().isGamePaused();
		if (!flag && this.isGamePaused) {
			LOGGER.info("Saving and pausing game...");
			this.getPlayerList().saveAllPlayerData();
			this.saveAllWorlds(false);
		}

		if (this.isGamePaused) {
			synchronized (this.futureTaskQueue) {
				while (!this.futureTaskQueue.isEmpty()) {
					Util.runTask((FutureTask) this.futureTaskQueue.poll(), LOGGER);
				}
			}
		} else {
			super.tick();
			if (this.mc.gameSettings.renderDistanceChunks != this.getPlayerList().getViewDistance()) {
				LOGGER.info("Changing view distance to {}, from {}",
						new Object[] { Integer.valueOf(this.mc.gameSettings.renderDistanceChunks), Integer.valueOf(this.getPlayerList().getViewDistance()) });
				this.getPlayerList().setViewDistance(this.mc.gameSettings.renderDistanceChunks);
			}

			if (this.mc.world != null) {
				WorldInfo worldinfo1 = this.worlds[0].getWorldInfo();
				WorldInfo worldinfo = this.mc.world.getWorldInfo();
				if (!worldinfo1.isDifficultyLocked() && worldinfo.getDifficulty() != worldinfo1.getDifficulty()) {
					LOGGER.info("Changing difficulty to {}, from {}", new Object[] { worldinfo.getDifficulty(), worldinfo1.getDifficulty() });
					this.setDifficultyForAllWorlds(worldinfo.getDifficulty());
				} else if (worldinfo.isDifficultyLocked() && !worldinfo1.isDifficultyLocked()) {
					LOGGER.info("Locking difficulty to {}", new Object[] { worldinfo.getDifficulty() });

					for (WorldServer worldserver : this.worlds) {
						if (worldserver != null) {
							worldserver.getWorldInfo().setDifficultyLocked(true);
						}
					}
				}
			}
		}

	}

	@Override
	public boolean canStructuresSpawn() {
		return false;
	}

	@Override
	public GameType getGameType() {
		return this.theWorldSettings.getGameType();
	}

	@Override
	public EnumDifficulty getDifficulty() {
		return this.mc.world == null ? this.mc.gameSettings.difficulty : this.mc.world.getWorldInfo().getDifficulty();
	}

	@Override
	public boolean isHardcore() {
		return this.theWorldSettings.getHardcoreEnabled();
	}

	@Override
	public boolean shouldBroadcastRconToOps() {
		return true;
	}

	@Override
	public boolean shouldBroadcastConsoleToOps() {
		return true;
	}

	@Override
	public void saveAllWorlds(boolean flag) {
		super.saveAllWorlds(flag);
	}

	@Override
	public File getDataDirectory() {
		return this.mc.mcDataDir;
	}

	@Override
	public boolean isDedicatedServer() {
		return false;
	}

	@Override
	public boolean shouldUseNativeTransport() {
		return false;
	}

	@Override
	public void finalTick(CrashReport crashreport) {
		this.mc.crashed(crashreport);
	}

	@Override
	public CrashReport addServerInfoToCrashReport(CrashReport crashreport) {
		crashreport = super.addServerInfoToCrashReport(crashreport);
		crashreport.getCategory().setDetail("Type", new ICrashReportDetail<String>() {
			@Override
			public String call() throws Exception {
				return "Integrated Server (map_client.txt)";
			}
		});
		crashreport.getCategory().setDetail("Is Modded", new ICrashReportDetail<String>() {
			@Override
			public String call() throws Exception {
				String s = ClientBrandRetriever.getClientModName();
				if (!s.equals("vanilla")) {
					return "Definitely; Client brand changed to \'" + s + "\'";
				} else {
					s = IntegratedServer.this.getServerModName();
					return !"vanilla".equals(s) ? "Definitely; Server brand changed to \'" + s + "\'"
							: (Minecraft.class.getSigners() == null ? "Very likely; Jar signature invalidated"
									: "Probably not. Jar signature remains and both client + server brands are untouched.");
				}
			}
		});
		return crashreport;
	}

	@Override
	public void setDifficultyForAllWorlds(EnumDifficulty enumdifficulty) {
		super.setDifficultyForAllWorlds(enumdifficulty);
		if (this.mc.world != null) {
			this.mc.world.getWorldInfo().setDifficulty(enumdifficulty);
		}

	}

	@Override
	public void addServerStatsToSnooper(Snooper snooper) {
		super.addServerStatsToSnooper(snooper);
		snooper.addClientStat("snooper_partner", this.mc.getPlayerUsageSnooper().getUniqueID());
	}

	@Override
	public boolean isSnooperEnabled() {
		return Minecraft.getMinecraft().isSnooperEnabled();
	}

	@Override
	public String shareToLAN(GameType gametype, boolean flag) {
		try {
			int i = -1;

			try {
				i = HttpUtil.getSuitableLanPort();
			} catch (IOException var5) {
				;
			}

			if (i <= 0) {
				i = 25564;
			}

			this.getNetworkSystem().addLanEndpoint((InetAddress) null, i);
			LOGGER.info("Started on {}", new Object[] { Integer.valueOf(i) });
			this.isPublic = true;
			this.lanServerPing = new ThreadLanServerPing(this.getMOTD(), i + "");
			this.lanServerPing.start();
			this.getPlayerList().setGameType(gametype);
			this.getPlayerList().setCommandsAllowedForAll(flag);
			this.mc.player.setPermissionLevel(flag ? 4 : 0);
			return i + "";
		} catch (IOException var6) {
			return null;
		}
	}

	@Override
	public void stopServer() {
		super.stopServer();
		if (this.lanServerPing != null) {
			this.lanServerPing.interrupt();
			this.lanServerPing = null;
		}

	}

	@Override
	public void initiateShutdown() {
		if (!Reflector.MinecraftForge.exists() || this.isServerRunning()) {
			Futures.getUnchecked(this.addScheduledTask(new Runnable() {
				@Override
				public void run() {
					for (EntityPlayerMP entityplayermp : Lists.newArrayList(IntegratedServer.this.getPlayerList().getPlayers())) {
						if (!entityplayermp.getUniqueID().equals(IntegratedServer.this.mc.player.getUniqueID())) {
							IntegratedServer.this.getPlayerList().playerLoggedOut(entityplayermp);
						}
					}

				}
			}));
		}

		super.initiateShutdown();
		if (this.lanServerPing != null) {
			this.lanServerPing.interrupt();
			this.lanServerPing = null;
		}

	}

	public boolean getPublic() {
		return this.isPublic;
	}

	@Override
	public void setGameType(GameType gametype) {
		super.setGameType(gametype);
		this.getPlayerList().setGameType(gametype);
	}

	@Override
	public boolean isCommandBlockEnabled() {
		return true;
	}

	@Override
	public int getOpPermissionLevel() {
		return 4;
	}

	public void reloadLootTables() {
		if (this.isCallingFromMinecraftThread()) {
			this.worlds[0].getLootTableManager().reloadLootTables();
		} else {
			this.addScheduledTask(new Runnable() {
				@Override
				public void run() {
					IntegratedServer.this.reloadLootTables();
				}
			});
		}

	}
}
