package net.minecraft.potion;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.Nullable;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.IAttribute;
import net.minecraft.init.PotionTypes;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.potion.PotionType;
import net.optifine.Config;
import net.optifine.CustomColors;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Tuple;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;

public class PotionUtils {
	public static List<PotionEffect> getEffectsFromStack(ItemStack itemstack) {
		return getEffectsFromTag(itemstack.getTagCompound());
	}

	public static List<PotionEffect> mergeEffects(PotionType potiontype, Collection<PotionEffect> collection) {
		ArrayList arraylist = Lists.newArrayList();
		arraylist.addAll(potiontype.getEffects());
		arraylist.addAll(collection);
		return arraylist;
	}

	public static List<PotionEffect> getEffectsFromTag(@Nullable NBTTagCompound nbttagcompound) {
		ArrayList arraylist = Lists.newArrayList();
		arraylist.addAll(getPotionTypeFromNBT(nbttagcompound).getEffects());
		addCustomPotionEffectToList(nbttagcompound, arraylist);
		return arraylist;
	}

	public static List<PotionEffect> getFullEffectsFromItem(ItemStack itemstack) {
		return getFullEffectsFromTag(itemstack.getTagCompound());
	}

	public static List<PotionEffect> getFullEffectsFromTag(@Nullable NBTTagCompound nbttagcompound) {
		ArrayList arraylist = Lists.newArrayList();
		addCustomPotionEffectToList(nbttagcompound, arraylist);
		return arraylist;
	}

	public static void addCustomPotionEffectToList(@Nullable NBTTagCompound nbttagcompound, List<PotionEffect> list) {
		if (nbttagcompound != null && nbttagcompound.hasKey("CustomPotionEffects", 9)) {
			NBTTagList nbttaglist = nbttagcompound.getTagList("CustomPotionEffects", 10);

			for (int i = 0; i < nbttaglist.tagCount(); ++i) {
				NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
				PotionEffect potioneffect = PotionEffect.readCustomPotionEffectFromNBT(nbttagcompound1);
				if (potioneffect != null) {
					list.add(potioneffect);
				}
			}
		}

	}

	public static int getColor(ItemStack itemstack) {
		NBTTagCompound nbttagcompound = itemstack.getTagCompound();
		return nbttagcompound != null && nbttagcompound.hasKey("CustomPotionColor", 99) ? nbttagcompound.getInteger("CustomPotionColor")
				: (getPotionFromItem(itemstack) == PotionTypes.EMPTY ? 16253176 : getPotionColorFromEffectList(getEffectsFromStack(itemstack)));
	}

	public static int getPotionColor(PotionType potiontype) {
		return potiontype == PotionTypes.EMPTY ? 16253176 : getPotionColorFromEffectList(potiontype.getEffects());
	}

	public static int getPotionColorFromEffectList(Collection<PotionEffect> collection) {
		int i = 3694022;
		if (collection.isEmpty()) {
			return Config.isCustomColors() ? CustomColors.getPotionColor((Potion) null, i) : 3694022;
		} else {
			float f = 0.0F;
			float f1 = 0.0F;
			float f2 = 0.0F;
			int j = 0;

			for (PotionEffect potioneffect : collection) {
				if (potioneffect.doesShowParticles()) {
					int k = potioneffect.getPotion().getLiquidColor();
					if (Config.isCustomColors()) {
						k = CustomColors.getPotionColor(potioneffect.getPotion(), k);
					}

					int l = potioneffect.getAmplifier() + 1;
					f += l * (k >> 16 & 255) / 255.0F;
					f1 += l * (k >> 8 & 255) / 255.0F;
					f2 += l * (k >> 0 & 255) / 255.0F;
					j += l;
				}
			}

			if (j == 0) {
				return 0;
			} else {
				f = f / j * 255.0F;
				f1 = f1 / j * 255.0F;
				f2 = f2 / j * 255.0F;
				return (int) f << 16 | (int) f1 << 8 | (int) f2;
			}
		}
	}

	public static PotionType getPotionFromItem(ItemStack itemstack) {
		return getPotionTypeFromNBT(itemstack.getTagCompound());
	}

	public static PotionType getPotionTypeFromNBT(@Nullable NBTTagCompound nbttagcompound) {
		return nbttagcompound == null ? PotionTypes.EMPTY : PotionType.getPotionTypeForName(nbttagcompound.getString("Potion"));
	}

	public static ItemStack addPotionToItemStack(ItemStack itemstack, PotionType potiontype) {
		ResourceLocation resourcelocation = PotionType.REGISTRY.getNameForObject(potiontype);
		if (potiontype == PotionTypes.EMPTY) {
			if (itemstack.hasTagCompound()) {
				NBTTagCompound nbttagcompound = itemstack.getTagCompound();
				nbttagcompound.removeTag("Potion");
				if (nbttagcompound.hasNoTags()) {
					itemstack.setTagCompound((NBTTagCompound) null);
				}
			}
		} else {
			NBTTagCompound nbttagcompound1 = itemstack.hasTagCompound() ? itemstack.getTagCompound() : new NBTTagCompound();
			nbttagcompound1.setString("Potion", resourcelocation.toString());
			itemstack.setTagCompound(nbttagcompound1);
		}

		return itemstack;
	}

	public static ItemStack appendEffects(ItemStack itemstack, Collection<PotionEffect> collection) {
		if (collection.isEmpty()) {
			return itemstack;
		} else {
			NBTTagCompound nbttagcompound = Objects.firstNonNull(itemstack.getTagCompound(), new NBTTagCompound());
			NBTTagList nbttaglist = nbttagcompound.getTagList("CustomPotionEffects", 9);

			for (PotionEffect potioneffect : collection) {
				nbttaglist.appendTag(potioneffect.writeCustomPotionEffectToNBT(new NBTTagCompound()));
			}

			nbttagcompound.setTag("CustomPotionEffects", nbttaglist);
			itemstack.setTagCompound(nbttagcompound);
			return itemstack;
		}
	}

	public static void addPotionTooltip(ItemStack itemIn, List<String> lores, float durationFactor) {
		List<PotionEffect> list = getEffectsFromStack(itemIn);
		List<Tuple<String, AttributeModifier>> list1 = Lists.<Tuple<String, AttributeModifier>> newArrayList();

		if (list.isEmpty()) {
			String s = I18n.translateToLocal("effect.none").trim();
			lores.add(TextFormatting.GRAY + s);
		} else {
			for (PotionEffect potioneffect : list) {
				String s1 = I18n.translateToLocal(potioneffect.getEffectName()).trim();
				Potion potion = potioneffect.getPotion();
				Map<IAttribute, AttributeModifier> map = potion.getAttributeModifierMap();

				if (!map.isEmpty()) {
					for (Entry<IAttribute, AttributeModifier> entry : map.entrySet()) {
						AttributeModifier attributemodifier = entry.getValue();
						AttributeModifier attributemodifier1 = new AttributeModifier(attributemodifier.getName(),
								potion.getAttributeModifierAmount(potioneffect.getAmplifier(), attributemodifier), attributemodifier.getOperation());
						list1.add(new Tuple(entry.getKey().getName(), attributemodifier1));
					}
				}

				if (potioneffect.getAmplifier() > 0) {
					s1 = s1 + " " + I18n.translateToLocal("potion.potency." + potioneffect.getAmplifier()).trim();
				}

				if (potioneffect.getDuration() > 20) {
					s1 = s1 + " (" + Potion.getPotionDurationString(potioneffect, durationFactor) + ")";
				}

				if (potion.isBadEffect()) {
					lores.add(TextFormatting.RED + s1);
				} else {
					lores.add(TextFormatting.BLUE + s1);
				}
			}
		}

		if (!list1.isEmpty()) {
			lores.add("");
			lores.add(TextFormatting.DARK_PURPLE + I18n.translateToLocal("potion.whenDrank"));

			for (Tuple<String, AttributeModifier> tuple : list1) {
				AttributeModifier attributemodifier2 = tuple.getSecond();
				double d0 = attributemodifier2.getAmount();
				double d1;

				if (attributemodifier2.getOperation() != 1 && attributemodifier2.getOperation() != 2) {
					d1 = attributemodifier2.getAmount();
				} else {
					d1 = attributemodifier2.getAmount() * 100.0D;
				}

				if (d0 > 0.0D) {
					lores.add(TextFormatting.BLUE + I18n.translateToLocalFormatted("attribute.modifier.plus." + attributemodifier2.getOperation(),
							new Object[] { ItemStack.DECIMALFORMAT.format(d1), I18n.translateToLocal("attribute.name." + tuple.getFirst()) }));
				} else if (d0 < 0.0D) {
					d1 = d1 * -1.0D;
					lores.add(TextFormatting.RED + I18n.translateToLocalFormatted("attribute.modifier.take." + attributemodifier2.getOperation(),
							new Object[] { ItemStack.DECIMALFORMAT.format(d1), I18n.translateToLocal("attribute.name." + tuple.getFirst()) }));
				}
			}
		}
	}
}
