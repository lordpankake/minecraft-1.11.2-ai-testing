package net.minecraft.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.IntBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.imageio.ImageIO;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.client.shader.Framebuffer;
import net.optifine.Config;
import net.optifine.Reflector;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.event.ClickEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.BufferUtils;

public class ScreenShotHelper {
	private static final Logger LOGGER = LogManager.getLogger();
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss");
	private static IntBuffer pixelBuffer;
	private static int[] pixelValues;

	public static ITextComponent saveScreenshot(File file1, int i, int j, Framebuffer framebuffer) {
		return saveScreenshot(file1, (String) null, i, j, framebuffer);
	}

	public static ITextComponent saveScreenshot(File file1, String s, int i, int j, Framebuffer framebuffer) {
		try {
			File file2 = new File(file1, "screenshots");
			file2.mkdir();
			Minecraft minecraft = Minecraft.getMinecraft();
			int k = Config.getGameSettings().guiScale;
			ScaledResolution scaledresolution = new ScaledResolution(minecraft);
			int l = scaledresolution.getScaleFactor();
			int i1 = Config.getScreenshotSize();
			boolean flag = OpenGlHelper.isFramebufferEnabled() && i1 > 1;
			if (flag) {
				Config.getGameSettings().guiScale = l * i1;
				resize(i * i1, j * i1);
				GlStateManager.pushMatrix();
				GlStateManager.clear(16640);
				minecraft.getFramebuffer().bindFramebuffer(true);
				minecraft.entityRenderer.updateCameraAndRender(minecraft.getRenderPartialTicks(), System.nanoTime());
			}

			BufferedImage bufferedimage = createScreenshot(i, j, framebuffer);
			if (flag) {
				minecraft.getFramebuffer().unbindFramebuffer();
				GlStateManager.popMatrix();
				Config.getGameSettings().guiScale = k;
				resize(i, j);
			}

			File file3;
			if (s == null) {
				file3 = getTimestampedPNGFileForDirectory(file2);
			} else {
				file3 = new File(file2, s);
			}

			file3 = file3.getCanonicalFile();
			Object object = null;
			if (Reflector.ForgeHooksClient_onScreenshot.exists()) {
				object = Reflector.call(Reflector.ForgeHooksClient_onScreenshot, new Object[] { bufferedimage, file3 });
				if (Reflector.callBoolean(object, Reflector.Event_isCanceled, new Object[0])) {
					return (ITextComponent) Reflector.call(object, Reflector.ScreenshotEvent_getCancelMessage, new Object[0]);
				}

				file3 = (File) Reflector.call(object, Reflector.ScreenshotEvent_getScreenshotFile, new Object[0]);
			}

			ImageIO.write(bufferedimage, "png", file3);
			TextComponentString textcomponentstring = new TextComponentString(file3.getName());
			textcomponentstring.getStyle().setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_FILE, file3.getAbsolutePath()));
			textcomponentstring.getStyle().setUnderlined(Boolean.valueOf(true));
			if (object != null) {
				ITextComponent itextcomponent = (ITextComponent) Reflector.call(object, Reflector.ScreenshotEvent_getResultMessage, new Object[0]);
				if (itextcomponent != null) {
					return itextcomponent;
				}
			}

			return new TextComponentTranslation("screenshot.success", new Object[] { textcomponentstring });
		} catch (Exception exception) {
			LOGGER.warn("Couldn\'t save screenshot", exception);
			return new TextComponentTranslation("screenshot.failure", new Object[] { exception.getMessage() });
		}
	}

	public static BufferedImage createScreenshot(int i, int j, Framebuffer framebuffer) {
		if (OpenGlHelper.isFramebufferEnabled()) {
			i = framebuffer.framebufferTextureWidth;
			j = framebuffer.framebufferTextureHeight;
		}

		int k = i * j;
		if (pixelBuffer == null || pixelBuffer.capacity() < k) {
			pixelBuffer = BufferUtils.createIntBuffer(k);
			pixelValues = new int[k];
		}

		GlStateManager.glPixelStorei(3333, 1);
		GlStateManager.glPixelStorei(3317, 1);
		pixelBuffer.clear();
		if (OpenGlHelper.isFramebufferEnabled()) {
			GlStateManager.bindTexture(framebuffer.framebufferTexture);
			GlStateManager.glGetTexImage(3553, 0, '\u80e1', '\u8367', pixelBuffer);
		} else {
			GlStateManager.glReadPixels(0, 0, i, j, '\u80e1', '\u8367', pixelBuffer);
		}

		pixelBuffer.get(pixelValues);
		TextureUtil.processPixelValues(pixelValues, i, j);
		BufferedImage bufferedimage = new BufferedImage(i, j, 1);
		bufferedimage.setRGB(0, 0, i, j, pixelValues, 0, i);
		return bufferedimage;
	}

	private static File getTimestampedPNGFileForDirectory(File file1) {
		String s = DATE_FORMAT.format(new Date()).toString();
		int i = 1;

		while (true) {
			File file2 = new File(file1, s + (i == 1 ? "" : "_" + i) + ".png");
			if (!file2.exists()) {
				return file2;
			}

			++i;
		}
	}

	private static void resize(int i, int j) {
		Minecraft minecraft = Minecraft.getMinecraft();
		minecraft.displayWidth = Math.max(1, i);
		minecraft.displayHeight = Math.max(1, j);
		if (minecraft.currentScreen != null) {
			ScaledResolution scaledresolution = new ScaledResolution(minecraft);
			minecraft.currentScreen.onResize(minecraft, scaledresolution.getScaledWidth(), scaledresolution.getScaledHeight());
		}

		updateFramebufferSize();
	}

	private static void updateFramebufferSize() {
		Minecraft minecraft = Minecraft.getMinecraft();
		minecraft.getFramebuffer().createBindFramebuffer(minecraft.displayWidth, minecraft.displayHeight);
		if (minecraft.entityRenderer != null) {
			minecraft.entityRenderer.updateShaderGroupSize(minecraft.displayWidth, minecraft.displayHeight);
		}

	}
}
