package net.minecraft.util;

import javax.annotation.Nullable;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.text.ITextComponent;

public class CombatEntry {
	private final DamageSource damageSrc;
	private final float damage;
	private final String fallSuffix;
	private final float fallDistance;

	public CombatEntry(DamageSource damageSrcIn, int timeIn, float healthAmount, float damageAmount, String fallSuffixIn, float fallDistanceIn) {
		this.damageSrc = damageSrcIn;
		this.damage = damageAmount;
		this.fallSuffix = fallSuffixIn;
		this.fallDistance = fallDistanceIn;
	}

	/**
	 * Get the DamageSource of the CombatEntry instance.
	 */
	public DamageSource getDamageSrc() {
		return this.damageSrc;
	}

	public float getDamage() {
		return this.damage;
	}

	/**
	 * Returns true if {@link net.minecraft.util.DamageSource#getEntity() damage
	 * source} is a living entity
	 */
	public boolean isLivingDamageSrc() {
		return this.damageSrc.getEntity() instanceof EntityLivingBase;
	}

	@Nullable
	public String getFallSuffix() {
		return this.fallSuffix;
	}

	@Nullable
	public ITextComponent getDamageSrcDisplayName() {
		return this.getDamageSrc().getEntity() == null ? null : this.getDamageSrc().getEntity().getDisplayName();
	}

	public float getDamageAmount() {
		return this.damageSrc == DamageSource.OUT_OF_WORLD ? Float.MAX_VALUE : this.fallDistance;
	}
}
