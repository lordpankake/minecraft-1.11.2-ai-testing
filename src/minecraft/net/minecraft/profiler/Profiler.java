package net.minecraft.profiler;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import net.minecraft.client.renderer.GlStateManager;
import net.optifine.Config;
import net.optifine.Lagometer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Profiler {
	private static final Logger LOGGER = LogManager.getLogger();
	private final List<String> sectionList = Lists.newArrayList();
	private final List<Long> timestampList = Lists.newArrayList();
	public boolean profilingEnabled;
	private String profilingSection = "";
	private final Map<String, Long> profilingMap = Maps.newHashMap();
	public boolean profilerGlobalEnabled = true;
	private boolean profilerLocalEnabled;
	private static final int HASH_SCHEDULED_EXECUTABLES = "scheduledExecutables".hashCode();
	private static final int HASH_TICK = "tick".hashCode();
	private static final int HASH_PRE_RENDER_ERRORS = "preRenderErrors".hashCode();
	private static final int HASH_RENDER = "render".hashCode();
	private static final int HASH_DISPLAY = "display".hashCode();

	public Profiler() {
		this.profilerLocalEnabled = this.profilerGlobalEnabled;
	}

	public void clearProfiling() {
		this.profilingMap.clear();
		this.profilingSection = "";
		this.sectionList.clear();
		this.profilerLocalEnabled = this.profilerGlobalEnabled;
	}

	public void startSection(String s) {
		if (Lagometer.isActive()) {
			int i = s.hashCode();
			if (i == HASH_SCHEDULED_EXECUTABLES && s.equals("scheduledExecutables")) {
				Lagometer.timerScheduledExecutables.start();
			} else if (i == HASH_TICK && s.equals("tick") && Config.isMinecraftThread()) {
				Lagometer.timerScheduledExecutables.end();
				Lagometer.timerTick.start();
			} else if (i == HASH_PRE_RENDER_ERRORS && s.equals("preRenderErrors")) {
				Lagometer.timerTick.end();
			}
		}

		if (Config.isFastRender()) {
			int j = s.hashCode();
			if (j == HASH_RENDER && s.equals("render")) {
				GlStateManager.clearEnabled = false;
			} else if (j == HASH_DISPLAY && s.equals("display")) {
				GlStateManager.clearEnabled = true;
			}
		}

		if (this.profilerLocalEnabled) {
			if (this.profilingEnabled) {
				if (this.profilingSection.length() > 0) {
					this.profilingSection = this.profilingSection + ".";
				}

				this.profilingSection = this.profilingSection + s;
				this.sectionList.add(this.profilingSection);
				this.timestampList.add(Long.valueOf(System.nanoTime()));
			}

		}
	}

	public void endSection() {
		if (this.profilerLocalEnabled) {
			if (this.profilingEnabled) {
				long i = System.nanoTime();
				long j = this.timestampList.remove(this.timestampList.size() - 1).longValue();
				this.sectionList.remove(this.sectionList.size() - 1);
				long k = i - j;
				if (this.profilingMap.containsKey(this.profilingSection)) {
					this.profilingMap.put(this.profilingSection, Long.valueOf(this.profilingMap.get(this.profilingSection).longValue() + k));
				} else {
					this.profilingMap.put(this.profilingSection, Long.valueOf(k));
				}

				if (k > 100000000L) {
					LOGGER.warn("Something\'s taking too long! \'{}\' took aprox {} ms", new Object[] { this.profilingSection, Double.valueOf(k / 1000000.0D) });
				}

				this.profilingSection = this.sectionList.isEmpty() ? "" : (String) this.sectionList.get(this.sectionList.size() - 1);
			}

		}
	}

	public List<Profiler.Result> getProfilingData(String s) {
		if (!this.profilingEnabled) {
			return Collections.<Profiler.Result> emptyList();
		} else {
			long i = this.profilingMap.containsKey("root") ? this.profilingMap.get("root").longValue() : 0L;
			long j = this.profilingMap.containsKey(s) ? this.profilingMap.get(s).longValue() : -1L;
			ArrayList arraylist = Lists.newArrayList();
			if (s.length() > 0) {
				s = s + ".";
			}

			long k = 0L;

			for (String s1 : this.profilingMap.keySet()) {
				if (s1.length() > s.length() && s1.startsWith(s) && s1.indexOf(".", s.length() + 1) < 0) {
					k += this.profilingMap.get(s1).longValue();
				}
			}

			float f = k;
			if (k < j) {
				k = j;
			}

			if (i < k) {
				i = k;
			}

			for (String s2 : this.profilingMap.keySet()) {
				if (s2.length() > s.length() && s2.startsWith(s) && s2.indexOf(".", s.length() + 1) < 0) {
					long l = this.profilingMap.get(s2).longValue();
					double d0 = l * 100.0D / k;
					double d1 = l * 100.0D / i;
					String s3 = s2.substring(s.length());
					arraylist.add(new Profiler.Result(s3, d0, d1));
				}
			}

			for (String s4 : this.profilingMap.keySet()) {
				this.profilingMap.put(s4, Long.valueOf(this.profilingMap.get(s4).longValue() * 950L / 1000L));
			}

			if (k > f) {
				arraylist.add(new Profiler.Result("unspecified", (k - f) * 100.0D / k, (k - f) * 100.0D / i));
			}

			Collections.sort(arraylist);
			arraylist.add(0, new Profiler.Result(s, 100.0D, k * 100.0D / i));
			return arraylist;
		}
	}

	public void endStartSection(String s) {
		if (this.profilerLocalEnabled) {
			this.endSection();
			this.startSection(s);
		}
	}

	public String getNameOfLastSection() {
		return this.sectionList.size() == 0 ? "[UNKNOWN]" : (String) this.sectionList.get(this.sectionList.size() - 1);
	}

	public static final class Result implements Comparable<Profiler.Result> {
		public double usePercentage;
		public double totalUsePercentage;
		public String profilerName;

		public Result(String s, double d0, double d1) {
			this.profilerName = s;
			this.usePercentage = d0;
			this.totalUsePercentage = d1;
		}

		@Override
		public int compareTo(Profiler.Result profiler$result) {
			return profiler$result.usePercentage < this.usePercentage ? -1
					: (profiler$result.usePercentage > this.usePercentage ? 1 : profiler$result.profilerName.compareTo(this.profilerName));
		}

		public int getColor() {
			return (this.profilerName.hashCode() & 11184810) + 4473924;
		}
	}
}
