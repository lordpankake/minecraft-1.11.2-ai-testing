package net.minecraft.entity;

import com.google.common.collect.Maps;

import me.lpk.ai.AIUser;
import me.lpk.ai.IJumpHelper;
import me.lpk.ai.IMoveHelper;

import java.util.Arrays;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import javax.annotation.Nullable;
import net.minecraft.block.state.IBlockState;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityBodyHelper;
import net.minecraft.entity.EntityHanging;
import net.minecraft.entity.EntityLeashKnot;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAITasks;
import net.minecraft.entity.ai.EntityJumpHelper;
import net.minecraft.entity.ai.EntityLookHelper;
import net.minecraft.entity.ai.EntityMoveHelper;
import net.minecraft.entity.ai.EntitySenses;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.item.EntityBoat;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityGhast;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagFloat;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.network.play.server.SPacketEntityAttach;
import net.minecraft.pathfinding.PathNavigate;
import net.minecraft.pathfinding.PathNavigateGround;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.scoreboard.Team;
import net.optifine.Config;
import net.optifine.Reflector;
import net.minecraft.stats.AchievementList;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.datafix.DataFixer;
import net.minecraft.util.datafix.FixTypes;
import net.minecraft.util.datafix.walkers.ItemStackDataLists;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.storage.loot.LootContext;
import net.minecraft.world.storage.loot.LootTable;

public abstract class EntityLiving extends EntityLivingBase implements AIUser {
	private static final DataParameter<Byte> AI_FLAGS = EntityDataManager.<Byte> createKey(EntityLiving.class, DataSerializers.BYTE);
	public int livingSoundTime;
	protected int experienceValue;
	private final EntityLookHelper lookHelper;
	protected EntityMoveHelper moveHelper;
	protected EntityJumpHelper jumpHelper;
	private final EntityBodyHelper bodyHelper;
	protected PathNavigate navigator;
	protected final EntityAITasks tasks;
	protected final EntityAITasks targetTasks;
	private EntityLivingBase attackTarget;
	private final EntitySenses senses;
	private final NonNullList<ItemStack> inventoryHands = NonNullList.<ItemStack> withSize(2, ItemStack.EMPTY);
	protected float[] inventoryHandsDropChances = new float[2];
	private final NonNullList<ItemStack> inventoryArmor = NonNullList.<ItemStack> withSize(4, ItemStack.EMPTY);
	protected float[] inventoryArmorDropChances = new float[4];
	private boolean canPickUpLoot;
	private boolean persistenceRequired;
	private final Map<PathNodeType, Float> mapPathPriority = Maps.newEnumMap(PathNodeType.class);
	private ResourceLocation deathLootTable;
	private long deathLootTableSeed;
	private boolean isLeashed;
	private Entity leashedToEntity;
	private NBTTagCompound leashNBTTag;
	public int randomMobsId = 0;
	public Biome spawnBiome = null;
	public BlockPos spawnPosition = null;
	private UUID teamUuid = null;
	private String teamUuidString = null;

	public EntityLiving(World world) {
		super(world);
		this.tasks = new EntityAITasks(world != null && world.theProfiler != null ? world.theProfiler : null);
		this.targetTasks = new EntityAITasks(world != null && world.theProfiler != null ? world.theProfiler : null);
		this.lookHelper = new EntityLookHelper(this);
		this.moveHelper = new EntityMoveHelper(this);
		this.jumpHelper = new EntityJumpHelper(this);
		this.bodyHelper = this.createBodyHelper();
		this.navigator = this.createNavigator(world);
		this.senses = new EntitySenses(this);
		Arrays.fill(this.inventoryArmorDropChances, 0.085F);
		Arrays.fill(this.inventoryHandsDropChances, 0.085F);
		if (world != null && !world.isRemote) {
			this.initEntityAI();
		}

		UUID uuid = this.getUniqueID();
		long i = uuid.getLeastSignificantBits();
		this.randomMobsId = (int) (i & 2147483647L);
	}

	protected void initEntityAI() {
	}

	@Override
	protected void applyEntityAttributes() {
		super.applyEntityAttributes();
		this.getAttributeMap().registerAttribute(SharedMonsterAttributes.FOLLOW_RANGE).setBaseValue(16.0D);
	}

	protected PathNavigate createNavigator(World world) {
		return new PathNavigateGround(this, world);
	}

	public float getPathPriority(PathNodeType pathnodetype) {
		Float f = this.mapPathPriority.get(pathnodetype);
		return f == null ? pathnodetype.getPriority() : f.floatValue();
	}

	public void setPathPriority(PathNodeType pathnodetype, float f) {
		this.mapPathPriority.put(pathnodetype, Float.valueOf(f));
	}

	protected EntityBodyHelper createBodyHelper() {
		return new EntityBodyHelper(this);
	}

	public EntityLookHelper getLookHelper() {
		return this.lookHelper;
	}

	public IMoveHelper getMoveHelper() {
		return this.moveHelper;
	}

	public IJumpHelper getJumpHelper() {
		return this.jumpHelper;
	}

	public PathNavigate getNavigator() {
		return this.navigator;
	}

	public EntitySenses getEntitySenses() {
		return this.senses;
	}

	@Nullable
	public EntityLivingBase getAttackTarget() {
		return this.attackTarget;
	}

	public void setAttackTarget(@Nullable EntityLivingBase entitylivingbase) {
		this.attackTarget = entitylivingbase;
		Reflector.callVoid(Reflector.ForgeHooks_onLivingSetAttackTarget, new Object[] { this, entitylivingbase });
	}

	public boolean canAttackClass(Class<? extends EntityLivingBase> oclass) {
		return oclass != EntityGhast.class;
	}

	public void eatGrassBonus() {
	}

	@Override
	protected void entityInit() {
		super.entityInit();
		this.dataManager.register(AI_FLAGS, Byte.valueOf((byte) 0));
	}

	public int getTalkInterval() {
		return 80;
	}

	public void playLivingSound() {
		SoundEvent soundevent = this.getAmbientSound();
		if (soundevent != null) {
			this.playSound(soundevent, this.getSoundVolume(), this.getSoundPitch());
		}

	}

	@Override
	public void onEntityUpdate() {
		super.onEntityUpdate();
		this.world.theProfiler.startSection("mobBaseTick");
		if (this.isEntityAlive() && this.rand.nextInt(1000) < this.livingSoundTime++) {
			this.applyEntityAI();
			this.playLivingSound();
		}

		this.world.theProfiler.endSection();
	}

	@Override
	protected void playHurtSound(DamageSource damagesource) {
		this.applyEntityAI();
		super.playHurtSound(damagesource);
	}

	private void applyEntityAI() {
		this.livingSoundTime = -this.getTalkInterval();
	}

	@Override
	protected int getExperiencePoints(EntityPlayer var1) {
		if (this.experienceValue > 0) {
			int i = this.experienceValue;

			for (int j = 0; j < this.inventoryArmor.size(); ++j) {
				if (!this.inventoryArmor.get(j).isEmpty() && this.inventoryArmorDropChances[j] <= 1.0F) {
					i += 1 + this.rand.nextInt(3);
				}
			}

			for (int k = 0; k < this.inventoryHands.size(); ++k) {
				if (!this.inventoryHands.get(k).isEmpty() && this.inventoryHandsDropChances[k] <= 1.0F) {
					i += 1 + this.rand.nextInt(3);
				}
			}

			return i;
		} else {
			return this.experienceValue;
		}
	}

	public void spawnExplosionParticle() {
		if (this.world.isRemote) {
			for (int i = 0; i < 20; ++i) {
				double d0 = this.rand.nextGaussian() * 0.02D;
				double d1 = this.rand.nextGaussian() * 0.02D;
				double d2 = this.rand.nextGaussian() * 0.02D;
				double d3 = 10.0D;
				this.world.spawnParticle(EnumParticleTypes.EXPLOSION_NORMAL, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width - d0 * 10.0D,
						this.posY + this.rand.nextFloat() * this.height - d1 * 10.0D, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width - d2 * 10.0D, d0, d1, d2,
						new int[0]);
			}
		} else {
			this.world.setEntityState(this, (byte) 20);
		}

	}

	@Override
	public void handleStatusUpdate(byte b0) {
		if (b0 == 20) {
			this.spawnExplosionParticle();
		} else {
			super.handleStatusUpdate(b0);
		}

	}

	@Override
	public void onUpdate() {
		if (Config.isSmoothWorld() && this.canSkipUpdate()) {
			this.onUpdateMinimal();
		} else {
			super.onUpdate();
			if (!this.world.isRemote) {
				this.updateLeashedState();
				if (this.ticksExisted % 5 == 0) {
					boolean flag = !(this.getControllingPassenger() instanceof EntityLiving);
					boolean flag1 = !(this.getRidingEntity() instanceof EntityBoat);
					this.tasks.setControlFlag(1, flag);
					this.tasks.setControlFlag(4, flag && flag1);
					this.tasks.setControlFlag(2, flag);
				}
			}

		}
	}

	@Override
	protected float updateDistance(float var1, float f) {
		this.bodyHelper.updateRenderAngles();
		return f;
	}

	@Nullable
	protected SoundEvent getAmbientSound() {
		return null;
	}

	@Nullable
	protected Item getDropItem() {
		return null;
	}

	@Override
	protected void dropFewItems(boolean var1, int i) {
		Item item = this.getDropItem();
		if (item != null) {
			int j = this.rand.nextInt(3);
			if (i > 0) {
				j += this.rand.nextInt(i + 1);
			}

			for (int k = 0; k < j; ++k) {
				this.dropItem(item, 1);
			}
		}

	}

	public static void registerFixesMob(DataFixer datafixer, Class<?> oclass) {
		datafixer.registerWalker(FixTypes.ENTITY, new ItemStackDataLists(oclass, new String[] { "ArmorItems", "HandItems" }));
	}

	@Override
	public void writeEntityToNBT(NBTTagCompound nbttagcompound) {
		super.writeEntityToNBT(nbttagcompound);
		nbttagcompound.setBoolean("CanPickUpLoot", this.canPickUpLoot());
		nbttagcompound.setBoolean("PersistenceRequired", this.persistenceRequired);
		NBTTagList nbttaglist = new NBTTagList();

		for (ItemStack itemstack : this.inventoryArmor) {
			NBTTagCompound nbttagcompound1 = new NBTTagCompound();
			if (!itemstack.isEmpty()) {
				itemstack.writeToNBT(nbttagcompound1);
			}

			nbttaglist.appendTag(nbttagcompound1);
		}

		nbttagcompound.setTag("ArmorItems", nbttaglist);
		NBTTagList nbttaglist1 = new NBTTagList();

		for (ItemStack itemstack1 : this.inventoryHands) {
			NBTTagCompound nbttagcompound2 = new NBTTagCompound();
			if (!itemstack1.isEmpty()) {
				itemstack1.writeToNBT(nbttagcompound2);
			}

			nbttaglist1.appendTag(nbttagcompound2);
		}

		nbttagcompound.setTag("HandItems", nbttaglist1);
		NBTTagList nbttaglist2 = new NBTTagList();

		for (float f : this.inventoryArmorDropChances) {
			nbttaglist2.appendTag(new NBTTagFloat(f));
		}

		nbttagcompound.setTag("ArmorDropChances", nbttaglist2);
		NBTTagList nbttaglist3 = new NBTTagList();

		for (float f1 : this.inventoryHandsDropChances) {
			nbttaglist3.appendTag(new NBTTagFloat(f1));
		}

		nbttagcompound.setTag("HandDropChances", nbttaglist3);
		nbttagcompound.setBoolean("Leashed", this.isLeashed);
		if (this.leashedToEntity != null) {
			NBTTagCompound nbttagcompound3 = new NBTTagCompound();
			if (this.leashedToEntity instanceof EntityLivingBase) {
				UUID uuid = this.leashedToEntity.getUniqueID();
				nbttagcompound3.setUniqueId("UUID", uuid);
			} else if (this.leashedToEntity instanceof EntityHanging) {
				BlockPos blockpos = ((EntityHanging) this.leashedToEntity).getHangingPosition();
				nbttagcompound3.setInteger("X", blockpos.getX());
				nbttagcompound3.setInteger("Y", blockpos.getY());
				nbttagcompound3.setInteger("Z", blockpos.getZ());
			}

			nbttagcompound.setTag("Leash", nbttagcompound3);
		}

		nbttagcompound.setBoolean("LeftHanded", this.isLeftHanded());
		if (this.deathLootTable != null) {
			nbttagcompound.setString("DeathLootTable", this.deathLootTable.toString());
			if (this.deathLootTableSeed != 0L) {
				nbttagcompound.setLong("DeathLootTableSeed", this.deathLootTableSeed);
			}
		}

		if (this.isAIDisabled()) {
			nbttagcompound.setBoolean("NoAI", this.isAIDisabled());
		}

	}

	@Override
	public void readEntityFromNBT(NBTTagCompound nbttagcompound) {
		super.readEntityFromNBT(nbttagcompound);
		if (nbttagcompound.hasKey("CanPickUpLoot", 1)) {
			this.setCanPickUpLoot(nbttagcompound.getBoolean("CanPickUpLoot"));
		}

		this.persistenceRequired = nbttagcompound.getBoolean("PersistenceRequired");
		if (nbttagcompound.hasKey("ArmorItems", 9)) {
			NBTTagList nbttaglist = nbttagcompound.getTagList("ArmorItems", 10);

			for (int i = 0; i < this.inventoryArmor.size(); ++i) {
				this.inventoryArmor.set(i, new ItemStack(nbttaglist.getCompoundTagAt(i)));
			}
		}

		if (nbttagcompound.hasKey("HandItems", 9)) {
			NBTTagList nbttaglist1 = nbttagcompound.getTagList("HandItems", 10);

			for (int j = 0; j < this.inventoryHands.size(); ++j) {
				this.inventoryHands.set(j, new ItemStack(nbttaglist1.getCompoundTagAt(j)));
			}
		}

		if (nbttagcompound.hasKey("ArmorDropChances", 9)) {
			NBTTagList nbttaglist2 = nbttagcompound.getTagList("ArmorDropChances", 5);

			for (int k = 0; k < nbttaglist2.tagCount(); ++k) {
				this.inventoryArmorDropChances[k] = nbttaglist2.getFloatAt(k);
			}
		}

		if (nbttagcompound.hasKey("HandDropChances", 9)) {
			NBTTagList nbttaglist3 = nbttagcompound.getTagList("HandDropChances", 5);

			for (int l = 0; l < nbttaglist3.tagCount(); ++l) {
				this.inventoryHandsDropChances[l] = nbttaglist3.getFloatAt(l);
			}
		}

		this.isLeashed = nbttagcompound.getBoolean("Leashed");
		if (this.isLeashed && nbttagcompound.hasKey("Leash", 10)) {
			this.leashNBTTag = nbttagcompound.getCompoundTag("Leash");
		}

		this.setLeftHanded(nbttagcompound.getBoolean("LeftHanded"));
		if (nbttagcompound.hasKey("DeathLootTable", 8)) {
			this.deathLootTable = new ResourceLocation(nbttagcompound.getString("DeathLootTable"));
			this.deathLootTableSeed = nbttagcompound.getLong("DeathLootTableSeed");
		}

		this.setNoAI(nbttagcompound.getBoolean("NoAI"));
	}

	@Nullable
	protected ResourceLocation getLootTable() {
		return null;
	}

	@Override
	protected void dropLoot(boolean flag, int i, DamageSource damagesource) {
		ResourceLocation resourcelocation = this.deathLootTable;
		if (resourcelocation == null) {
			resourcelocation = this.getLootTable();
		}

		if (resourcelocation != null) {
			LootTable loottable = this.world.getLootTableManager().getLootTableFromLocation(resourcelocation);
			this.deathLootTable = null;
			LootContext.Builder lootcontext$builder = (new LootContext.Builder((WorldServer) this.world)).withLootedEntity(this).withDamageSource(damagesource);
			if (flag && this.attackingPlayer != null) {
				lootcontext$builder = lootcontext$builder.withPlayer(this.attackingPlayer).withLuck(this.attackingPlayer.getLuck());
			}

			for (ItemStack itemstack : loottable.generateLootForPools(this.deathLootTableSeed == 0L ? this.rand : new Random(this.deathLootTableSeed),
					lootcontext$builder.build())) {
				this.entityDropItem(itemstack, 0.0F);
			}

			this.dropEquipment(flag, i);
		} else {
			super.dropLoot(flag, i, damagesource);
		}

	}

	@Override
	public void setMoveForward(float f) {
		this.moveForward = f;
	}

	@Override
	public void setMoveStrafing(float f) {
		this.moveStrafing = f;
	}

	@Override
	public void setAIMoveSpeed(float f) {
		super.setAIMoveSpeed(f);
		this.setMoveForward(f);
	}

	@Override
	public void onLivingUpdate() {
		super.onLivingUpdate();
		this.world.theProfiler.startSection("looting");
		if (!this.world.isRemote && this.canPickUpLoot() && !this.dead && this.world.getGameRules().getBoolean("mobGriefing")) {
			for (EntityItem entityitem : this.world.getEntitiesWithinAABB(EntityItem.class, this.getEntityBoundingBox().expand(1.0D, 0.0D, 1.0D))) {
				if (!entityitem.isDead && !entityitem.getEntityItem().isEmpty() && !entityitem.cannotPickup()) {
					this.updateEquipmentIfNeeded(entityitem);
				}
			}
		}

		this.world.theProfiler.endSection();
	}

	protected void updateEquipmentIfNeeded(EntityItem entityitem) {
		ItemStack itemstack = entityitem.getEntityItem();
		EntityEquipmentSlot entityequipmentslot = getSlotForItemStack(itemstack);
		boolean flag = true;
		ItemStack itemstack1 = this.getItemStackFromSlot(entityequipmentslot);
		if (!itemstack1.isEmpty()) {
			if (entityequipmentslot.getSlotType() == EntityEquipmentSlot.Type.HAND) {
				if (itemstack.getItem() instanceof ItemSword && !(itemstack1.getItem() instanceof ItemSword)) {
					flag = true;
				} else if (itemstack.getItem() instanceof ItemSword && itemstack1.getItem() instanceof ItemSword) {
					ItemSword itemsword = (ItemSword) itemstack.getItem();
					ItemSword itemsword1 = (ItemSword) itemstack1.getItem();
					if (itemsword.getDamageVsEntity() == itemsword1.getDamageVsEntity()) {
						flag = itemstack.getMetadata() > itemstack1.getMetadata() || itemstack.hasTagCompound() && !itemstack1.hasTagCompound();
					} else {
						flag = itemsword.getDamageVsEntity() > itemsword1.getDamageVsEntity();
					}
				} else if (itemstack.getItem() instanceof ItemBow && itemstack1.getItem() instanceof ItemBow) {
					flag = itemstack.hasTagCompound() && !itemstack1.hasTagCompound();
				} else {
					flag = false;
				}
			} else if (itemstack.getItem() instanceof ItemArmor && !(itemstack1.getItem() instanceof ItemArmor)) {
				flag = true;
			} else if (itemstack.getItem() instanceof ItemArmor && itemstack1.getItem() instanceof ItemArmor && !EnchantmentHelper.hasBindingCurse(itemstack1)) {
				ItemArmor itemarmor = (ItemArmor) itemstack.getItem();
				ItemArmor itemarmor1 = (ItemArmor) itemstack1.getItem();
				if (itemarmor.damageReduceAmount == itemarmor1.damageReduceAmount) {
					flag = itemstack.getMetadata() > itemstack1.getMetadata() || itemstack.hasTagCompound() && !itemstack1.hasTagCompound();
				} else {
					flag = itemarmor.damageReduceAmount > itemarmor1.damageReduceAmount;
				}
			} else {
				flag = false;
			}
		}

		if (flag && this.canEquipItem(itemstack)) {
			double d0;
			switch (entityequipmentslot.getSlotType()) {
			case HAND:
				d0 = this.inventoryHandsDropChances[entityequipmentslot.getIndex()];
				break;
			case ARMOR:
				d0 = this.inventoryArmorDropChances[entityequipmentslot.getIndex()];
				break;
			default:
				d0 = 0.0D;
			}

			if (!itemstack1.isEmpty() && this.rand.nextFloat() - 0.1F < d0) {
				this.entityDropItem(itemstack1, 0.0F);
			}

			if (itemstack.getItem() == Items.DIAMOND && entityitem.getThrower() != null) {
				EntityPlayer entityplayer = this.world.getPlayerEntityByName(entityitem.getThrower());
				if (entityplayer != null) {
					entityplayer.addStat(AchievementList.DIAMONDS_TO_YOU);
				}
			}

			this.setItemStackToSlot(entityequipmentslot, itemstack);
			switch (entityequipmentslot.getSlotType()) {
			case HAND:
				this.inventoryHandsDropChances[entityequipmentslot.getIndex()] = 2.0F;
				break;
			case ARMOR:
				this.inventoryArmorDropChances[entityequipmentslot.getIndex()] = 2.0F;
			}

			this.persistenceRequired = true;
			this.onItemPickup(entityitem, itemstack.getCount());
			entityitem.setDead();
		}

	}

	protected boolean canEquipItem(ItemStack var1) {
		return true;
	}

	protected boolean canDespawn() {
		return true;
	}

	protected void despawnEntity() {
		Object object = null;
		Object object1 = Reflector.getFieldValue(Reflector.Event_Result_DEFAULT);
		Object object2 = Reflector.getFieldValue(Reflector.Event_Result_DENY);
		if (this.persistenceRequired) {
			this.entityAge = 0;
		} else if ((this.entityAge & 31) == 31 && (object = Reflector.call(Reflector.ForgeEventFactory_canEntityDespawn, new Object[] { this })) != object1) {
			if (object == object2) {
				this.entityAge = 0;
			} else {
				this.setDead();
			}
		} else {
			EntityPlayer entityplayer = this.world.getClosestPlayerToEntity(this, -1.0D);
			if (entityplayer != null) {
				double d0 = entityplayer.posX - this.posX;
				double d1 = entityplayer.posY - this.posY;
				double d2 = entityplayer.posZ - this.posZ;
				double d3 = d0 * d0 + d1 * d1 + d2 * d2;
				if (this.canDespawn() && d3 > 16384.0D) {
					this.setDead();
				}

				if (this.entityAge > 600 && this.rand.nextInt(800) == 0 && d3 > 1024.0D && this.canDespawn()) {
					this.setDead();
				} else if (d3 < 1024.0D) {
					this.entityAge = 0;
				}
			}
		}

	}

	@Override
	protected final void updateEntityActionState() {
		++this.entityAge;
		this.world.theProfiler.startSection("checkDespawn");
		this.despawnEntity();
		this.world.theProfiler.endSection();
		this.world.theProfiler.startSection("sensing");
		this.senses.clearSensingCache();
		this.world.theProfiler.endSection();
		this.world.theProfiler.startSection("targetSelector");
		this.targetTasks.onUpdateTasks();
		this.world.theProfiler.endSection();
		this.world.theProfiler.startSection("goalSelector");
		this.tasks.onUpdateTasks();
		this.world.theProfiler.endSection();
		this.world.theProfiler.startSection("navigation");
		this.navigator.onUpdateNavigation();
		this.world.theProfiler.endSection();
		this.world.theProfiler.startSection("mob tick");
		this.updateAITasks();
		this.world.theProfiler.endSection();
		if (this.isRiding() && this.getRidingEntity() instanceof EntityLiving) {
			EntityLiving entityliving = (EntityLiving) this.getRidingEntity();
			entityliving.getNavigator().setPath(this.getNavigator().getPath(), 1.5D);
			entityliving.getMoveHelper().read(this.getMoveHelper());
		}

		this.world.theProfiler.startSection("controls");
		this.world.theProfiler.startSection("move");
		this.moveHelper.onUpdateMoveHelper();
		this.world.theProfiler.endStartSection("look");
		this.lookHelper.onUpdateLook();
		this.world.theProfiler.endStartSection("jump");
		this.jumpHelper.doJump();
		this.world.theProfiler.endSection();
		this.world.theProfiler.endSection();
	}

	protected void updateAITasks() {
	}

	public int getVerticalFaceSpeed() {
		return 40;
	}

	public int getHorizontalFaceSpeed() {
		return 10;
	}

	public void faceEntity(Entity entity, float f, float f1) {
		double d0 = entity.posX - this.posX;
		double d1 = entity.posZ - this.posZ;
		double d2;
		if (entity instanceof EntityLivingBase) {
			EntityLivingBase entitylivingbase = (EntityLivingBase) entity;
			d2 = entitylivingbase.posY + entitylivingbase.getEyeHeight() - (this.posY + this.getEyeHeight());
		} else {
			d2 = (entity.getEntityBoundingBox().minY + entity.getEntityBoundingBox().maxY) / 2.0D - (this.posY + this.getEyeHeight());
		}

		double d3 = MathHelper.sqrt(d0 * d0 + d1 * d1);
		float f2 = (float) (MathHelper.atan2(d1, d0) * 57.29577951308232D) - 90.0F;
		float f3 = (float) (-(MathHelper.atan2(d2, d3) * 57.29577951308232D));
		this.rotationPitch = EntityLiving.updateRotation(this.rotationPitch, f3, f1);
		this.rotationYaw = EntityLiving.updateRotation(this.rotationYaw, f2, f);
	}

	private static float updateRotation(float f, float f1, float f2) {
		float f3 = MathHelper.wrapDegrees(f1 - f);
		if (f3 > f2) {
			f3 = f2;
		}

		if (f3 < -f2) {
			f3 = -f2;
		}

		return f + f3;
	}

	public boolean getCanSpawnHere() {
		IBlockState iblockstate = this.world.getBlockState((new BlockPos(this)).down());
		return iblockstate.canEntitySpawn(this);
	}

	public boolean isNotColliding() {
		return !this.world.containsAnyLiquid(this.getEntityBoundingBox()) && this.world.getCollisionBoxes(this, this.getEntityBoundingBox()).isEmpty()
				&& this.world.checkNoEntityCollision(this.getEntityBoundingBox(), this);
	}

	public float getRenderSizeModifier() {
		return 1.0F;
	}

	public int getMaxSpawnedInChunk() {
		return 4;
	}

	@Override
	public int getMaxFallHeight() {
		if (this.getAttackTarget() == null) {
			return 3;
		} else {
			int i = (int) (this.getHealth() - this.getMaxHealth() * 0.33F);
			i = i - (3 - this.world.getDifficulty().getDifficultyId()) * 4;
			if (i < 0) {
				i = 0;
			}

			return i + 3;
		}
	}

	@Override
	public Iterable<ItemStack> getHeldEquipment() {
		return this.inventoryHands;
	}

	@Override
	public Iterable<ItemStack> getArmorInventoryList() {
		return this.inventoryArmor;
	}

	@Override
	public ItemStack getItemStackFromSlot(EntityEquipmentSlot entityequipmentslot) {
		switch (entityequipmentslot.getSlotType()) {
		case HAND:
			return this.inventoryHands.get(entityequipmentslot.getIndex());
		case ARMOR:
			return this.inventoryArmor.get(entityequipmentslot.getIndex());
		default:
			return ItemStack.EMPTY;
		}
	}

	@Override
	public void setItemStackToSlot(EntityEquipmentSlot entityequipmentslot, ItemStack itemstack) {
		switch (entityequipmentslot.getSlotType()) {
		case HAND:
			this.inventoryHands.set(entityequipmentslot.getIndex(), itemstack);
			break;
		case ARMOR:
			this.inventoryArmor.set(entityequipmentslot.getIndex(), itemstack);
		}

	}

	@Override
	protected void dropEquipment(boolean flag, int i) {
		for (EntityEquipmentSlot entityequipmentslot : EntityEquipmentSlot.values()) {
			ItemStack itemstack = this.getItemStackFromSlot(entityequipmentslot);
			double d0;
			switch (entityequipmentslot.getSlotType()) {
			case HAND:
				d0 = this.inventoryHandsDropChances[entityequipmentslot.getIndex()];
				break;
			case ARMOR:
				d0 = this.inventoryArmorDropChances[entityequipmentslot.getIndex()];
				break;
			default:
				d0 = 0.0D;
			}

			boolean flag1 = d0 > 1.0D;
			if (!itemstack.isEmpty() && !EnchantmentHelper.hasVanishingCurse(itemstack) && (flag || flag1) && this.rand.nextFloat() - i * 0.01F < d0) {
				if (!flag1 && itemstack.isItemStackDamageable()) {
					itemstack.setItemDamage(itemstack.getMaxDamage() - this.rand.nextInt(1 + this.rand.nextInt(Math.max(itemstack.getMaxDamage() - 3, 1))));
				}

				this.entityDropItem(itemstack, 0.0F);
			}
		}

	}

	protected void setEquipmentBasedOnDifficulty(DifficultyInstance difficultyinstance) {
		if (this.rand.nextFloat() < 0.15F * difficultyinstance.getClampedAdditionalDifficulty()) {
			int i = this.rand.nextInt(2);
			float f = this.world.getDifficulty() == EnumDifficulty.HARD ? 0.1F : 0.25F;
			if (this.rand.nextFloat() < 0.095F) {
				++i;
			}

			if (this.rand.nextFloat() < 0.095F) {
				++i;
			}

			if (this.rand.nextFloat() < 0.095F) {
				++i;
			}

			boolean flag = true;

			for (EntityEquipmentSlot entityequipmentslot : EntityEquipmentSlot.values()) {
				if (entityequipmentslot.getSlotType() == EntityEquipmentSlot.Type.ARMOR) {
					ItemStack itemstack = this.getItemStackFromSlot(entityequipmentslot);
					if (!flag && this.rand.nextFloat() < f) {
						break;
					}

					flag = false;
					if (itemstack.isEmpty()) {
						Item item = getArmorByChance(entityequipmentslot, i);
						if (item != null) {
							this.setItemStackToSlot(entityequipmentslot, new ItemStack(item));
						}
					}
				}
			}
		}

	}

	public static EntityEquipmentSlot getSlotForItemStack(ItemStack itemstack) {
		return itemstack.getItem() != Item.getItemFromBlock(Blocks.PUMPKIN) && itemstack.getItem() != Items.SKULL
				? (itemstack.getItem() instanceof ItemArmor ? ((ItemArmor) itemstack.getItem()).armorType
						: (itemstack.getItem() == Items.ELYTRA ? EntityEquipmentSlot.CHEST
								: (itemstack.getItem() == Items.SHIELD ? EntityEquipmentSlot.OFFHAND : EntityEquipmentSlot.MAINHAND)))
				: EntityEquipmentSlot.HEAD;
	}

	@Nullable
	public static Item getArmorByChance(EntityEquipmentSlot entityequipmentslot, int i) {
		switch (entityequipmentslot) {
		case HEAD:
			if (i == 0) {
				return Items.LEATHER_HELMET;
			} else if (i == 1) {
				return Items.GOLDEN_HELMET;
			} else if (i == 2) {
				return Items.CHAINMAIL_HELMET;
			} else if (i == 3) {
				return Items.IRON_HELMET;
			} else if (i == 4) {
				return Items.DIAMOND_HELMET;
			}
		case CHEST:
			if (i == 0) {
				return Items.LEATHER_CHESTPLATE;
			} else if (i == 1) {
				return Items.GOLDEN_CHESTPLATE;
			} else if (i == 2) {
				return Items.CHAINMAIL_CHESTPLATE;
			} else if (i == 3) {
				return Items.IRON_CHESTPLATE;
			} else if (i == 4) {
				return Items.DIAMOND_CHESTPLATE;
			}
		case LEGS:
			if (i == 0) {
				return Items.LEATHER_LEGGINGS;
			} else if (i == 1) {
				return Items.GOLDEN_LEGGINGS;
			} else if (i == 2) {
				return Items.CHAINMAIL_LEGGINGS;
			} else if (i == 3) {
				return Items.IRON_LEGGINGS;
			} else if (i == 4) {
				return Items.DIAMOND_LEGGINGS;
			}
		case FEET:
			if (i == 0) {
				return Items.LEATHER_BOOTS;
			} else if (i == 1) {
				return Items.GOLDEN_BOOTS;
			} else if (i == 2) {
				return Items.CHAINMAIL_BOOTS;
			} else if (i == 3) {
				return Items.IRON_BOOTS;
			} else if (i == 4) {
				return Items.DIAMOND_BOOTS;
			}
		default:
			return null;
		}
	}

	protected void setEnchantmentBasedOnDifficulty(DifficultyInstance difficultyinstance) {
		float f = difficultyinstance.getClampedAdditionalDifficulty();
		if (!this.getHeldItemMainhand().isEmpty() && this.rand.nextFloat() < 0.25F * f) {
			this.setItemStackToSlot(EntityEquipmentSlot.MAINHAND,
					EnchantmentHelper.addRandomEnchantment(this.rand, this.getHeldItemMainhand(), (int) (5.0F + f * this.rand.nextInt(18)), false));
		}

		for (EntityEquipmentSlot entityequipmentslot : EntityEquipmentSlot.values()) {
			if (entityequipmentslot.getSlotType() == EntityEquipmentSlot.Type.ARMOR) {
				ItemStack itemstack = this.getItemStackFromSlot(entityequipmentslot);
				if (!itemstack.isEmpty() && this.rand.nextFloat() < 0.5F * f) {
					this.setItemStackToSlot(entityequipmentslot, EnchantmentHelper.addRandomEnchantment(this.rand, itemstack, (int) (5.0F + f * this.rand.nextInt(18)), false));
				}
			}
		}

	}

	@Nullable
	public IEntityLivingData onInitialSpawn(DifficultyInstance var1, @Nullable IEntityLivingData ientitylivingdata) {
		this.getEntityAttribute(SharedMonsterAttributes.FOLLOW_RANGE).applyModifier(new AttributeModifier("Random spawn bonus", this.rand.nextGaussian() * 0.05D, 1));
		if (this.rand.nextFloat() < 0.05F) {
			this.setLeftHanded(true);
		} else {
			this.setLeftHanded(false);
		}

		return ientitylivingdata;
	}

	public boolean canBeSteered() {
		return false;
	}

	public void enablePersistence() {
		this.persistenceRequired = true;
	}

	public void setDropChance(EntityEquipmentSlot entityequipmentslot, float f) {
		switch (entityequipmentslot.getSlotType()) {
		case HAND:
			this.inventoryHandsDropChances[entityequipmentslot.getIndex()] = f;
			break;
		case ARMOR:
			this.inventoryArmorDropChances[entityequipmentslot.getIndex()] = f;
		}

	}

	public boolean canPickUpLoot() {
		return this.canPickUpLoot;
	}

	public void setCanPickUpLoot(boolean flag) {
		this.canPickUpLoot = flag;
	}

	public boolean isNoDespawnRequired() {
		return this.persistenceRequired;
	}

	@Override
	public final boolean processInitialInteract(EntityPlayer entityplayer, EnumHand enumhand) {
		if (this.getLeashed() && this.getLeashedToEntity() == entityplayer) {
			this.clearLeashed(true, !entityplayer.capabilities.isCreativeMode);
			return true;
		} else {
			ItemStack itemstack = entityplayer.getHeldItem(enumhand);
			if (itemstack.getItem() == Items.LEAD && this.canBeLeashedTo(entityplayer)) {
				this.setLeashedToEntity(entityplayer, true);
				itemstack.shrink(1);
				return true;
			} else {
				return this.processInteract(entityplayer, enumhand) ? true : super.processInitialInteract(entityplayer, enumhand);
			}
		}
	}

	protected boolean processInteract(EntityPlayer var1, EnumHand var2) {
		return false;
	}

	protected void updateLeashedState() {
		if (this.leashNBTTag != null) {
			this.recreateLeash();
		}

		if (this.isLeashed) {
			if (!this.isEntityAlive()) {
				this.clearLeashed(true, true);
			}

			if (this.leashedToEntity == null || this.leashedToEntity.isDead) {
				this.clearLeashed(true, true);
			}
		}

	}

	public void clearLeashed(boolean flag, boolean flag1) {
		if (this.isLeashed) {
			this.isLeashed = false;
			this.leashedToEntity = null;
			if (!this.world.isRemote && flag1) {
				this.dropItem(Items.LEAD, 1);
			}

			if (!this.world.isRemote && flag && this.world instanceof WorldServer) {
				((WorldServer) this.world).getEntityTracker().sendToTracking(this, new SPacketEntityAttach(this, (Entity) null));
			}
		}

	}

	public boolean canBeLeashedTo(EntityPlayer var1) {
		return !this.getLeashed() && !(this instanceof IMob);
	}

	public boolean getLeashed() {
		return this.isLeashed;
	}

	public Entity getLeashedToEntity() {
		return this.leashedToEntity;
	}

	public void setLeashedToEntity(Entity entity, boolean flag) {
		this.isLeashed = true;
		this.leashedToEntity = entity;
		if (!this.world.isRemote && flag && this.world instanceof WorldServer) {
			((WorldServer) this.world).getEntityTracker().sendToTracking(this, new SPacketEntityAttach(this, this.leashedToEntity));
		}

		if (this.isRiding()) {
			this.dismountRidingEntity();
		}

	}

	@Override
	public boolean startRiding(Entity entity, boolean flag) {
		boolean flag1 = super.startRiding(entity, flag);
		if (flag1 && this.getLeashed()) {
			this.clearLeashed(true, true);
		}

		return flag1;
	}

	private void recreateLeash() {
		if (this.isLeashed && this.leashNBTTag != null) {
			if (this.leashNBTTag.hasUniqueId("UUID")) {
				UUID uuid = this.leashNBTTag.getUniqueId("UUID");

				for (EntityLivingBase entitylivingbase : this.world.getEntitiesWithinAABB(EntityLivingBase.class, this.getEntityBoundingBox().expandXyz(10.0D))) {
					if (entitylivingbase.getUniqueID().equals(uuid)) {
						this.setLeashedToEntity(entitylivingbase, true);
						break;
					}
				}
			} else if (this.leashNBTTag.hasKey("X", 99) && this.leashNBTTag.hasKey("Y", 99) && this.leashNBTTag.hasKey("Z", 99)) {
				BlockPos blockpos = new BlockPos(this.leashNBTTag.getInteger("X"), this.leashNBTTag.getInteger("Y"), this.leashNBTTag.getInteger("Z"));
				EntityLeashKnot entityleashknot = EntityLeashKnot.getKnotForPosition(this.world, blockpos);
				if (entityleashknot == null) {
					entityleashknot = EntityLeashKnot.createKnot(this.world, blockpos);
				}

				this.setLeashedToEntity(entityleashknot, true);
			} else {
				this.clearLeashed(false, true);
			}
		}

		this.leashNBTTag = null;
	}

	@Override
	public boolean replaceItemInInventory(int i, ItemStack itemstack) {
		EntityEquipmentSlot entityequipmentslot;
		if (i == 98) {
			entityequipmentslot = EntityEquipmentSlot.MAINHAND;
		} else if (i == 99) {
			entityequipmentslot = EntityEquipmentSlot.OFFHAND;
		} else if (i == 100 + EntityEquipmentSlot.HEAD.getIndex()) {
			entityequipmentslot = EntityEquipmentSlot.HEAD;
		} else if (i == 100 + EntityEquipmentSlot.CHEST.getIndex()) {
			entityequipmentslot = EntityEquipmentSlot.CHEST;
		} else if (i == 100 + EntityEquipmentSlot.LEGS.getIndex()) {
			entityequipmentslot = EntityEquipmentSlot.LEGS;
		} else {
			if (i != 100 + EntityEquipmentSlot.FEET.getIndex()) {
				return false;
			}

			entityequipmentslot = EntityEquipmentSlot.FEET;
		}

		if (!itemstack.isEmpty() && !isItemStackInSlot(entityequipmentslot, itemstack) && entityequipmentslot != EntityEquipmentSlot.HEAD) {
			return false;
		} else {
			this.setItemStackToSlot(entityequipmentslot, itemstack);
			return true;
		}
	}

	@Override
	public boolean canPassengerSteer() {
		return this.canBeSteered() && super.canPassengerSteer();
	}

	public static boolean isItemStackInSlot(EntityEquipmentSlot entityequipmentslot, ItemStack itemstack) {
		EntityEquipmentSlot entityequipmentslot1 = getSlotForItemStack(itemstack);
		return entityequipmentslot1 == entityequipmentslot || entityequipmentslot1 == EntityEquipmentSlot.MAINHAND && entityequipmentslot == EntityEquipmentSlot.OFFHAND
				|| entityequipmentslot1 == EntityEquipmentSlot.OFFHAND && entityequipmentslot == EntityEquipmentSlot.MAINHAND;
	}

	@Override
	public boolean isServerWorld() {
		return super.isServerWorld() && !this.isAIDisabled();
	}

	public void setNoAI(boolean flag) {
		byte b0 = this.dataManager.get(AI_FLAGS).byteValue();
		this.dataManager.set(AI_FLAGS, Byte.valueOf(flag ? (byte) (b0 | 1) : (byte) (b0 & -2)));
	}

	public void setLeftHanded(boolean flag) {
		byte b0 = this.dataManager.get(AI_FLAGS).byteValue();
		this.dataManager.set(AI_FLAGS, Byte.valueOf(flag ? (byte) (b0 | 2) : (byte) (b0 & -3)));
	}

	public boolean isAIDisabled() {
		return (this.dataManager.get(AI_FLAGS).byteValue() & 1) != 0;
	}

	public boolean isLeftHanded() {
		return (this.dataManager.get(AI_FLAGS).byteValue() & 2) != 0;
	}

	@Override
	public EnumHandSide getPrimaryHand() {
		return this.isLeftHanded() ? EnumHandSide.LEFT : EnumHandSide.RIGHT;
	}

	private boolean canSkipUpdate() {
		if (this.isChild()) {
			return false;
		} else if (this.hurtTime > 0) {
			return false;
		} else if (this.ticksExisted < 20) {
			return false;
		} else {
			World world = this.getEntityWorld();
			if (world == null) {
				return false;
			} else if (world.playerEntities.size() != 1) {
				return false;
			} else {
				Entity entity = world.playerEntities.get(0);
				double d0 = Math.max(Math.abs(this.posX - entity.posX) - 16.0D, 0.0D);
				double d1 = Math.max(Math.abs(this.posZ - entity.posZ) - 16.0D, 0.0D);
				double d2 = d0 * d0 + d1 * d1;
				return !this.isInRangeToRenderDist(d2);
			}
		}
	}

	private void onUpdateMinimal() {
		++this.entityAge;
		if (this instanceof EntityMob) {
			float f = this.getBrightness(1.0F);
			if (f > 0.5F) {
				this.entityAge += 2;
			}
		}

		this.despawnEntity();
	}

	@Override
	public Team getTeam() {
		UUID uuid = this.getUniqueID();
		if (this.teamUuid != uuid) {
			this.teamUuid = uuid;
			this.teamUuidString = uuid.toString();
		}

		return this.world.getScoreboard().getPlayersTeam(this.teamUuidString);
	}

	@Override
	public EntityLivingBase getEntity() {
		return this;
	}

	public static enum SpawnPlacementType {
		ON_GROUND, IN_AIR, IN_WATER;
	}
}
