package net.minecraft.entity.ai;

import com.google.common.collect.Lists;

import me.lpk.ai.AIUser;

import java.util.List;
import net.minecraft.entity.Entity;

public class EntitySenses {
	AIUser user;
	List<Entity> seenEntities = Lists.<Entity> newArrayList();
	List<Entity> unseenEntities = Lists.<Entity> newArrayList();

	public EntitySenses(AIUser entityObjIn) {
		this.user = entityObjIn;
	}

	/**
	 * Clears canSeeCachePositive and canSeeCacheNegative.
	 */
	public void clearSensingCache() {
		this.seenEntities.clear();
		this.unseenEntities.clear();
	}

	/**
	 * Checks, whether 'our' entity can see the entity given as argument (true)
	 * or not (false), caching the result.
	 */
	public boolean canSee(Entity entityIn) {
		if (this.seenEntities.contains(entityIn)) {
			return true;
		} else if (this.unseenEntities.contains(entityIn)) {
			return false;
		} else {
			this.user.getEntity().world.theProfiler.startSection("canSee");
			boolean flag = this.user.getEntity().canEntityBeSeen(entityIn);
			this.user.getEntity().world.theProfiler.endSection();

			if (flag) {
				this.seenEntities.add(entityIn);
			} else {
				this.unseenEntities.add(entityIn);
			}

			return flag;
		}
	}
}
