package net.minecraft.entity.ai;

import me.lpk.ai.AIUser;
import me.lpk.ai.IJumpHelper;

public class EntityJumpHelper implements IJumpHelper {
	private final AIUser user;
	public boolean isJumping;

	public EntityJumpHelper(AIUser entityIn) {
		this.user = entityIn;
	}

	public void setJumping() {
		this.setJumping(true);
	}

	/**
	 * Called to actually make the entity jump if isJumping is true.
	 */
	public void doJump() {
		if (this.isJumping) {
			user.getEntity().jump();
			this.isJumping = false;
		}
	}

	@Override
	public void setJumping(boolean isJumping) {
		this.isJumping = isJumping;
	}
}
