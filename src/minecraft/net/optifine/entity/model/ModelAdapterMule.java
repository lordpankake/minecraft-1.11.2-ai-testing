package net.optifine.entity.model;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderAbstractHorse;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.passive.EntityMule;
import net.optifine.entity.model.IEntityRenderer;
import net.optifine.entity.model.ModelAdapterHorse;

public class ModelAdapterMule extends ModelAdapterHorse {
	public ModelAdapterMule() {
		super(EntityMule.class, "mule", 0.75F);
	}

	@Override
	public IEntityRenderer makeEntityRender(ModelBase modelbase, float f) {
		RenderManager rendermanager = Minecraft.getMinecraft().getRenderManager();
		RenderAbstractHorse renderabstracthorse = new RenderAbstractHorse(rendermanager);
		renderabstracthorse.mainModel = modelbase;
		renderabstracthorse.shadowSize = f;
		return renderabstracthorse;
	}
}
