package net.optifine.entity.model.anim;

public class ModelUpdater {
	private ModelVariableUpdater[] modelVariableUpdaters;

	public ModelUpdater(ModelVariableUpdater[] modelVariableUpdaters) {
		this.modelVariableUpdaters = modelVariableUpdaters;
	}

	public void update() {
		for (ModelVariableUpdater modelvariableupdater : this.modelVariableUpdaters) {
			modelvariableupdater.update();
		}
	}

	public boolean initialize(IModelResolver mr) {
		for (ModelVariableUpdater modelVariableUpdater2 : this.modelVariableUpdaters) {
			ModelVariableUpdater modelvariableupdater = modelVariableUpdater2;

			if (!modelvariableupdater.initialize(mr)) {
				return false;
			}
		}

		return true;
	}
}
