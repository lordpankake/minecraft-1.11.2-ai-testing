package net.optifine.entity.model.anim;

public class RenderResolverEntity implements IRenderResolver {
	@Override
	public IExpression getParameter(String name) {
		EnumRenderParameterEntity enumrenderparameterentity = EnumRenderParameterEntity.parse(name);
		return enumrenderparameterentity;
	}
}
